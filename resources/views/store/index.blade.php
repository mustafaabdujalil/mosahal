@extends('layouts.master')
@section('title')
    الخزينة
@endsection
@section('styles')
    <style>
        #admins tbody tr {
            word-break: break-all;
        }
    </style>
@endsection
@section('modals')
@endsection
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">

                    <div class="box">
                        <!-- /.box-header -->
                        <div class="box-header with-border">
                            <h3 class="box-title">الخزينة</h3>
                        </div>
                        <div class="filter">
                            <h6 style="font-style: italic;margin-right: 15px;">بحث بتاريخ التحصيل</h6>
                            <form action="/store" method="get" id="filter-form">
                                <div class="row">
                                    <div class="col-md-6 form-group">
                                        <label for="from">من</label>
                                        <input type="date" value="{{(Session::has('store_from') ? Session::get('store_from'): "")}}" class="form-control" name="from">
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label for="to">الي</label>
                                        <input type="date" value="{{(Session::has('store_to') ? Session::get('store_to'): "")}}" class="form-control" name="to">
                                    </div>
                                    @if(Auth::user()->role == "Admin")
                                    <div class="col-md-12 form-group">
                                        <label for="collector">بحث بالمحصلين </label>
                                        <select name="collector[]" id="collector" class="form-control" multiple>
                                            <option disabled>اختار محصل</option>
                                            <option value="0">الكل</option>
                                            @foreach($collectors as $collector)
                                                @if(!is_null(Session::get('store_collector')))
                                                    <option value="{{$collector->id}}" {{(in_array($collector->id,Session::get('store_collector')) ? 'selected' : "")}}>{{$collector->name}}</option>
                                                @else
                                                    <option value="{{$collector->id}}" >{{$collector->name}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    @endif
                                    <div class="col-md-12" style="padding-top: 25px;">
                                        <label for=""></label>
                                        <input type="submit" value="بحث" class="btn btn-md btn-success">
                                        <input type="button" value="مسح البحث" class="btn btn-md btn-success reset">
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="box-body">
                            <table id="suppliers"  width="100%">
                                <thead>
                                    <tr>
                                        <th>اسم المحصل</th>
                                        <th>اسم العميل</th>
                                        <th>تاريخ التحصيل</th>
                                        <th>اجمالي المبلغ المحصل</th>
                                        <th>اجمالي المبلغ المتبقي</th>
                                        <th>نوع عملية التحصيل</th>
                                    </tr>
                                </thead>
                            </table>

                            <div class="form-group">
                                <label for="sub_total" class="col-md-2 below-table">اجمالي المبلغ المحصل : </label>
                                <div class="col-md-3">
                                    <div class="input-group">
                                        <input id="total" type="text" class="form-control" name="total" readonly>
                                    </div>
                                </div>
{{--                                <label for="sub_total" class="col-md-2 below-table">اجمالي المبلغ المسدد : </label>--}}
{{--                                <div class="col-md-3">--}}
{{--                                    <div class="input-group">--}}
{{--                                        <input id="total_current_balance" type="text" class="form-control" name="total_current_balance" readonly>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
                                <label for="sub_total" class="col-md-2 below-table">اجمالي المبلغ المتبقي : </label>
                                <div class="col-md-3">
                                    <div class="input-group">
                                        <input id="total_remaining_balance" type="text" class="form-control" name="total_remaining_balance" readonly>
                                    </div>
                                </div>
                                <div class="col-md-2"></div>
                            </div>
                            <br><br>

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
@section('scripts')
    <script type="text/javascript">
        var totalAmount = 0.00;
        var totalCurrentAmount = 0.00;
        var totalRemainingAmount = 0.00;
        $("#collector").select2();
        $('#suppliers').DataTable({
            destroy: true,
            processing: true,
            serverSide: true,
            order: [[0, "asc"]],
            dom: 'bfrtlip',
            buttons: [
                {
                    extend: 'copyHtml5',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3,4,5,6]
                    }
                },
                {
                    extend: 'excelHtml5',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3,4,5,6]
                    }
                },
                {
                    extend: 'pdfHtml5',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3,4,5,6]
                    }
                },
                {
                    extend: 'print',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3,4,5,6]
                    }
                },

            ],
            select: true,
            ajax: {
                "url": '{{route("store.fetchCollectors")}}',
                "type": 'GET'
            },
            columns: [
                {data: 'collector_name', name: 'collector_name'},
                {data: 'client_name', name: 'client_name'},
                {data: 'collect_date', name: 'collect_date'},
                {data: 'paid_amount', name: 'paid_amount'},
                {data: 'remaining_amount', name: 'remaining_amount'},
                {data: 'submitted_value_status', name: 'submitted_value_status'},
            ],
            aLengthMenu: [
                [10, 50, 100, -1],
                [10, 50, 100, "All"]
            ],
            "oLanguage": {
                "sSearch": "بحث : "
            },
            'fnCreatedRow': function (nRow, aData, iDataIndex) {

                if(iDataIndex == 0){
                    totalAmount = 0.00;
                    totalCurrentAmount = 0.00;
                    totalRemainingAmount = 0.00;
                }
                totalAmount = parseFloat(aData['paid_amount']) + totalAmount;
                $('#total').val(totalAmount.toFixed(2));
                totalRemainingAmount = parseFloat(aData['remaining_amount']) + totalRemainingAmount;
                $('#total_remaining_balance').val(totalRemainingAmount.toFixed(2));
                // totalCurrentAmount = totalAmount - totalRemainingAmount;
                // $('#total_current_balance').val(totalCurrentAmount.toFixed(2));
            }
        });

        $(".reset").on('click',function () {

            $('input[name=to]').val('');
            $('input[name=from]').val('');
            $('#collector').val(null).trigger('change');

            $.ajax({
                type:'get',
                url:'/store/reset-filter-form',
                success:function(data){
                    location.replace(data.url);
                }
            });
        });
    </script>
@endsection

