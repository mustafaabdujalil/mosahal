@extends('layouts.master')
@section('title')
اضافة محصل
@endsection
@section('styles')
@endsection
@section('modals')
@endsection
@section('content')
    <div class="content-wrapper" style="min-height: 1295.8px;">
        <section class="content">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">اضافة محصل جديد</h3>
                </div>
                <!-- /.box-header -->
                <form action="{{route('collector.storeAdding')}}" method="post" data-parsley-validate="" novalidate class="error">
                    @csrf
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="example-name-input" class="col-form-label">الاسم</label>
                                    <input class="form-control" value="{{old('name')}}" name="name" type="text" id="example-name-input" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="example-email-input" class="col-form-label">البريد الالكتروني</label>
                                    <input class="form-control" value="{{old('email')}}" name="email" type="email" id="example-email-input" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="example-password-input" class="col-form-label">كلمة المرور </label>
                                    <input class="form-control" name="password" type="password" id="example-password-input" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="example-phone-input" class="col-form-label">التليفون</label>
                                    <input class="form-control" value="{{old('phone')}}" name="phone" data-parsley-type="digits"	 type="text" id="example-phone-input">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="example-mobile-input" class="col-form-label">الموبايل</label>
                                    <input class="form-control" value="{{old('mobile')}}" name="mobile" type="text" data-parsley-type="digits" data-parsley-length="[11, 11]" id="example-mobile-input" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="example-number-input" class="col-form-label">رقم الواتس اب</label>
                                    <input class="form-control" value="{{old('whatsapp_number')}}" name="whatsapp_number" data-parsley-type="digits" data-parsley-length="[11, 11]" type="text" id="example-number-input">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="example-card-code-input" class="col-form-label">كود الكارته</label>
                                    <input class="form-control" value="{{old('card_code')}}" name="card_code" type="text" id="example-card_code-input">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="example-national-id-input" class="col-form-label">الرقم القومي</label>
                                    <input class="form-control" value="{{old('national_id')}}" name="national_id" data-parsley-length="[14, 14]" data-parsley-type="digits" type="text" id="example-name-input" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="example-address-input" class="col-form-label">العنوان</label>
                                    <input class="form-control" value="{{old('address')}}" name="address" type="text" id="example-address-input" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="example-number-input" class="col-form-label">المدينة</label>
                                    <select class="form-control select2 select2-hidden-accessible" name="city_id" style="width: 100%;" tabindex="-1" aria-hidden="true" required>
                                        @foreach($cities as $city)
                                            <option value="{{$city->id}}">{{$city->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="example-number-input" class="col-form-label"></label>
                                    <div class="demo-checkbox">
                                        <input type="submit" class="btn btn-blue" value="حفظ">
                                    </div>
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                </form>
                <!-- /.box-body -->
            </div>
        </section>
    </div>
@endsection
@section('scripts')
@endsection

