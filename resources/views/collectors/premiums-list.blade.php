@extends('layouts.master')
@section('title')
جدول استحقاقات الخاص بالمحصل {{$collector->name}}
@endsection
@section('styles')
    <style>
        #admins tbody tr {
            word-break: break-all;
        }
    </style>
@endsection
@section('modals')
@endsection
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">

                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title" >جدول الاستحقاقات الخاص بالمحصل {{$collector->name}}</h3>
                            <h4 style="margin-top: 25px !important;">اجمالي التحصيل : {{$total}} جنيه مصري</h4>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="premiums" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th>المبلغ المدفوع</th>
                                    <th>كود العملية</th>
                                    <th>تاريخ التحصيل</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
@section('scripts')
    <script type="text/javascript">

        $('#premiums').DataTable({
            destroy: true,
            processing: true,
            serverSide: true,
            order: [[0, "asc"]],
            dom: 'bfrtlip',
            buttons: [
                {
                    extend: 'copyHtml5',
                    exportOptions: {
                        columns: [ 0, 1, 2 ]
                    }
                },
                {
                    extend: 'excelHtml5',
                    exportOptions: {
                        columns: [ 0, 1, 2 ]
                    }
                },
                {
                    extend: 'pdfHtml5',
                    exportOptions: {
                        columns: [ 0, 1, 2 ]
                    }
                },
                {
                    extend: 'print',
                    exportOptions: {
                        columns: [ 0, 1, 2 ]
                    }
                },

            ],
            select: true,
            ajax: {
                "url": '{{route("collector.fetchPremiums")}}',
                "type": 'GET',
                'data':{
                    'id' : "{{$collector->id}}",
                },
            },
            columns: [
                {data: 'paid_amount', name: 'paid_amount'},
                {data: 'code', name: 'code'},
                {data: 'collect_date', name: 'collect_date'},
            ],
            aLengthMenu: [
                [10, 50, 100, 200, -1],
                [10, 50, 100, 200, "All"]
            ],
            "oLanguage": {
                "sSearch": "بحث : "
            }
        });
    </script>
@endsection

