@extends('layouts.master')
@section('title')
    العملاء
@endsection
@section('styles')
    <style>
        #admins tbody tr {
            word-break: break-all;
        }

    </style>
@endsection
@section('modals')
@endsection
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">

                    <div class="box">
                        @if(Auth::user()->role == "Admin")
                            <div class="box-header with-border">
                                <h3 class="box-title">جدول العملاء</h3>
                                <a class="btn btn-cyan" style="float: left;" href="{{route('client.addForm')}}">اضف عميل</a>
                            </div>
                        @endif

                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="clients" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th>البريد الالكتروني</th>
                                    <th>الاسم</th>
                                    <th>الموبايل</th>
                                    <th>التليفون</th>
                                    <th>واتس اب</th>
                                    <th>كود الكارته</th>
                                    <th>العنوان</th>
                                    <th>الشركة</th>
                                    <th>المدينة</th>
                                    <th>امكانية الشراء بدون كود تاكيد</th>
                                    <th>اجمالي المبلغ المستحق</th>
                                    <th>الحالة</th>
{{--                                    <th>الصوره</th>--}}
                                    <th>العمليات</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
@section('scripts')
    <script type="text/javascript">

        $('#clients').DataTable({
            destroy: true,
            processing: true,
            serverSide: true,
            order: [[0, "asc"]],
            dom: 'bfrtlip',
            buttons: [
                {
                    extend: 'copyHtml5',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11 ]
                    }
                },
                {
                    extend: 'excelHtml5',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11 ]
                    }
                },
                {
                    extend: 'pdfHtml5',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11 ]
                    }
                },
                {
                    extend: 'print',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11 ]
                    }
                },

            ],
            select: true,
            ajax: {
                "url": '{{route("client.fetchClients")}}',
                "type": 'GET'
            },
            columns: [
                {data: 'email', name: 'email'},
                {data: 'name', name: 'name'},
                {data: 'mobile', name: 'mobile'},
                {data: 'phone', name: 'phone'},
                {data: 'whatsapp', name: 'whatsapp'},
                {data: 'card_code', name: 'card_code'},
                {data: 'address', name: 'address'},
                {data: 'company', name: 'company'},
                {data: 'city', name: 'city'},
                {data: 'auto_buy_option', name: 'auto_buy_option'},
                {data: 'remaining_amount', name: 'remaining_amount'},
                {data: 'status', name: 'status'},
                // {data: 'image', name: 'image'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ],
            aLengthMenu: [
                [10, 50, 100, 200, -1],
                [10, 50, 100, 200, "All"]
            ],
            "oLanguage": {
                "sSearch": "بحث : "
            }
        });

        function deleteClient(id) {
            console.log(id);
            Swal.fire({
                title: 'هل انت متاكد من الحذف ؟',
                text: "ﻻ تستطيع استعاده العميل بعد الحذف",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'نعم',
                cancelButtonText: 'ﻻ',
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        'url':'/client/delete',
                        'type':'POST',
                        'data':{
                            'id' : id,
                            "_token": "{{ csrf_token() }}",
                        },
                        success:function(data){
                            if(data.status == "warning"){
                                Swal.queue([{
                                    title: 'Warning',
                                    confirmButtonText: 'Ok',
                                    text:'You can\'t remove this client because he has Purchases are still not paid',
                                    showLoaderOnConfirm: true,
                                    preConfirm: () => {
                                        location.reload(true)
                                    }
                                }])
                            }else{
                                Swal.queue([{
                                    title: 'success',
                                    confirmButtonText: 'Ok',
                                    text:'Client has been deleted successfully',
                                    showLoaderOnConfirm: true,
                                    preConfirm: () => {
                                        location.reload(true)
                                    }
                                }])
                            }
                        },
                        error : function(){
                        }
                    });
                }
            })
        }
        // set table style
        $("#clients").removeClass('dataTable');
    </script>
@endsection

