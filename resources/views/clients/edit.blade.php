@extends('layouts.master')
@section('title')
    تعديل البيانات
@endsection
@section('styles')
@endsection
@section('modals')
@endsection
@section('content')
<div class="content-wrapper" style="min-height: 1295.8px;">
    <section class="content">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title"> تعديل بيانات {{$client->name}}</h3>
            </div>
            <!-- /.box-header -->
            <form action="{{route('client.storeEdit')}}" method="post" data-parsley-validate="" enctype="multipart/form-data" novalidate class="error">
                @csrf
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="example-name-input" class="col-form-label">الاسم</label>
                                <input class="form-control" value="{{$client->name}}" name="name" type="text" id="example-name-input" required>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="example-national-id-input" class="col-form-label">الرقم القومي</label>
                                <input class="form-control" value="{{$client->national_id}}" data-parsley-type="digits"	 data-parsley-length="[14, 14]" name="national_id" type="text" id="example-name-input" required>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="example-card-code-input" class="col-form-label">كود الكارته</label>
                                <input class="form-control" name="card_code" value="{{$client->user->card_code}}" type="text" id="example-card_code-input" required>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="example-gender-input" class="col-form-label">الجنس</label>
                                <select class="form-control select2 select2-hidden-accessible" name="gender" style="width: 100%;" tabindex="-1" aria-hidden="true" required>
                                    <option value="ذكر" <?php if($client->gender == "ذكر") echo "selected"; ?>>ذكر</option>
                                    <option value="انثي" <?php if($client->gender == "انثي") echo "selected"; ?>>انثي</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="example-social-status-input" class="col-form-label">الحالة الاجتماعية</label>
                                <select class="form-control select2 select2-hidden-accessible" name="social_status" style="width: 100%;" tabindex="-1" aria-hidden="true" required>
                                    <option value="اعزب" <?php if($client->social_status == "اعزب") echo "selected"; ?>>اعزب/عزبة</option>
                                    <option value="متزوج" <?php if($client->social_status == "متزوج") echo "selected"; ?>>متزوج / متزوجة</option>
                                    <option value="ارمل" <?php if($client->social_status == "ارمل") echo "selected"; ?>>ارمل / ارملة</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="example-address-input" class="col-form-label">العنوان</label>
                                <input class="form-control" value="{{$client->address}}" name="address" type="text" id="example-address-input" required>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="example-floor-number-input" class="col-form-label">رقم الطابق</label>
                                <input class="form-control" name="floor_number" required data-parsley-type="digits"	 value="{{$client->floor_number}}" type="number" min="0" id="example-floor-number-input">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="example-flat-number-input" class="col-form-label">رقم الشقة</label>
                                <input class="form-control" name="flat_number" required data-parsley-type="digits"	 value="{{$client->flat_number}}" type="number" min="0" id="example-flat-number-input">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="example-area-input" class="col-form-label">المنطقة</label>
                                <input class="form-control" name="area" required value="{{$client->area}}" type="text" id="example-area-input">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="example-fork-name-input" class="col-form-label">متفرع من</label>
                                <input class="form-control" name="fork_name" required value="{{$client->fork_name}}" type="text" id="example-fork-name-input">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="example-special-mark-input" class="col-form-label">علامة مميزة</label>
                                <input class="form-control" name="special_mark" required value="{{$client->special_mark}}" type="text" id="example-special-mark-input">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="example-number-input" class="col-form-label">المدينة</label>
                                <select class="form-control select2 select2-hidden-accessible" name="city_id" style="width: 100%;" tabindex="-1" aria-hidden="true" required>
                                    @foreach($cities as $city)
                                        <option value="{{$city->id}}" <?php if($client->city_id == $city->id) echo "selected"; ?>>{{$city->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="example-company-input" class="col-form-label">الشركة</label>
                                <select class="form-control select2 select2-hidden-accessible" name="company_id" style="width: 100%;" tabindex="-1" aria-hidden="true" required>
                                    @foreach($companies as $company)
                                        <option value="{{$company->id}}" <?php if($client->company_id == $company->id) echo "selected"; ?>>{{$company->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="example-company-input" class="col-form-label">المحصلون</label>
                                <select class="form-control select2 select2-hidden-accessible" multiple name="collectors[]" style="width: 100%;" tabindex="-1" aria-hidden="true" required>
                                    @foreach($collectors as $collector)
                                        @if(in_array($collector->id,$selected_collectors))
                                            <option value="{{$collector->id}}" selected="selected">{{$collector->name}}</option>
                                        @else
                                            <option value="{{$collector->id}}">{{$collector->name}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="example-company-input" class="col-form-label">عملاء ذات صلة</label>
                                <select class="form-control select2 select2-hidden-accessible" multiple name="clients[]" style="width: 100%;" tabindex="-1" aria-hidden="true">
                                    @foreach($clients as $related)
                                        @if(in_array($related->id,$related_clients))
                                            <option value="{{$related->id}}" selected="selected">{{$related->name}}</option>
                                        @else
                                            <option value="{{$related->id}}">{{$related->name}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="example-job-input" class="col-form-label">الوظيفة</label>
                                <input class="form-control" name="job" value="{{$client->job}}" type="text" id="example-job-input" required>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="example-department-input" class="col-form-label">القسم</label>
                                <input class="form-control" name="department" value="{{$client->department}}" type="text" id="example-department-input">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="example-customer-number-input" class="col-form-label">رقم العميل</label>
                                <input class="form-control" name="customer_number" value="{{$client->customer_number}}" type="text" id="example-customer-number-input">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="auto_buy_option" class="col-form-label">الشراء اتوماتيك بدون كود تاكيد</label>
                                <select class="form-control select2 select2-hidden-accessible" name="auto_buy_option" style="width: 100%;" tabindex="-1" aria-hidden="true" required>
                                    <option value="0" {{$client->auto_buy_option == 0 ? 'selected' : ''}}>ﻻ</option>
                                    <option value="1" {{$client->auto_buy_option == 1 ? 'selected' : ''}}>نعم</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="example-salary-input" class="col-form-label">المرتب</label>
                                <input class="form-control" name="salary" data-parsley-type="digits"  value="{{$client->salary}}"type="number" min="0" id="example-salary-input">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="example-premiums-months-input" class="col-form-label">عدد الاقساط الشهرية</label>
                                <input class="form-control" name="premiums_months" data-parsley-type="digits"  value="{{$client->premiums_months}}"type="number" min="0" id="example-premiums-months-input">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="example-max_value-input" class="col-form-label">اقصي قيمه للشراء</label>
                                <input class="form-control" name="max_value" data-parsley-type="digits"  value="{{$client->fixed_max_value}}"type="number" min="0" id="example-max_value-input">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="example-facebook-link-input" class="col-form-label">رابط facebook</label>
                                <input class="form-control" name="facebook_link" data-parsley-type="url"	 value="{{$client->facebook_link}}" type="url" id="example-facebook-link-input">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="example-mobileinput" class="col-form-label">التليفون</label>
                                <input class="form-control" name="mobile" type="text" data-parsley-length="[11, 11]" data-parsley-type="digits" value="{{$client->mobile}}" id="example-mobile-input" required>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="example-alter-mobile-input" class="col-form-label"> التليفون البديل</label>
                                <input class="form-control" name="alter_mobile" data-parsley-type="digits" data-parsley-length="[11, 11]"	 type="text" value="{{$client->alter_mobile}}" id="example-alter-mobile-input">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="example-mother-name-input" class="col-form-label">اسم الام</label>
                                <input class="form-control" name="mother_name" value="{{$client->mother_name}}" type="text" id="example-mother-name-input">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="example-phone-input" class="col-form-label">تليفون ارضي</label>
                                <input class="form-control" name="phone" data-parsley-type="digits"	 value="{{$client->phone}}" type="phone" id="example-phone-input">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="example-number-input" class="col-form-label">رقم الواتس اب</label>
                                <input class="form-control" name="whatsapp_number" data-parsley-type="digits" data-parsley-length="[11, 11]"	 value="{{$client->whatsapp_number}}" type="text" id="example-number-input">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="example-available-calling-time-input" class="col-form-label">الوقت المتاح للاتصال</label>
                                <input class="form-control" name="available_calling_time" value="{{$client->available_calling_time}}" type="text" id="example-available-calling-time-input">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="example-submitted-value-input" class="col-form-label">المقدم</label>
                                <input class="form-control" value="{{$client->submitted_value}}" data-parsley-type="digits" min="0"	 name="submitted_value" type="number" id="example-submitted-value-input" required>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="example-repayment-date-input" class="col-form-label">ميعاد السداد</label>
                                <select class="form-control select2 select2-hidden-accessible" name="repayment_date" style="width: 100%;" tabindex="-1" aria-hidden="true" required>
                                    <option value="1" <?php if($client->repayment_date == 1) echo "selected"; ?>>1</option>
                                    <option value="5" <?php if($client->repayment_date == 5) echo "selected"; ?>>5</option>
                                    <option value="10" <?php if($client->repayment_date == 10) echo "selected"; ?>>10</option>
                                    <option value="15" <?php if($client->repayment_date == 15) echo "selected"; ?>>15</option>
                                    <option value="20" <?php if($client->repayment_date == 20) echo "selected"; ?>>20</option>
                                    <option value="25" <?php if($client->repayment_date == 25) echo "selected"; ?>>25</option>
                                    <option value="28" <?php if($client->repayment_date == 28) echo "selected"; ?>>28</option>

                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="example-follower-name-input" class="col-form-label">اسم المفوض</label>
                                <input class="form-control" value="{{$client->follower_name}}" name="follower_name" type="text" id="example-follower-name-input">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="example-follower-national-id-input" class="col-form-label"> الرقم القومي للمفوض</label>
                                <input class="form-control" value="{{$client->follower_national_id}}" name="follower_national_id" type="text" id="example-follower-national-id-input">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="example-follower-phone-input" class="col-form-label">تليفون المفوض</label>
                                <input class="form-control" value="{{$client->follower_phone}}" data-parsley-type="digits"	data-parsley-length="[11, 11]" name="follower_phone" type="phone" id="example-follower-phone-input">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="example-follower-address-input" class="col-form-label">عنوان المفوض</label>
                                <input class="form-control" value="{{$client->follower_address}}" name="follower_address" type="text" id="example-follower-address-input">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="example-follower-floor-number-input" class="col-form-label">رقم طابق المفوض</label>
                                <input class="form-control" value="{{$client->follower_floor_number}}" data-parsley-type="digits"	 name="follower_floor_number" type="number" id="example-follower-floor-number-input">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="example-follower-flat-number-input" class="col-form-label">رقم شقة المفوض</label>
                                <input class="form-control" value="{{$client->follower_flat_number}}" data-parsley-type="digits"	 name="follower_flat_number" type="number" id="example-follower-flat-number-input">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="example-follower-area-input" class="col-form-label">منطقة المفوض</label>
                                <input class="form-control" value="{{$client->follower_area}}" name="follower_area" type="text" id="example-follower-area-input">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="example-follower-fork-name-input" class="col-form-label">عنوان المفوض متفرع من </label>
                                <input class="form-control" value="{{$client->follower_fork_name}}" name="follower_fork_name" type="text" id="example-follower-fork-name-input">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="example-follower-special-mark-input" class="col-form-label">علامة مميزه لعنوان المفوض</label>
                                <input class="form-control" value="{{$client->follower_special_mark}}" name="follower_special_mark" type="text" id="example-special-mark-input">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="example-relation-input" class="col-form-label">صلة القرابة</label>
                                <input class="form-control" value="{{$client->relation}}" name="relation" type="text" id="example-relation-input">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="example-partner-name-input" class="col-form-label">اسم الزوج / الزوجة</label>
                                <input class="form-control" value="{{$client->partner_name}}" name="partner_name" type="text" id="example-partner-name-input">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="example-partner-company-name-input" class="col-form-label">اسم شركة الزوج / الزوجة</label>
                                <input class="form-control" value="{{$client->partner_company_name}}" name="partner_company_name" type="text" id="example-partner-company-name-input">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="example-partner-work-address-input" class="col-form-label">عنوان عمل الزوج / الزوجة </label>
                                <input class="form-control" value="{{$client->partner_work_address}}" name="partner_work_address" type="text" id="example-partner-work-address-input">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="example-partner-job-input" class="col-form-label">وظيفة الزوج / الزوجة </label>
                                <input class="form-control" value="{{$client->partner_job}}" name="partner_job" type="text" id="example-partner-job-input">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="example-partner-work-area-input" class="col-form-label">منطقة عمل الزوج / الزوجة </label>
                                <input class="form-control" value="{{$client->partner_work_area}}" name="partner_work_area" type="text" id="example-partner-work-area-input">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="example-partner-department-input" class="col-form-label">قسم الزوج / الزوجة </label>
                                <input class="form-control" value="{{$client->partner_department}}" name="partner_department" type="text" id="example-partner-department-input">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="example-partner-mobile-input" class="col-form-label">تليفون الزوج / الزوجة </label>
                                <input class="form-control" value="{{$client->partner_mobile}}" data-parsley-type="digits"	data-parsley-length="[11, 11]" name="partner_mobile" type="phone" id="example-partner-mobile-input">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="example-status-input" class="col-form-label">الحالة</label>
                                <select class="form-control select2 select2-hidden-accessible" name="status" style="width: 100%;" tabindex="-1" aria-hidden="true" required>
                                    <option value="1" <?php if($client->status == 1) echo "selected"; ?>>مفعل</option>
                                    <option value="0" <?php if($client->status == 0) echo "selected"; ?>>معطل</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="example-image-input" class="col-form-label">صورة</label>
                                <input class="form-control" name="image" type="file" id="example-image-input">
                                <img src="{{$client->image}}" alt="client image" class="img-responsive" style="width: 100px;">

                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <input type="submit" class="btn btn-blue" value="حفظ">
                                <input type="hidden" name="id" value="{{$client->id}}">

                            </div>
                        </div>

                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
            </form>
            <!-- /.box-body -->
        </div>
    </section>
</div>
@endsection
@section('scripts')
@endsection

