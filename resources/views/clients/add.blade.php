@extends('layouts.master')
@section('title')
اضافة عميل
@endsection
@section('styles')
@endsection
@section('modals')
@endsection
@section('content')
    <div class="content-wrapper" style="min-height: 1295.8px;">
        <section class="content">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">اضافة عميل جديد</h3>
                </div>
                <!-- /.box-header -->
                <form action="{{route('client.storeAdding')}}" method="post" data-parsley-validate="" novalidate class="error" enctype="multipart/form-data">
                    @csrf
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="example-name-input" class="col-form-label">الاسم</label>
                                    <input class="form-control" value="{{old('name')}}" name="name" type="text" id="example-name-input" required>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="example-name-input" class="col-form-label">البريد الالكتروني</label>
                                    <input class="form-control" value="{{old('email')}}" name="email" type="email" id="example-email-input">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="example-name-input" class="col-form-label">كلمة المروور</label>
                                    <input class="form-control" name="password" data-parsley-type="digits" data-parsley-minlength="6"
                                           type="password" id="example-password-input" required>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="example-national-id-input" class="col-form-label">الرقم القومي</label>
                                    <input class="form-control" value="{{old('national_id')}}" name="national_id" data-parsley-length="[14, 14]"	 data-parsley-type="digits" type="text" id="example-name-input" required>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="example-card-code-input" class="col-form-label">كود الكارته</label>
                                    <input class="form-control" value="{{old('card_code')}}" name="card_code" type="text" id="example-card_code-input" required>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="example-gender-input" class="col-form-label">الجنس</label>
                                    <select class="form-control select2 select2-hidden-accessible" name="gender" style="width: 100%;" tabindex="-1" aria-hidden="true" required>
                                            <option value="ذكر">ذكر</option>
                                            <option value="انثي">انثي</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="example-social-status-input" class="col-form-label">الحالة الاجتماعية</label>
                                    <select class="form-control select2 select2-hidden-accessible" name="social_status" style="width: 100%;" tabindex="-1" aria-hidden="true" required>
                                        <option value="اعزب">اعزب/عزبة</option>
                                        <option value="متزوج">متزوج / متزوجة</option>
                                        <option value="ارمل">ارمل / ارملة</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="example-address-input" class="col-form-label">العنوان</label>
                                    <input class="form-control" value="{{old('address')}}" name="address" type="text" id="example-address-input" required>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="example-floor-number-input" class="col-form-label">رقم الطابق</label>
                                    <input class="form-control" value="{{old('floor_number')}}" name="floor_number" data-parsley-type="digits" type="number" min="0" id="example-floor-number-input" required>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="example-flat-number-input" class="col-form-label">رقم الشقة</label>
                                    <input class="form-control" value="{{old('flat_number')}}" required name="flat_number" data-parsley-type="digits" type="number" min="0" id="example-flat-number-input">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="example-area-input" class="col-form-label">المنطقة</label>
                                    <input class="form-control" value="{{old('area')}}" name="area" type="text" id="example-area-input" required>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="example-fork-name-input" class="col-form-label">متفرع من</label>
                                    <input class="form-control" value="{{old('fork_name')}}" name="fork_name" type="text" id="example-fork-name-input" required>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="example-special-mark-input" class="col-form-label">علامة مميزة</label>
                                    <input class="form-control" value="{{old('special_mark')}}" required name="special_mark" type="text" id="example-special-mark-input">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="example-number-input" class="col-form-label">المدينة</label>
                                    <select class="form-control select2 select2-hidden-accessible" name="city_id" style="width: 100%;" tabindex="-1" aria-hidden="true" required>
                                        @foreach($cities as $city)
                                            <option value="{{$city->id}}">{{$city->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="example-company-input" class="col-form-label">الشركة</label>
                                    <select class="form-control select2 select2-hidden-accessible" name="company_id" style="width: 100%;" tabindex="-1" aria-hidden="true" required>
                                        @foreach($companies as $company)
                                            <option value="{{$company->id}}">{{$company->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="example-company-input" class="col-form-label">المحصلون</label>
                                    <select class="form-control select2 select2-hidden-accessible" multiple name="collectors[]" style="width: 100%;" tabindex="-1" aria-hidden="true" required>
                                        @foreach($collectors as $collector)
                                            <option value="{{$collector->id}}">{{$collector->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="example-company-input" class="col-form-label">عملاء ذات صلة</label>
                                    <select class="form-control select2 select2-hidden-accessible" multiple name="  clients[]" style="width: 100%;" tabindex="-1" aria-hidden="true">
                                        @foreach($clients as $client)
                                            <option value="{{$client->id}}">{{$client->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="example-job-input" class="col-form-label">الوظيفة</label>
                                    <input class="form-control" value="{{old('job')}}" name="job" type="text" id="example-job-input" required>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="example-department-input" class="col-form-label">القسم</label>
                                    <input class="form-control" value="{{old('department')}}" name="department" type="text" id="example-department-input">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="example-customer-number-input" class="col-form-label">رقم العميل</label>
                                    <input class="form-control" value="{{old('customer_number')}}" name="customer_number" type="text" id="example-customer-number-input">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="example-salary-input" class="col-form-label">المرتب</label>
                                    <input class="form-control" value="{{old('salary')}}" name="salary" data-parsley-type="digits" type="number" min="0" id="example-salary-input">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="example-facebook-link-input" class="col-form-label">رابط facebook</label>
                                    <input class="form-control" value="{{old('facebook_link')}}" name="facebook_link" data-parsley-type="url" type="url" id="example-facebook-link-input">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="example-mobileinput" class="col-form-label">التليفون</label>
                                    <input class="form-control" value="{{old('mobile')}}" name="mobile" data-parsley-type="digits" data-parsley-length="[11, 11]"	  type="text" id="example-mobile-input" required>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="example-alter-mobile-input" class="col-form-label"> التليفون البديل</label>
                                    <input class="form-control" value="{{old('alter_mobile')}}" name="alter_mobile" data-parsley-length="[11, 11]"	 type="text" id="example-alter-mobile-input">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="example-mother-name-input" class="col-form-label">اسم الام</label>
                                    <input class="form-control" value="{{old('mother_name')}}" name="mother_name" type="text" id="example-mother-name-input">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="example-phone-input" class="col-form-label">تليفون ارضي</label>
                                    <input class="form-control" value="{{old('phone')}}" name="phone" type="phone" id="example-phone-input">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="example-number-input" class="col-form-label">رقم الواتس اب</label>
                                    <input class="form-control" value="{{old('whatsapp_number')}}" name="whatsapp_number" data-parsley-type="digits"	 data-parsley-length="[11, 11]"	 type="text" id="example-number-input">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="example-available-calling-time-input" class="col-form-label">الوقت المتاح للاتصال</label>
                                    <input class="form-control" value="{{old('available_calling_time')}}" name="available_calling_time" type="text" id="example-available-calling-time-input">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="example-image-input" class="col-form-label">صورة</label>
                                    <input class="form-control" name="image" type="file" id="example-image-input">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="example-submitted-value-input" class="col-form-label">المقدم</label>
                                    <input class="form-control" value="{{old('submitted_value')}}" name="submitted_value" min="0" data-parsley-type="digits" type="number" id="example-submitted-value-input" required>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="example-repayment-date-input" class="col-form-label">ميعاد السداد</label>
                                    <select class="form-control select2 select2-hidden-accessible" name="repayment_date" style="width: 100%;" tabindex="-1" aria-hidden="true" required>
                                        <option value="1">1</option>
                                        <option value="5">5</option>
                                        <option value="10">10</option>
                                        <option value="15">15</option>
                                        <option value="20">20</option>
                                        <option value="25">25</option>
                                        <option value="28">28</option>

                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="auto_buy_option" class="col-form-label">الشراء اتوماتيك بدون كود تاكيد</label>
                                    <select class="form-control select2 select2-hidden-accessible" name="auto_buy_option" style="width: 100%;" tabindex="-1" aria-hidden="true" required>
                                        <option value="0">ﻻ</option>
                                        <option value="1">نعم</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="example-follower-name-input" class="col-form-label">اسم المفوض</label>
                                    <input class="form-control" value="{{old('follower_name')}}" name="follower_name" type="text" id="example-follower-name-input">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="example-follower-national-id-input" class="col-form-label"> الرقم القومي للمفوض</label>
                                    <input class="form-control" value="{{old('follower_national_id')}}" name="follower_national_id" type="text" id="example-follower-national-id-input">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="example-follower-phone-input" class="col-form-label">تليفون المفوض</label>
                                    <input class="form-control" value="{{old('follower_phone')}}" name="follower_phone" data-parsley-type="digits" data-parsley-length="[11, 11]"	type="phone" id="example-follower-phone-input">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="example-follower-address-input" class="col-form-label">عنوان المفوض</label>
                                    <input class="form-control" value="{{old('follower_address')}}" name="follower_address" type="text" id="example-follower-address-input">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="example-follower-floor-number-input" class="col-form-label">رقم طابق المفوض</label>
                                    <input class="form-control" value="{{old('follower_floor_number')}}" name="follower_floor_number" data-parsley-type="digits"	 type="number" id="example-follower-floor-number-input">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="example-follower-flat-number-input" class="col-form-label">رقم شقة المفوض</label>
                                    <input class="form-control" value="{{old('follower_flat_number')}}" name="follower_flat_number" data-parsley-type="digits"	 type="number" id="example-follower-flat-number-input">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="example-follower-area-input" class="col-form-label">منطقة المفوض</label>
                                    <input class="form-control" value="{{old('follower_area')}}" name="follower_area" type="text" id="example-follower-area-input">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="example-follower-fork-name-input" class="col-form-label">عنوان المفوض متفرع من </label>
                                    <input class="form-control" value="{{old('follower_fork_name')}}" name="follower_fork_name" type="text" id="example-follower-fork-name-input">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="example-follower-special-mark-input" class="col-form-label">علامة مميزه لعنوان المفوض</label>
                                    <input class="form-control" value="{{old('follower_special_mark')}}" name="follower_special_mark" type="text" id="example-special-mark-input">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="example-relation-input" class="col-form-label">صلة القرابة</label>
                                    <input class="form-control" value="{{old('relation')}}" name="relation" type="text" id="example-relation-input">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="example-partner-name-input" class="col-form-label">اسم الزوج / الزوجة</label>
                                    <input class="form-control" value="{{old('partner_name')}}" name="partner_name" type="text" id="example-partner-name-input">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="example-partner-company-name-input" class="col-form-label">اسم شركة الزوج / الزوجة</label>
                                    <input class="form-control" value="{{old('partner_company_name')}}" name="partner_company_name" type="text" id="example-partner-company-name-input">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="example-partner-work-address-input" class="col-form-label">عنوان عمل الزوج / الزوجة </label>
                                    <input class="form-control" value="{{old('partner_work_address')}}" name="partner_work_address" type="text" id="example-partner-work-address-input">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="example-partner-job-input" class="col-form-label">وظيفة الزوج / الزوجة </label>
                                    <input class="form-control" value="{{old('partner_job')}}" name="partner_job" type="text" id="example-partner-job-input">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="example-partner-work-area-input" class="col-form-label">منطقة عمل الزوج / الزوجة </label>
                                    <input class="form-control" value="{{old('partner_work_area')}}" name="partner_work_area" type="text" id="example-partner-work-area-input">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="example-partner-department-input" class="col-form-label">قسم الزوج / الزوجة </label>
                                    <input class="form-control" value="{{old('partner_department')}}" name="partner_department" type="text" id="example-partner-department-input">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="example-partner-mobile-input" class="col-form-label">تليفون الزوج / الزوجة </label>
                                    <input class="form-control" value="{{old('partner_mobile')}}" name="partner_mobile" data-parsley-type="digits" data-parsley-length="[11, 11]"	type="phone" id="example-partner-mobile-input">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="example-status-input" class="col-form-label">الحالة</label>
                                    <select class="form-control select2 select2-hidden-accessible" name="status" style="width: 100%;" tabindex="-1" aria-hidden="true" required>
                                        <option value="1">مفعل</option>
                                        <option value="0">معطل</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="example-number-input" class="col-form-label"></label>
                                    <div class="demo-checkbox">
                                        <input type="submit" class="btn btn-blue" value="حفظ">
                                    </div>
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                </form>
                <!-- /.box-body -->
            </div>
        </section>
    </div>
@endsection
@section('scripts')
@endsection

