@extends('layouts.master')
@section('title')
    جدول الاستحقاقات
@endsection
@section('styles')
    <style>
        #admins tbody tr {
            word-break: break-all;
        }
    </style>
@endsection
@section('modals')
@endsection
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">

                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">جدول الاستحقاقات</h3>
                        </div>
                        <div class="filter">
                            <h6 style="font-style: italic;margin-right: 15px;">بحث بتاريخ التحصيل</h6>
                            <form action="/client/filter-next-premiums" method="get">
                                <div class="col-md-12 row">
                                    <div class="col-md-5 form-group">
                                        <label for="">من</label>
                                        <input type="date" value="{{(Session::has('from') ? Session::get('from'): "")}}" class="form-control" name="from">
                                    </div>
                                    <div class="col-md-5 form-group">
                                        <label for="">الي</label>
                                        <input type="date" value="{{(Session::has('to') ? Session::get('to'): "")}}" class="form-control" name="to">
                                        <input type="hidden" value="{{$id}}" name="id">
                                    </div>
                                    <div class="col-md-2" style="padding-top: 25px;">
                                        <label for=""></label>
                                        <input type="submit" value="بحث" class="btn btn-md btn-success">
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="premiums" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th>المبلغ المدفوع</th>
                                    <th>المبلغ المستحق</th>
                                    <th>اجمالي المبلغ المستحق</th>
                                    <th>رقم الشهر</th>
                                    <th> الحالة</th>
                                    <th>كود العملية</th>
                                    <th>تاريخ التحصيل</th>
                                    <th>المحصل</th>
                                    <th>العمليات</th>
                                </tr>
                                </thead>
                            </table>


                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
@section('scripts')
    <script type="text/javascript">

        $('#premiums').DataTable({
            destroy: true,
            processing: true,
            serverSide: true,
            order: [[6, "asc"]],
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'copyHtml5',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3,4,5,6,7 ]
                    }
                },
                {
                    extend: 'excelHtml5',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3,4,5,6,7 ]
                    }
                },
                {
                    extend: 'pdfHtml5',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3,4,5,6,7 ]
                    }
                },
                {
                    extend: 'print',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3,4,5,6,7 ]
                    }
                },

            ],
            select: true,
            ajax: {
                "url": '/client/fetchNextPremiums/'+"{{$id}}",
                "type": 'GET',
            },
            columns: [
                {data: 'paid_amount', name: 'paid_amount'},
                {data: 'remaining_amount', name: 'remaining_amount'},
                {data: 'total_remaining_amount', name: 'total_remaining_amount'},
                {data: 'premium_month_number', name: 'premium_month_number'},
                {data: 'status', name: 'status'},
                {data: 'code', name: 'code'},
                {data: 'collect_date', name: 'collect_date'},
                {data: 'collector', name: 'collector'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ],
            aLengthMenu: [
                [10, 50, 100, 200, -1],
                [10, 50, 100, 200, "All"]
            ],
            "oLanguage": {
                "sSearch": "بحث : "
            }
        });

        async function pay(id) {

            const ipAPI = 'https://api.ipify.org?format=json'

            const inputValue = fetch(ipAPI)
                .then(response => response.json())
                .then(data => data.ip)

            const {value: paidAmount} = await Swal.fire({
                title: 'برجاء ادخال القيمة المدفوعة ؟',
                input: 'number',
                inputValue: inputValue,
                showCancelButton: true,
                inputValidator: (value) => {
                    if (!value || value <= 0) {
                        return 'برجاء ادخال القيمة المدفوعة بشكل صحيح'
                    }

                }
            })

            if (paidAmount) {
                Swal.fire({
                    title: 'هل انت متاكد من الدفع ؟',
                    text: "ﻻ تستطيع استعاده القيمة بعد الدفع",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'نعم',
                    cancelButtonText: 'ﻻ',
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            'url':'/purchases/premiums/pay',
                            'type':'POST',
                            'data':{
                                'id' : id,
                                'paidAmount' : paidAmount,
                                "_token": "{{ csrf_token() }}",
                            },
                            success:function(data){
                                Swal.fire({
                                    type: data.status,
                                    title: data.title,
                                    text: data.msg,
                                    footer: '<a href="/purchases/premiums/download-bill-pay/'+data.id+'">Download Bill </a>'
                                });

                            },
                            error : function(){
                            }
                        });
                    }
                })
            }
        }

        // relay remaining amount
        function relayRemainingAmount(id,purchase_id) {

            Swal.fire({
                title: 'هل انت متاكد من ترحيل المبلغ المتبقي للشهر القادم ؟',
                text: "ﻻ تستطيع اعاده القيمة بعد الترحيل",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'نعم',
                cancelButtonText: 'ﻻ',
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        'url':'/purchases/premiums/relay-remaining-amount',
                        'type':'POST',
                        'data':{
                            'id' : id,
                            'purchase_id' : purchase_id,
                            "_token": "{{ csrf_token() }}",
                        },
                        success:function(data){
                            Swal.fire({
                                type: data.status,
                                title: data.title,
                                text: data.msg,
                            });
                            $('#premiums').DataTable().ajax.reload();
                        },
                        error : function(){
                        }
                    });
                }
            })
        }

        // relay remaining amount to new month
        function relayRemainingAmountToNewMonth(id,purchaseId) {

            Swal.fire({
                title: 'هل انت متاكد من ترحيل المبلغ المتبقي لشهر اضافي ؟',
                text: "ﻻ تستطيع اعاده القيمة بعد الترحيل",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'نعم',
                cancelButtonText: 'ﻻ',
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        'url':'/purchases/premiums/relay-remaining-amount-to-new-month',
                        'type':'POST',
                        'data':{
                            'id' : id,
                            'purchase_id' : purchaseId,
                            "_token": "{{ csrf_token() }}",
                        },
                        success:function(data){
                            Swal.fire({
                                type: data.status,
                                title: data.title,
                                text: data.msg,
                            });
                            $('#premiums').DataTable().ajax.reload();
                        },
                        error : function(){
                        }
                    });
                }
            })
        }

    </script>
@endsection

