<!doctype html>
<html lang="en" dir="rtl">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>مبيعة رقم : {{$purchase->id}}</title>

    <style type="text/css">
        @page {
            margin: 0px;
        }

        body {
            margin: 0px;
        }

        * {
            font-family: 'dejavu sans', sans-serif;
        }

        a {
            color: #fff;
            text-decoration: none;
        }

        table {
            font-size: x-small;
        }

        tfoot tr td {
            font-weight: bold;
            font-size: x-small;
        }

        .invoice h3 {
            margin-left: 15px;
        }

        .information {
            background-color: #60A7A6;
            color: #FFF;
            height: 50px !important;
        }

        .information .logo {
            margin: 5px;
        }

        .information table {
            padding: 10px;
        }

        /*body {*/
        /*    font-family: DejaVu Sans, sans-serif !important;*/
        /*}*/
        @page {
            size: A4;
            margin: 0;
        }
        @media print {
            html, body {
                width: 210mm;
                height: 297mm;
            }
            /* ... the rest of the rules ... */
        }
    </style>

</head>
<body dir="rtl">

<div class="information">
    <table width="100%">
        <tr>
            <td align="left" style="width: 40%;">
                <h3>اسم العميل : {{$client->name}}</h3>
                <h3>التاريخ : {{date('Y-m-d')}}</h3>
                <h3>حالة الدفع : تم الدفع</h3>
            </td>
            <td align="right" style="width: 40%;">

                <h3>{{$company->name}} شركة </h3>
                <h3>
                    كود العميل : {{$client->customer_number}}
                </h3>
                <h3>
                    كود الكارت : {{$client->user->card_code}}
                </h3>
                <h3>
                   ت  : {{$company->phone}}
                </h3>
            </td>
        </tr>

    </table>
</div>


<br/>

<div class="invoice">
    <h3>مبيعة رقم : {{$purchase->id}}</h3>
    <h3>المبلغ المدفوع : {{$purchase->paid_amount}}</h3>
    <h3>المبلغ المتبقي : {{$purchase->remaining_amount}}</h3>
    <table width="100%">
        <thead>
        <tr>
            <th>اسم المنتج</th>
            <th>سعر المنتج</th>
            <th>الكمية</th>
            <th>اجمالي السعر</th>
        </tr>
        </thead>
        <tbody>
        @foreach($purchase_products as $product)
            <tr>
                <td>{{$product->commodity_name}}</td>
                <td>{{$product->price}}</td>
                <td>{{$product->quantity}}</td>
                <td align="left">{{$product->total_price}}</td>
            </tr>
        @endforeach
        </tbody>

    </table>
</div>

<div class="invoice">
    <h3>تقرير الاقساط</h3>
    <table width="100%">
        <thead>
        <tr>
            <th>كود المنتج</th>
            {{--            <th>Count</th>--}}
            <th>المبلغ المدفوع</th>
            <th>المبلغ المتبقي</th>
            <th>تاريخ الاستحقاق</th>
            <th>تاريخ الدفع</th>
            <th>اجمالي السعر</th>
        </tr>
        </thead>
        <tbody>
        @foreach($premiums as $premium)
            <tr>
                <td>{{$premium->code}}</td>
                <td>{{$premium->paid_amount}}</td>
                <td>{{$premium->remaining_amount}}</td>
                <td>{{$premium->paid_amount != 0 ? date('Y-m-d',strtotime($premium->updated_at)) : '-'}}</td>
                <td>{{$premium->collect_date}}</td>
                <td align="left">{{$premium->paid_amount}}</td>
            </tr>
        @endforeach
        </tbody>
        <tfoot>
        <tr>
            <td align="left">اجمالي المبلغ المدفوع</td>
            <td align="left" class="gray">{{$total_paid}}</td>
            <td align="left">اجمالي المبلغ المتبقي</td>
            <td align="left" class="gray">{{$total_remaining}}</td>
        </tr>
        <tr>
            <td align="right">الحد الائتماني للشراء</td>
            <td align="left" class="gray">{{$client->fixed_max_value}}</td>
            <td align="right">المبلغ المتاح للشراء</td>
            <td align="left" class="gray">{{$client->max_value}}</td>
        </tr>
        </tfoot>
    </table>
</div>
<div style="margin-left: 20px">
    <h5>توقيع</h5>
    <p>.................................</p>
</div>

</body>
</html>
