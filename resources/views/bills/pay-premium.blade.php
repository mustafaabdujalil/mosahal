<!doctype html>
<html lang="en" dir="rtl">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>قسط رقم {{$premium->id}}</title>

    <style type="text/css">
        @page {
            margin: 0px;
        }

        body {
            margin: 0px;
        }

        * {
            font-family: 'dejavu sans', sans-serif;
        }

        a {
            color: #fff;
            text-decoration: none;
        }

        table {
            font-size: x-small;
        }

        tfoot tr td {
            font-weight: bold;
            font-size: x-small;
        }

        .invoice h3 {
            margin-left: 15px;
        }

        .information {
            background-color: #60A7A6;
            color: #FFF;
            height: 50px !important;
        }

        .information .logo {
            margin: 5px;
        }

        .information table {
            padding: 10px;
        }

        /*body {*/
        /*    font-family: DejaVu Sans, sans-serif !important;*/
        /*}*/
        @page {
            size: A4;
            margin: 0;
        }
        @media print {
            html, body {
                width: 210mm;
                height: 297mm;
            }
            /* ... the rest of the rules ... */
        }
    </style>

</head>
<body dir="rtl">

<div class="information">
    <table width="100%">
        <tr>
            <td align="left" style="width: 40%;">
                <h3>اسم العميل : {{$client->name}}</h3>
                <h3>تاريخ: {{date('Y-m-d')}}</h3>
                <h3>حالة الدفع: تم الدفع</h3>
            </td>
            <td align="right" style="width: 40%;">

                <h3>{{$company->name}} شركة</h3>
                <h3>
                    كود العميل : {{$client->customer_number}}
                </h3>
                <h3>
                    كود الكارت : {{$client->user->card_code}}
                </h3>
                <h3>
                   ت : {{$company->phone}} - {{$company->mobile}}
                </h3>
            </td>
        </tr>

    </table>
</div>


<br/>

<div class="invoice">
    <h3>قسط رقم {{$premium->id}}</h3>
    <table width="100%">
        <thead>
        <tr>
            <th>كود المنتج</th>
            {{--            <th>Count</th>--}}
            <th>المبلغ المدفوع</th>
            <th>قيمة اشتراك الكارنية</th>
            <th>المبلغ المتبقي</th>
            <th>تاريخ الاستحقاق</th>
            <th>تاريخ الدفع</th>
            <th>اجمالي السعر</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>{{$premium->code}}</td>
{{--            <td>1</td>--}}
            <td>{{$premium->paid_amount - $setting->subscription_value}}</td>
            <td>{{isset($setting->subscription_value) ? $setting->subscription_value : 0}}</td>
            <td>{{$premium->remaining_amount}}</td>
            <td>{{date('Y-m-d',strtotime($premium->updated_at))}}</td>
            <td>{{$premium->collect_date}}</td>
            <td align="left">{{$premium->paid_amount}}</td>
        </tr>
        </tbody>
    </table>
    <hr>
    <h3 style="text-align: center">الاقساط المتبقية</h3>
    <table width="100%">
        <thead>
        <tr>
            <th>كود القسط</th>
            <th>Count</th>
            <th>المبلغ المدفوع</th>
            <th>المبلغ المتبقي</th>
            <th>تاريخ الاستحقاق</th>
            <th>تاريخ الدفع</th>
            <th>اجمالي السعر</th>
        </tr>
        </thead>
        <tbody>
            @foreach($collective_premiums as $prem)
                <tr>
                    <td>{{$prem->code}}</td>
                    <td>1</td>
                    <td>{{$prem->paid_amount}}</td>
                    <td>{{$prem->remaining_amount}}</td>
                    <td>{{date('Y-m-d',strtotime($prem->updated_at))}}</td>
                    <td>{{$prem->collect_date}}</td>
                    <td align="left">{{$prem->total_remaining_amount}}</td>
                </tr>
            @endforeach
        </tbody>

        <tfoot>
        <tr>
            <td colspan="1"></td>
            <td align="left">اجمالي المتبقي</td>
            <td align="left" class="gray">{{$sum_collective_premiums}}</td>
        </tr>
        </tfoot>
    </table>

    <h5>توقيع</h5>
    <p>.................................</p>

</div>

</body>
</html>
