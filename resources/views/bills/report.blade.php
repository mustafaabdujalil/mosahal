<!doctype html>
<html lang="en" dir="rtl">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>اسم العميل : {{$client->name}}</title>

    <style type="text/css">
        @page {
            margin: 0px;
        }

        body {
            margin: 0px;
        }

        * {
            font-family: 'dejavu sans', sans-serif;
        }

        a {
            color: #fff;
            text-decoration: none;
        }

        table {
            font-size: x-small;
        }

        tfoot tr td {
            font-weight: bold;
            font-size: x-small;
        }

        .invoice h3 {
            margin-left: 15px;
        }

        .information {
            background-color: #60A7A6;
            color: #FFF;
            height: 50px !important;
        }

        .information .logo {
            margin: 5px;
        }

        .information table {
            padding: 10px;
        }

        /*body {*/
        /*    font-family: DejaVu Sans, sans-serif !important;*/
        /*}*/

        @page {
            size: A4;
            margin: 0;
        }
        @media print {
            html, body {
                width: 210mm;
                height: 297mm;
            }
            /* ... the rest of the rules ... */
        }
    </style>

</head>
<body dir="rtl">

<div class="information">
    <table width="100%">
        <tr>
            <td align="left" style="width: 40%;">
                <h3>اسم العميل : {{$client->name}}</h3>
                <h3>التاريخ: {{date('Y-m-d')}}</h3>
                <h3>الغرص: تقرير شامل </h3>
            </td>
            <td align="right" style="width: 40%;">

                <h3>{{$company->name}} شركة</h3>
                <h3>
                    كود العميل : {{$client->customer_number}}
                </h3>
                <h3>
                    كود الكارت : {{$client->user->card_code}}
                </h3>
                <h3>
                    ت :    {{$company->phone}}
                </h3>
            </td>
        </tr>
    </table>
</div>
<div class="invoice">
    <h3>تقرير الاقساط</h3>
    <table width="100%">
        <thead>
        <tr>
            <th>كود المنتج</th>
{{--            <th>Count</th>--}}
            <th>المبلغ المدفوع</th>
            <th>المبلغ المتبقي</th>
            <th>تاريخ الاستحقاق</th>
            <th>تاريخ الدفع</th>
            <th>اجمالي السعر</th>
        </tr>
        </thead>
        <tbody>
        @foreach($premiums as $premium)
            <tr>
                <td>{{$premium->code}}</td>
                <td>{{$premium->paid_amount}}</td>
                <td>{{$premium->remaining_amount}}</td>
                <td>{{$premium->paid_amount != 0 ? date('Y-m-d',strtotime($premium->updated_at)) : '-'}}</td>
                <td>{{$premium->collect_date}}</td>
                <td align="left">{{$premium->paid_amount}}</td>
            </tr>
        @endforeach
        </tbody>

        <tfoot>
        <tr>
            <td align="left">اجمالي المبلغ المدفوع</td>
            <td align="left" class="gray">{{$total_paid}}</td>
            <td align="left">اجمالي المبلغ المتبقي</td>
            <td align="left" class="gray">{{$total_remaining_amount}}</td>
        </tr>
        <tr>
            <td align="right">الحد الائتماني للشراء</td>
            <td align="left" class="gray">{{$client->fixed_max_value}}</td>
            <td align="right">المبلغ المتاح للشراء</td>
            <td align="left" class="gray">{{$client->max_value}}</td>
        </tr>
        </tfoot>
    </table>
    <h3>المشتريات</h3>
    <table width="100%">
        <thead>
        <tr>
            <th>اسم المنتج</th>
            <th>السعر</th>
            <th>اجمالي الكمية</th>
            <th>الكمية الفعلية</th>
            <th>الكمية المسترجعه</th>
            <th>اجمالي السعر</th>
            <th>تاريخ الشراء</th>
        </tr>
        </thead>
        <tbody>
        @foreach($bought_products as $product)
            <tr>
                <td>{{$product->commodity_name}}</td>
                <td>{{$product->price}}</td>
                <td>{{$product->recovery_count + $product->quantity}}</td>
                <td>{{$product->quantity}}</td>
                <td>{{$product->recovery_count}}</td>
                <td>{{$product->price * $product->quantity }}</td>
                <td>{{date('Y-m-d',strtotime($product->created_at))}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <h5>توقيع</h5>
    <p>.................................</p>
</div>


{{--<div class="information" style="position: absolute; bottom: 0;">--}}
{{--    <table width="100%">--}}
{{--        <tr>--}}
{{--            <td align="left" style="width: 50%;">--}}
{{--                &copy; {{ date('Y') }}  - جميع الحقوق محفوظه لدي--}}
{{--            </td>--}}
{{--            <td align="right" style="width: 50%;">--}}
{{--                مسهل--}}
{{--            </td>--}}
{{--        </tr>--}}

{{--    </table>--}}
{{--</div>--}}
</body>
</html>
