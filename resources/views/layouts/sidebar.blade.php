<aside class="main-sidebar" style="height:-webkit-fill-available;">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="user-profile treeview">
                <a href="{{route('admin.index')}}">
                    <img src="{{Auth::user()->image}}" alt="user">
                    <span>{{Auth::user()->name}}</span>
                    <span class="pull-right-container">
                <i class="fa fa-angle-left pull-left"></i>
              </span>
                </a>
                <ul class="treeview-menu">
{{--                    <li><a href="javascript:void()">My Profile </a></li>--}}
                    <li>
                        <a class="dropdown-item" href="{{ route('logout') }}"
                           onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>
                        <form action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>

                    </li>
                </ul>
            </li>
            <li class="nav-devider"></li>
{{--            <li class="header nav-small-cap"></li>--}}
            @if(Auth::user()->role == 'Admin')
                <li class="active">
                    <a href="{{route('admin.index')}}">
                        <i class="fa fa-dashboard"></i><span>الادمن</span>
                        <span class="pull-right-container"></span>
                    </a>
                </li>
                <li>
                    <a href="{{route('supplier.index')}}">
                        <i class="fa fa-pie-chart"></i> <span>منافذ البيع</span>
                        <span class="pull-right-container"></span>
                    </a>
                </li>
                <li>
                    <a href="{{route('store.index')}}">
                        <i class="fa fa-money"></i> <span>الخزينة</span>
                        <span class="pull-right-container"></span>
                    </a>
                </li>
                <li>
                    <a href="{{route('collector.index')}}">
                        <i class="fa fa-edit"></i> <span>المحصلين</span>
                        <span class="pull-right-container"></span>
                    </a>
                </li>
                <li>
                    <a href="{{route('company.index')}}">
                        <i class="fa fa-database"></i> <span>الشركات</span>
                        <span class="pull-right-container"></span>
                    </a>
                </li>
                <li>
                    <a href="{{route('settings.index')}}">
                        <i class="fa fa-circle"></i> <span>الاعدادات</span>
                        <span class="pull-right-container"></span>
                    </a>
                </li>
            @endif
            @if(Auth::user()->role == 'Admin' || Auth::user()->role == 'Collector')
                <li>
                    <a href="{{route('client.index')}}">
                        <i class="fa fa-user"></i> <span>العملاء</span>
                        <span class="pull-right-container"></span>
                    </a>
                </li>
            @endif

            @if(Auth::user()->role == 'Supplier' || Auth::user()->role == 'SupplierEmployee')
                <li>
                    <a href="{{route('client.getClient')}}">
                        <i class="fa fa-user"></i> <span>العملاء</span>
                        <span class="pull-right-container"></span>
                    </a>
                </li>
                <li>
                    <a href="{{route('client.getClientByName')}}">
                        <i class="fa fa-money"></i> <span>تحصيل</span>
                        <span class="pull-right-container"></span>
                    </a>
                </li>
            @endif
            @if(Auth::user()->role == 'Supplier')
                <li>
                    <a href="/supplier/employees/{{Auth::user()->id}}">
                        <i class="fa fa-users"></i> <span>الموظفين</span>
                        <span class="pull-right-container"></span>
                    </a>
                </li>
                <li>
                    <a href="/supplier/branches/{{Auth::user()->id}}">
                        <i class="fa fa-home"></i> <span>الفروع</span>
                        <span class="pull-right-container"></span>
                    </a>
                </li>
                <li>
                    <a href="{{route('store.index')}}">
                        <i class="fa fa-money"></i> <span>الخزينة</span>
                        <span class="pull-right-container"></span>
                    </a>
                </li>
            @endif

            <li>
                <a href="{{route('purchases.index')}}">
                    <i class="fa fa-user"></i> <span>المبيعات</span>
                    <span class="pull-right-container"></span>
                </a>
            </li>
            @if(Auth::user()->role == "Admin")

                <li>
                    <a href="{{route('reports.companies.index')}}">
                        <i class="fa fa-file"></i> <span>تقارير الشركات</span>
                        <span class="pull-right-container"></span>
                    </a>
                </li>
                <li>
                    <a href="{{route('reports.suppliers.index')}}">
                        <i class="fa fa-file"></i> <span>تقارير منافذ البيع</span>
                        <span class="pull-right-container"></span>
                    </a>
                </li>
            @endif
            @if(Auth::user()->role == "Supplier")
                <li>
                    <a href="{{route('reports.suppliers.index')}}">
                        <i class="fa fa-file"></i> <span>تقارير منفذ البيع</span>
                        <span class="pull-right-container"></span>
                    </a>
                </li>
            @endif
        </ul>
    </section>
</aside>
