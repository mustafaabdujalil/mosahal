<!DOCTYPE html>
<html lang="en" dir="rtl">
 <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="{{asset('images/fav.jpeg')}}">
	<title>@yield('title')</title>
	@include('layouts.styles')
</head>
<body class="hold-transition skin-green sidebar-mini">
    @include('layouts.header')
    @include('layouts.sidebar')
    <div class="wrapper">
        @include('layouts.alert')
        @yield('content')
    </div>
    @include('layouts.footer')
    @include('layouts.scripts')
    @yield('modals')
</body>
</html>
