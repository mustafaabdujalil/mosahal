@extends('layouts.master')
@section('title')
    اضافة شركة جديدة
@endsection
@section('styles')
@endsection
@section('modals')
@endsection
@section('content')
    <div class="content-wrapper" style="min-height: 1295.8px;">
        <section class="content">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">اضافة شركة جديد</h3>
                </div>
                <!-- /.box-header -->
                <form action="{{route('company.storeAdding')}}" method="post" data-parsley-validate="" novalidate class="error" enctype="multipart/form-data">
                    @csrf
                    <div class="box-body">
                        <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="example-name-input" class=" col-form-label">الاسم</label>
                                        <input class="form-control" name="name" value="{{old('name')}}" type="text" id="example-name-input" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="example-description-input" class="col-form-label">نبذه عن الشركة</label>
                                        <input class="form-control" name="description" value="{{old('description')}}" type="text" id="example-description-input" required>

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="example-manager-name-input" class="col-form-label">اسم المدير</label>
                                        <input class="form-control" name="manager_name" value="{{old('manager_name')}}" type="text" id="example-manager-name-input" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="example-phone-input" class="col-form-label">التليفون</label>
                                        <input class="form-control" name="phone" type="text" value="{{old('phone')}}" id="example-phone-input" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="example-mobile-input" class="col-form-label">الموبايل</label>
                                        <input class="form-control" name="mobile" data-parsley-type="digits" value="{{old('mobile')}}" data-parsley-length="[11, 11]" type="text" id="example-mobile-input" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label for="example-number-input" class="col-form-label">رقم الواتس اب</label>
                                    <input class="form-control" name="whatsapp_number" value="{{old('whatsapp_number')}}" data-parsley-type="digits" data-parsley-length="[11, 11]" type="text" id="example-number-input">
                                </div>
                                <div class="col-md-6">
                                    <label for="example-address-input" class="col-form-label">العنوان</label>
                                    <input class="form-control" name="address" value="{{old('address')}}" type="text" id="example-address-input" required>
                                </div>
                                <div class="col-md-6">
                                    <label for="example-activity-input" class="col-form-label">النشاط</label>
                                    <input class="form-control" name="activity" value="{{old('activity')}}" type="text" id="example-activity-input" required>
                                </div>
                                <div class="col-md-6">
                                    <label for="example-number-input" class="col-form-label">المدينة</label>
                                    <select class="form-control select2 select2-hidden-accessible" name="city_id" style="width: 100%;" tabindex="-1" aria-hidden="true" required>
                                        @foreach($cities as $city)
                                            <option value="{{$city->id}}">{{$city->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <label for="example-logo-input" class="col-form-label">لوجو الشركة</label>
                                    <input class="form-control" name="logo" type="file" id="example-logo-input" required>
                                </div>
                                <div class="col-md-6">
                                    <label for="example-suppliers-input" class="col-form-label">الموردين</label>
                                    <select class="form-control select2 select2-hidden-accessible" multiple name="suppliers[]" style="width: 100%;" tabindex="-1" aria-hidden="true" required>
                                        @foreach($suppliers as $supplier)
                                            <option value="{{$supplier->id}}">{{$supplier->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <label for="example-premiums-months-input" class="col-form-label">القسط الشهري</label>
                                    <input class="form-control" name="premiums_months" value="{{old('premiums_months')}}" min="1" type="number" data-parsley-type="digits"	 id="example-premiums-months-input" required>
                                </div>
                                <div class="col-md-6">
                                    <label for="example-max-value-input" class="col-form-label">اقصي قيمة للتقسيط</label>
                                    <input class="form-control" name="max_value" min="1" value="{{old('max_value')}}" type="number" data-parsley-type="digits" id="example-max-value-input" required>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="example-number-input" class="col-form-label"></label>
                                        <div class="demo-checkbox">
                                            <input type="submit" class="btn btn-blue" value="حفظ">
                                        </div>
                                    </div>
                                </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                </form>
                <!-- /.box-body -->
            </div>
        </section>
    </div>
@endsection
@section('scripts')
@endsection

