@extends('layouts.master')
@section('title')
    الشركات
@endsection
@section('styles')
    <style>
        #companies tbody tr {
            word-break: break-all;
        }
    </style>
@endsection
@section('modals')
@endsection
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">

                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">جدول الشركات</h3>
                            <a class="btn btn-cyan" style="float: left;" href="{{route('company.addForm')}}">اضف شركة</a>
                        </div>

                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="companies" class="table-bordered">
                                <thead>
                                <tr>
                                    <th>الاسم</th>
                                    <th>نبذه عن الشركة</th>
                                    <th>المدير</th>
                                    <th>الموبايل</th>
                                    <th>التليفون</th>
                                    <th>واتس اب</th>
                                    <th>العنوان</th>
                                    <th>المدينة</th>
                                    <th>الحالة</th>
                                    <th>الصوره</th>
                                    <th>القسط الشهري</th>
                                    <th>اقصي قيمة</th>
                                    <th>النشاط</th>
                                    <th>العمليات</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
@section('scripts')
    <script type="text/javascript">

        $('#companies').DataTable({
            destroy: true,
            processing: true,
            serverSide: true,
            order: [[0, "asc"]],
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'copyHtml5',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11 ]
                    }
                },
                {
                    extend: 'excelHtml5',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11 ]
                    }
                },
                {
                    extend: 'pdfHtml5',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11 ]
                    }
                },
                {
                    extend: 'print',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11 ]
                    }
                },

            ],
            select: true,
            ajax: {
                "url": '{{route("company.fetchCompanies")}}',
                "type": 'GET'
            },
            columns: [
                {data: 'name', name: 'name'},
                {data: 'description', name: 'description'},
                {data: 'manager_name', name: 'manager_name'},
                {data: 'mobile', name: 'mobile'},
                {data: 'phone', name: 'phone'},
                {data: 'whatsapp', name: 'whatsapp'},
                {data: 'address', name: 'address'},
                {data: 'city', name: 'city'},
                {data: 'status', name: 'status'},
                {data: 'logo', name: 'logo'},
                {data: 'premiums_months', name: 'premiums_months'},
                {data: 'max_value', name: 'max_value'},
                {data: 'activity', name: 'activity'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ],
            // aLengthMenu: [
            //     [10, 50, 100, 200, -1],
            //     [10, 50, 100, 200, "All"]
            // ]
            "oLanguage": {
                "sSearch": "بحث : "
            }
        });

        function deleteCompany(id) {
            console.log(id);
            Swal.fire({
                title: 'هل انت متاكد من الحذف ؟',
                text: "ﻻ تستطيع استعاده الشركة بعد الحذف",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'نعم',
                cancelButtonText: 'ﻻ',
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        'url':'/company/delete',
                        'type':'GET',
                        'data':{
                            'id' : id,
                        },
                        success:function(data){
                            location.reload(true);
                        },
                        error : function(){
                        }
                    });
                }
            })

        }

    </script>
@endsection

