@extends('layouts.master')
@section('title')
    اضافة مورد لشركة {{$company->name}}
@endsection
@section('styles')
@endsection
@section('modals')
@endsection
@section('content')
    <div class="content-wrapper" style="min-height: 1295.8px;">
        <section class="content">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">اضافة مورد لشركة {{$company->name}}</h3>
                </div>
                <!-- /.box-header -->
                <form action="{{route('company.supplier.store')}}" method="post" data-parsley-validate="" novalidate class="error" enctype="multipart/form-data">
                    @csrf
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <label for="example-suppliers-input" class="col-form-label">الموردين</label>
                                <select class="form-control select2 select2-hidden-accessible" multiple name="suppliers[]" style="width: 100%;" tabindex="-1" aria-hidden="true" required>
                                    @foreach($suppliers as $supplier)
                                        <option value="{{$supplier->id}}" <?php if(in_array($supplier->id,$exist_suppliers)) echo "selected"; ?>>{{$supplier->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <input type="hidden" name="company_id" value="{{$company->id}}">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="example-number-input" class="col-form-label"></label>
                                    <div class="demo-checkbox">
                                        <input type="submit" class="btn btn-blue" value="حفظ">
                                    </div>
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                </form>
                <!-- /.box-body -->
            </div>
        </section>
    </div>
@endsection
@section('scripts')
@endsection

