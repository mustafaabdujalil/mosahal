@extends('layouts.master')
@section('title')
    تعديل البيانات
@endsection
@section('styles')
@endsection
@section('modals')
@endsection
@section('content')
    <div class="content-wrapper" style="min-height: 1295.8px;">
        <section class="content">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title"> تعديل بيانات {{$company->name}}</h3>
                </div>
                <!-- /.box-header -->
                <form action="{{route('company.storeEdit')}}" method="post" data-parsley-validate="" enctype="multipart/form-data" novalidate class="error">
                    @csrf
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="example-name-input" class=" col-form-label">الاسم</label>
                                    <input class="form-control" name="name" value="{{$company->name}}" type="text" id="example-name-input" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="example-description-input" class="col-form-label">نبذه عن الشركة</label>
                                    <input class="form-control" name="description" value="{{$company->description}}" type="text" id="example-description-input" required>

                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="example-manager-name-input" class="col-form-label">اسم المدير</label>
                                    <input class="form-control" name="manager_name" value="{{$company->manager_name}}" type="text" id="example-manager-name-input" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="example-phone-input" class="col-form-label">التليفون</label>
                                    <input class="form-control" name="phone" data-parsley-type="digits"	data-parsley-length="[11, 11]" value="{{$company->phone}}" type="text" id="example-phone-input" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="example-mobile-input" class="col-form-label">الموبايل</label>
                                    <input class="form-control" name="mobile" data-parsley-length="[11, 11]" data-parsley-type="digits" data-parsley-length="[11, 11]" value="{{$company->mobile}}" type="text" id="example-mobile-input" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label for="example-number-input" class="col-form-label">رقم الواتس اب</label>
                                <input class="form-control" name="whatsapp_number" data-parsley-type="digits"	data-parsley-length="[11, 11]"	 value="{{$company->whatsapp_number}}" type="text" id="example-number-input" required>
                            </div>
                            <div class="col-md-6">
                                <label for="example-address-input" class="col-form-label">العنوان</label>
                                <input class="form-control" name="address" value="{{$company->address}}" type="text" id="example-address-input" required>
                            </div>
                            <div class="col-md-6">
                                <label for="example-activity-input" class="col-form-label">النشاط</label>
                                <input class="form-control" name="activity" value="{{$company->activity}}" type="text" id="example-activity-input" required>
                            </div>
                            <div class="col-md-6">
                                <label for="example-number-input" class="col-form-label">المدينة</label>
                                <select class="form-control select2 select2-hidden-accessible" name="city_id" style="width: 100%;" tabindex="-1" aria-hidden="true" required>
                                    @foreach($cities as $city)
                                        <option value="{{$city->id}}" <?php if($company->city_id == $city->id) echo "selected"; ?>>{{$city->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-6">
                                    <label for="example-suppliers-input" class="col-form-label">الموردين</label>
                                    <select class="form-control select2 select2-hidden-accessible" multiple name="suppliers[]" style="width: 100%;" tabindex="-1" aria-hidden="true" required>
                                        @foreach($suppliers as $supplier)
                                            <option value="{{$supplier->id}}" <?php if(in_array($supplier->id,$company_suppliers)) echo "selected"; ?>>{{$supplier->name}}</option>
                                        @endforeach
                                        
                                    </select>
                            </div>
                            <div class="col-md-6">
                                <label for="example-logo-input" class="col-form-label">لوجو الشركة</label>
                                <input class="form-control" name="logo" type="file" id="example-logo-input">
                                <img src="{{$company->logo}}" alt="logo image" class="img-responsive">
                            </div>
                            <div class="col-md-6">
                                <label for="example-premiums-months-input" class="col-form-label">القسط الشهري</label>
                                <input class="form-control" name="premiums_months" min="1" value="{{$company->premiums_months}}" type="number" id="example-premiums-months-input" required>
                            </div>
                            
                            <div class="col-md-6">
                                <label for="example-max-value-input" class="col-form-label">اقصي قيمة للتقسيط</label>
                                <input class="form-control" name="max_value" min="1" value="{{$company->max_value}}" type="number" id="example-max-value-input" required>
                            </div>
                            <div class="col-md-12">
                                <label for="status">الحالة</label>
                                <label class="switch switch-border switch-info">
                                    <input id="status" type="checkbox" name="status" <?php if($company->status == 1) echo "checked"; ?>>
                                    <span class="switch-indicator"></span>
                                    <span class="switch-description"></span>
                                </label>
                            </div>
                        
                            <input type="hidden" name="id" value="{{$company->id}}">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="example-number-input" class="col-form-label"></label>
                                    <div class="demo-checkbox">
                                        <input type="submit" class="btn btn-blue" value="حفظ">
                                    </div>
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>                        <!-- /.row -->
                    </div>
                </form>
                <!-- /.box-body -->
            </div>
        </section>
    </div>
@endsection
@section('scripts')
@endsection

