@extends('layouts.master')
@section('title')
    تعديل بيانات الاعدادات
@endsection
@section('styles')
@endsection
@section('modals')
@endsection
@section('content')
<div class="content-wrapper" style="min-height: 1295.8px;">
    <section class="content">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title"> تعديل بيانات الاعدادات</h3>
            </div>
            <!-- /.box-header -->
            <form action="{{route('settings.storeEdit')}}" method="post" data-parsley-validate="" enctype="multipart/form-data" novalidate class="error">
                @csrf
                <div class="box-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group row">
                                <label for="example-text-input" class="col-sm-2 col-form-label">قيمة اشتراك الكارنية</label>
                                <div class="col-sm-10">
                                    <input class="form-control" name="subscription_value" type="text" value="{{$setting->subscription_value}}" id="example-name-input" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-number-input" class="col-sm-2 col-form-label"></label>
                                <div class="col-sm-10">
                                    <div class="demo-checkbox">
                                        <input type="submit" class="btn btn-blue" value="حفظ">
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="id" value="{{$setting->id}}">
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
            </form>
            <!-- /.box-body -->
        </div>
    </section>
</div>
@endsection
@section('scripts')
@endsection

