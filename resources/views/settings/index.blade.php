@extends('layouts.master')
@section('title')
    الاعدادات
@endsection
@section('styles')
    <style>
        #admins tbody tr {
            word-break: break-all;
        }
    </style>
@endsection
@section('modals')
@endsection
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">

                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">جدول الاعدادات</h3>
                        </div>
                    <!-- /.box-header -->
                        <div class="box-body">
                            <table id="settings" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th>قيمة اشتراك الكارنية</th>
                                    <th>العمليات</th>
                                </tr>
                                </thead>
                            </table>


                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
@section('scripts')
    <script type="text/javascript">

        $('#settings').DataTable({
            destroy: true,
            processing: true,
            serverSide: true,
            order: [[0, "asc"]],
            dom: 'bfrtlip',
            buttons: [
                {
                    extend: 'copyHtml5',
                    exportOptions: {
                        columns: [ 0 ,1 ]
                    }
                },
                {
                    extend: 'excelHtml5',
                    exportOptions: {
                        columns: [ 0 ,1 ]
                    }
                },
                {
                    extend: 'pdfHtml5',
                    exportOptions: {
                        columns: [ 0 ,1 ]
                    }
                },
                {
                    extend: 'print',
                    exportOptions: {
                        columns: [ 0 ,1 ]
                    }
                },

            ],
            select: true,
            ajax: {
                "url": '{{route("settings.fetchSetting")}}',
                "type": 'GET'
            },
            columns: [
                {data: 'subscription_value', name: 'subscription_value'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ],
            aLengthMenu: [
                [10, 50, 100, 200, -1],
                [10, 50, 100, 200, "All"]
            ],
            "oLanguage": {
                "sSearch": "بحث : "
            }
        });


    </script>
@endsection

