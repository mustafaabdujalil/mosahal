@extends('layouts.master')
@section('title')
    تعديل البيانات
@endsection
@section('styles')
@endsection
@section('modals')
@endsection
@section('content')
    <div class="content-wrapper" style="min-height: 1295.8px;">
        <section class="content">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">اضافة ادمن جديد</h3>
                </div>
                <!-- /.box-header -->
                <form action="{{route('admin.storeAdding')}}" method="post" data-parsley-validate="" novalidate class="error">
                    @csrf
                    <div class="box-body">
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group row">
                                    <label for="example-name-input" class="col-sm-2 col-form-label">الاسم</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" value="{{old('name')}}" name="name" type="text" id="example-name-input" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="example-email-input" class="col-sm-2 col-form-label">البريد الالكتروني</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" value="{{old('email')}}" name="email" type="email" id="example-email-input" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="example-password-input" class="col-sm-2 col-form-label">كلمة المرور</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" name="password" type="password" id="example-password-input" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="example-phone-input" class="col-sm-2 col-form-label">التليفون</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" value="{{old('phone')}}" data-parsley-type="digits"	 name="phone" type="text" id="example-phone-input">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="example-mobile-input" class="col-sm-2 col-form-label">الموبايل</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" value="{{old('mobile')}}" name="mobile" type="text" data-parsley-length="[11, 11]" data-parsley-type="digits" id="example-mobile-input" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="example-number-input" class="col-sm-2 col-form-label">رقم الواتس اب</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" value="{{old('whatsapp_number')}}" name="whatsapp_number" data-parsley-length="[11, 11]" data-parsley-type="digits"	 type="text" id="example-number-input">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="example-address-input" class="col-sm-2 col-form-label">العنوان</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" value="{{old('address')}}" name="address" type="text" id="example-address-input" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="example-activity-input" class="col-sm-2 col-form-label">النشاط</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" value="{{old('activity')}}" name="activity" type="text" id="example-activity-input" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="example-number-input" class="col-sm-2 col-form-label">المدينة</label>
                                    <div class="col-sm-10">
                                        <select class="form-control select2 select2-hidden-accessible" name="city_id" style="width: 100%;" tabindex="-1" aria-hidden="true" required>
                                            @foreach($cities as $city)
                                                <option value="{{$city->id}}">{{$city->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="example-number-input" class="col-sm-2 col-form-label"></label>
                                    <div class="col-sm-10">
                                        <div class="demo-checkbox">
                                            <input type="submit" class="btn btn-blue" value="حفظ">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                </form>
                <!-- /.box-body -->
            </div>
        </section>
    </div>
@endsection
@section('scripts')
@endsection

