@extends('layouts.master')
@section('title')
    تعديل البيانات
@endsection
@section('styles')
@endsection
@section('modals')
@endsection
@section('content')
<div class="content-wrapper" style="min-height: 1295.8px;">
    <section class="content">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title"> تعديل بيانات {{$admin->name}}</h3>
            </div>
            <!-- /.box-header -->
            <form action="{{route('admin.storeEdit')}}" method="post" data-parsley-validate="" enctype="multipart/form-data" novalidate class="error">
                @csrf
                <div class="box-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group row">
                                <label for="example-text-input" class="col-sm-2 col-form-label">الاسم</label>
                                <div class="col-sm-10">
                                    <input class="form-control" name="name" type="text" value="{{$admin->name}}" id="example-name-input" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-email-input" class="col-sm-2 col-form-label">البريد الالكتروني</label>
                                <div class="col-sm-10">
                                    <input class="form-control" name="email" type="email" value="{{$admin->email}}" id="example-email-input" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-password-input" class="col-sm-2 col-form-label">كلمة المرور</label>
                                <div class="col-sm-10">
                                    <input class="form-control" placeholder="If you want old password, leave input empty" name="password" type="password" id="example-password-input">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-tel-input" class="col-sm-2 col-form-label">التليفون</label>
                                <div class="col-sm-10">
                                    <input class="form-control" name="phone" type="text" value="{{$admin->phone}}" id="example-phone-input">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-password-input" class="col-sm-2 col-form-label">الموبايل</label>
                                <div class="col-sm-10">
                                    <input class="form-control" name="mobile" data-parsley-type="digits" data-parsley-length="[11, 11]"	 type="text" value="{{$admin->mobile}}" id="example-mobile-input" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-number-input" class="col-sm-2 col-form-label">رقم الواتس اب</label>
                                <div class="col-sm-10">
                                    <input class="form-control" name="whatsapp_number" data-parsley-length="[11, 11]" type="text" value="{{$admin->whatsapp_number}}" id="example-number-input">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-number-input" class="col-sm-2 col-form-label">العنوان</label>
                                <div class="col-sm-10">
                                    <input class="form-control" name="address" type="text" value="{{$admin->address}}" id="example-address-input" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-number-input" class="col-sm-2 col-form-label">المدينة</label>
                                <div class="col-sm-10">
                                    <select class="form-control select2 select2-hidden-accessible" name="city_id" style="width: 100%;" tabindex="-1" aria-hidden="true" required>
                                        @foreach($cities as $city)
                                            <option value="{{$city->id}}" <?php if($admin->city_id == $city->id) echo "selected"; ?>>{{$city->name}}</option>
                                        @endforeach

                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-number-input" class="col-sm-2 col-form-label">الحالة</label>
                                <div class="col-sm-10">
                                    <label class="switch switch-border switch-info">
                                        <input type="checkbox" name="status" <?php if($admin->status == 1) echo "checked"; ?>>
                                        <span class="switch-indicator"></span>
                                        <span class="switch-description"></span>
                                    </label>

                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-number-input" class="col-sm-2 col-form-label">الصوره</label>
                                <div class="col-sm-10">
                                    <input type="file" name="image">
                                    <img src="{{$admin->image}}" alt="admin image" class="img-responsive" style="width: 100px;">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-number-input" class="col-sm-2 col-form-label"></label>
                                <div class="col-sm-10">
                                    <div class="demo-checkbox">
                                        <input type="submit" class="btn btn-blue" value="حفظ">
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="id" value="{{$admin->id}}">
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
            </form>
            <!-- /.box-body -->
        </div>
    </section>
</div>
@endsection
@section('scripts')
@endsection

