@extends('layouts.master')
@section('title')
    تعديل البيانات
@endsection
@section('styles')
@endsection
@section('modals')
@endsection
@section('content')
<div class="content-wrapper" style="min-height: 1295.8px;">
    <section class="content">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title"> تعديل بيانات {{$supplier->name}}</h3>
            </div>
            <!-- /.box-header -->
            <form action="{{route('supplier.storeEdit')}}" method="post" data-parsley-validate="" enctype="multipart/form-data" novalidate class="error">
                @csrf
                <div class="box-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group row">
                                <label for="example-text-input" class="col-sm-2 col-form-label">الاسم</label>
                                <div class="col-sm-10">
                                    <input class="form-control" name="name" type="text" value="{{$supplier->name}}" id="example-name-input" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-email-input" class="col-sm-2 col-form-label">البريد الالكتروني</label>
                                <div class="col-sm-10">
                                    <input class="form-control" name="email" type="email" value="{{$supplier->email}}" id="example-email-input" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-password-input" class="col-sm-2 col-form-label">كلمة المرور</label>
                                <div class="col-sm-10">
                                    <input class="form-control" placeholder="If you want old password, leave input empty" name="password" type="password" id="example-password-input">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-tel-input" class="col-sm-2 col-form-label">التليفون</label>
                                <div class="col-sm-10">
                                    <input class="form-control" name="phone" data-parsley-type="digits" type="text" value="{{$supplier->phone}}" id="example-phone-input">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-password-input" class="col-sm-2 col-form-label">الموبايل</label>
                                <div class="col-sm-10">
                                    <input class="form-control" name="mobile" data-parsley-type="digits" type="text" data-parsley-length="[11, 11]" value="{{$supplier->mobile}}" id="example-mobile-input" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-number-input" class="col-sm-2 col-form-label">رقم الواتس اب</label>
                                <div class="col-sm-10">
                                    <input class="form-control" name="whatsapp_number" data-parsley-type="digits"  data-parsley-length="[11, 11]" type="text" value="{{$supplier->whatsapp_number}}" id="example-number-input">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-discount-value-input" class="col-sm-2 col-form-label">نسبة الخصم</label>
                                <div class="col-sm-10">
                                    <input class="form-control" name="discount_value" value="{{$supplier->discount_value}}"	type="text" id="example-discount-value-input">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-number-input" class="col-sm-2 col-form-label">العنوان</label>
                                <div class="col-sm-10">
                                    <input class="form-control" name="address" type="text" value="{{$supplier->address}}" id="example-address-input" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-collect-time-input" class="col-form-label">ميعاد السداد</label>
                                <div class="col-sm-10">
                                    <input class="form-control" name="collect_time" type="number" min="1" value="{{$supplier->collect_time}}" id="example-collect-time-input" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-premiums-months-input" class="col-form-label">عدد شهور الاقساط</label>
                                <div class="col-sm-10">
                                    <input class="form-control" name="premiums_months" type="number" min="1" value="{{$supplier->premiums_months}}" id="example-premiums-months-input" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-number-input" class="col-sm-2 col-form-label">المدينة</label>
                                <div class="col-sm-10">
                                    <select class="form-control select2 select2-hidden-accessible" name="city_id" style="width: 100%;" tabindex="-1" aria-hidden="true" required>
                                        @foreach($cities as $city)
                                            <option value="{{$city->id}}" <?php if($supplier->city_id == $city->id) echo "selected"; ?>>{{$city->name}}</option>
                                        @endforeach

                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-number-input" class="col-sm-2 col-form-label">الحالة</label>
                                <div class="col-sm-10">
                                    <label class="switch switch-border switch-info">
                                        <input type="checkbox" name="status" <?php if($supplier->status == 1) echo "checked"; ?>>
                                        <span class="switch-indicator"></span>
                                        <span class="switch-description"></span>
                                    </label>

                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-number-input" class="col-sm-2 col-form-label">الصوره</label>
                                <div class="col-sm-10">
                                    <input type="file" name="image">
                                    <img src="{{$supplier->image}}" alt="supplier image" class="img-responsive" style="width: 100px;">

                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-number-input" class="col-sm-2 col-form-label"></label>
                                <div class="col-sm-10">
                                    <div class="demo-checkbox">
                                        <input type="submit" class="btn btn-blue" value="حفظ">
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="id" value="{{$supplier->id}}">
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
            </form>
            <!-- /.box-body -->
        </div>
    </section>
</div>
@endsection
@section('scripts')
@endsection

