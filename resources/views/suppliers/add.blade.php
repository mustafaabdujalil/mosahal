@extends('layouts.master')
@section('title')
اضافة مورد
@endsection
@section('styles')
@endsection
@section('modals')
@endsection
@section('content')
    <div class="content-wrapper" style="min-height: 1295.8px;">
        <section class="content">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">اضافة مورد جديد</h3>
                </div>
                <!-- /.box-header -->
                <form action="{{route('supplier.storeAdding')}}" method="post" data-parsley-validate="" novalidate class="error">
                    @csrf
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="example-name-input" class="col-form-label">الاسم</label>
                                    <input class="form-control" name="name" type="text" value="{{old('name')}}" id="example-name-input" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="example-email-input" class="col-form-label">البريد الالكتروني</label>
                                    <input class="form-control" name="email" type="email" value="{{old('email')}}" id="example-email-input" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="example-password-input" class="col-form-label">كلمة المرور </label>
                                    <input class="form-control" name="password" type="password" id="example-password-input" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="example-phone-input" class="col-form-label">التليفون</label>
                                    <input class="form-control" name="phone" data-parsley-type="digits"	value="{{old('phone')}}" type="text" id="example-phone-input">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="example-mobile-input" class="col-form-label">الموبايل</label>
                                    <input class="form-control" name="mobile" data-parsley-type="digits" value="{{old('mobile')}}" data-parsley-length="[11, 11]"	type="text" id="example-mobile-input" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="example-number-input" class="col-form-label">رقم الواتس اب</label>
                                    <input class="form-control" name="whatsapp_number" data-parsley-type="digits" value="{{old('whatsapp_number')}}"	data-parsley-length="[11, 11]"	 type="text" id="example-number-input">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="example-discount-value-input" class="col-form-label">نسبة الخصم</label>
                                    <input class="form-control" name="discount_value" value="{{old('discount_value')}}"	type="text" id="example-discount-value-input">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="example-address-input" class="col-form-label">العنوان</label>
                                    <input class="form-control" name="address" type="text" value="{{old('address')}}" id="example-address-input" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="example-collect-time-input" class="col-form-label">ميعاد السداد</label>
                                    <input class="form-control" name="collect_time" type="number" min="1" value="{{old('collect_time')}}" id="example-collect-time-input" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="example-premiums-months-input" class="col-form-label">عدد شهور الاقساط</label>
                                    <input class="form-control" name="premiums_months" type="number" min="1" value="{{old('premiums_months')}}" id="example-premiums-months-input" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="example-activity-input" class="col-form-label">النشاط</label>
                                    <input class="form-control" name="activity" type="text" id="example-activity-input" value="{{old('activity')}}" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="example-number-input" class="col-form-label">المدينة</label>
                                    <select class="form-control select2 select2-hidden-accessible" name="city_id" style="width: 100%;" tabindex="-1" aria-hidden="true" required>
                                        @foreach($cities as $city)
                                            <option value="{{$city->id}}">{{$city->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="example-number-input" class="col-form-label"></label>
                                    <div class="demo-checkbox">
                                        <input type="submit" class="btn btn-blue" value="حفظ">
                                    </div>
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                </form>
                <!-- /.box-body -->
            </div>
        </section>
    </div>
@endsection
@section('scripts')
@endsection

