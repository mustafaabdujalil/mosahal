@extends('layouts.master')
@section('title')
    العملاء
@endsection
@section('styles')
    <style>
        #admins tbody tr {
            word-break: break-all;
        }

    </style>
@endsection
@section('modals')
@endsection
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">

                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">جدول العملاء</h3>
                        </div>
                        <div class="filter">
                            <h6 style="font-style: italic;margin-right: 15px;">بحث عن عميل</h6>
                            <form action="/client/get-client-by-name" method="get" data-parsley-validate="" novalidate>
                                <div class="col-md-12 row">
                                    <div class="col-md-5 form-group">
                                        <label for="">الاسم</label>
                                        <input type="text" value="{{(Session::has('name') ? Session::get('name') : '')}}" class="form-control" name="name" required>
                                    </div>
                                    <div class="col-md-2" style="padding-top: 25px;">
                                        <label for=""></label>
                                        <input type="submit" value="بحث" class="btn btn-md btn-success">
                                    </div>
                                </div>
                            </form>
                        </div>
                    <!-- /.box-header -->
                        <div class="box-body">
                            <table id="clients" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th>البريد الالكتروني</th>
                                    <th>الاسم</th>
                                    <th>الموبايل</th>
                                    <th>التليفون</th>
                                    <th>واتس اب</th>
                                    <th>كود الكارته</th>
                                    <th>العنوان</th>
                                    <th>الشركة</th>
                                    <th>المدينة</th>
                                    <th>اجمالي المبلغ المستحق</th>
                                    <th>الحالة</th>
                                    <th>الصوره</th>
                                    <th>العمليات</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
@section('scripts')
    <script type="text/javascript">

        $('#clients').DataTable({
            destroy: true,
            processing: true,
            serverSide: true,
            order: [[0, "asc"]],
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'copyHtml5',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10 ]
                    }
                },
                {
                    extend: 'excelHtml5',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10 ]
                    }
                },
                {
                    extend: 'pdfHtml5',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10 ]
                    }
                },
                {
                    extend: 'print',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10 ]
                    }
                },

            ],
            select: true,
            ajax: {
                "url": '{{route("client.fetchClientByName")}}',
                "type": 'GET'
            },
            columns: [
                {data: 'email', name: 'email'},
                {data: 'name', name: 'name'},
                {data: 'mobile', name: 'mobile'},
                {data: 'phone', name: 'phone'},
                {data: 'whatsapp', name: 'whatsapp'},
                {data: 'card_code', name: 'card_code'},
                {data: 'address', name: 'address'},
                {data: 'company', name: 'company'},
                {data: 'city', name: 'city'},
                {data: 'remaining_amount', name: 'remaining_amount'},
                {data: 'status', name: 'status'},
                {data: 'image', name: 'image'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ],
            // aLengthMenu: [
            //     [10, 50, 100, 200, -1],
            //     [10, 50, 100, 200, "All"]
            // ]
            "oLanguage": {
                "sSearch": "بحث : "
            }
        });

        // set table style
        $("#clients").removeClass('dataTable');
    </script>
@endsection

