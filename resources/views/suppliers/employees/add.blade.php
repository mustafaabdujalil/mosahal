@extends('layouts.master')
@section('title')
    اضافة موظف
@endsection
@section('styles')
@endsection
@section('modals')
@endsection
@section('content')
    <div class="content-wrapper" style="min-height: 1295.8px;">
        <section class="content">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">اضافة موظف جديد</h3>
                </div>
                <!-- /.box-header -->
                <form action="{{route('supplier.storeAddingEmployee')}}" method="post" data-parsley-validate="" novalidate class="error">
                    @csrf
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="example-name-input" class="col-form-label">الاسم</label>
                                    <input class="form-control" name="name" type="text" value="{{old('name')}}" id="example-name-input" required>
                                    <input type="hidden" name="supplier_id" value="{{$supplier->id}}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="example-email-input" class="col-form-label">البريد الالكتروني</label>
                                    <input class="form-control" name="email" type="email" value="{{old('email')}}" id="example-email-input" required>
                                    <input type="hidden" name="supplier_id" value="{{$supplier->id}}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="example-password-input" class="col-form-label">كلمة المرور</label>
                                    <input class="form-control" name="password" type="password" value="{{old('password')}}" id="example-password-input" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="example-company-input" class="col-form-label">الفرع</label>
                                    <select class="form-control select2 select2-hidden-accessible" name="branch_id" style="width: 100%;" tabindex="-1" aria-hidden="true" required>
                                        @foreach($branches as $branch)
                                            <option value="{{$branch->id}}">{{$branch->branch_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="example-branch_id-input" class="col-form-label">الصلاحيات</label>
                                    <select class="form-control select2 select2-hidden-accessible" multiple name="roles[]" style="width: 100%;" tabindex="-1" aria-hidden="true" required>
                                        <option value="1">بيع</option>
                                        <option value="2">تحصيل</option>
                                        <option value="3">مرتجع</option>
                                        <option value="0">كل الصلاحيات</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="example-number-input" class="col-form-label"></label>
                                    <div class="demo-checkbox">
                                        <input type="submit" class="btn btn-blue" value="حفظ">
                                    </div>
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                </form>
                <!-- /.box-body -->
            </div>
        </section>
    </div>
@endsection
@section('scripts')
@endsection

