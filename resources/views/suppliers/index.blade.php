@extends('layouts.master')
@section('title')
    منافذ البيع
@endsection
@section('styles')
    <style>
        #admins tbody tr {
            word-break: break-all;
        }
    </style>
@endsection
@section('modals')
@endsection
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">

                    <div class="box">
                        @if(Auth::user()->role == "Admin")
                            <div class="box-header with-border">
                                <h3 class="box-title">جدول منافذالبيع</h3>
                                <a class="btn btn-cyan" style="float: left;" href="{{route('supplier.addForm')}}">اضف منفذ بيع</a>
                            </div>
                        @endif
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="suppliers" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>الاسم</th>
                                        <th>البريد الالكتروني</th>
                                        <th>كود الكارته</th>
                                        <th>الموبايل</th>
                                        <th>التليفون</th>
                                        <th>واتس اب</th>
                                        <th>نسبة الخصم </th>
                                        <th>العنوان</th>
                                        <th>المدينة</th>
                                        <th>ميعاد اسداد</th>
                                        <th>عدد اشهر الاقساط</th>
                                        <th>المبلغ المسدد</th>
                                        <th>الحالة</th>
                                        <th>الصوره</th>
                                        <th>العمليات</th>
                                    </tr>
                                </thead>
                            </table>


                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
@section('scripts')
    <script type="text/javascript">

        var suppliersTable = $('#suppliers').DataTable({
            destroy: true,
            processing: true,
            serverSide: true,
            order: [[0, "asc"]],
            dom: 'bfrtlip',
            buttons: [
                {
                    extend: 'copyHtml5',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11 ]
                    }
                },
                {
                    extend: 'excelHtml5',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11 ]
                    }
                },
                {
                    extend: 'pdfHtml5',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11 ]
                    }
                },
                {
                    extend: 'print',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3,4,5,6,7,8,9,10,11 ]
                    }
                },

            ],
            select: true,
            ajax: {
                "url": '{{route("supplier.fetchSuppliers")}}',
                "type": 'GET'
            },
            columns: [
                {data: 'name', name: 'name'},
                {data: 'email', name: 'email'},
                {data: 'card_code', name: 'card_code'},
                {data: 'mobile', name: 'mobile'},
                {data: 'phone', name: 'phone'},
                {data: 'whatsapp', name: 'whatsapp'},
                {data: 'discount_value', name: 'discount_value'},
                {data: 'address', name: 'address'},
                {data: 'city', name: 'city'},
                {data: 'collect_time', name: 'collect_time'},
                {data: 'premiums_months', name: 'premiums_months'},
                {data: 'debt_value', name: 'debt_value'},
                {data: 'status', name: 'status'},
                {data: 'image', name: 'image'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ],
            aLengthMenu: [
                [10, 50, 100, 200, -1],
                [10, 50, 100, 200, "All"]
            ],
            "oLanguage": {
                "sSearch": "بحث : "
            }
        });

        function deleteSupplier(id) {
            console.log(id);
            Swal.fire({
                title: 'هل انت متاكد من الحذف ؟',
                text: "ﻻ تستطيع استعاده الموارد بعد الحذف",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'نعم',
                cancelButtonText: 'ﻻ',
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        'url':'/supplier/delete',
                        'type':'POST',
                        'data':{
                            'id' : id,
                            "_token": "{{ csrf_token() }}",
                        },
                        success:function(data){
                            location.reload(true);
                        },
                        error : function(){
                        }
                    });
                }
            })

        }



        async function changeDebtValue(id) {

            const ipAPI = 'https://api.ipify.org?format=json';

            const inputValue = fetch(ipAPI)
                .then(response => response.json())
                .then(data => data.ip)

            const {value: debtValue} = await Swal.fire({
                title: 'برجاء ادخال المبلغ المسدد ؟',
                input: 'number',
                inputValue: '',
                showCancelButton: true,
                inputValidator: (value) => {
                    if (!value || value <= 0) {
                        return 'برجاء ادخال القيمة الدين بشكل صحيح'
                    }

                }
            })

            if (debtValue) {
                Swal.fire({
                    title: 'هل انت متاكد من القيمة ؟',
                    text: "",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'نعم',
                    cancelButtonText: 'ﻻ',
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            'url':'/admin/store_debt_value',
                            'type':'POST',
                            'data':{
                                'supplier_id' : id,
                                'debt_value' : debtValue,
                                "_token": "{{ csrf_token() }}",
                            },
                            success:function(data){
                                suppliersTable.ajax.reload();
                                Swal.fire({
                                    type: data.status,
                                    title: data.title,
                                    text: data.msg,
                                });
                                // $('#premiums').DataTable().ajax.reload();
                            },
                            error : function(){
                            }
                        });
                    }
                })
            }
        }

        window.onload = function() {
            $("#suppliers").removeClass('dataTable');
        }
    </script>
@endsection

