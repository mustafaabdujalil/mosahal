@extends('layouts.master')
@section('title')
    تعديل البيانات
@endsection
@section('styles')
@endsection
@section('modals')
@endsection
@section('content')
    <div class="content-wrapper" style="min-height: 1295.8px;">
        <section class="content">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title"> تعديل بيانات {{$purchase->commodity_name}}</h3>
                </div>
                <!-- /.box-header -->
                <form action="{{route('purchases.storeEdit')}}" method="post" data-parsley-validate="" enctype="multipart/form-data" novalidate class="error">
                    @csrf
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="example-commodity-name-input" class=" col-form-label">اسم المنتج</label>
                                    <input class="form-control" value="{{$purchase->commodity_name}}" name="commodity_name" type="text" id="example-commodity-name-input" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="example-commodity-description-input" class="col-form-label">نبذه عن المنتج</label>
                                    <input class="form-control" value="{{$purchase->commodity_name}}" name="commodity_description" type="text" id="example-commodity-description-input" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="example-total-price-input" class="col-form-label">السعر الاجمالي</label>
                                    <input class="form-control" data-parsley-type="digits"	 value="{{ (int) $purchase->total_price}}" name="total_price" type="text" id="example-total-price-input" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label for="example-number-input" class="col-form-label">العميل</label>
                                <select class="form-control select2 select2-hidden-accessible" name="client_id" style="width: 100%;" tabindex="-1" aria-hidden="true" required>
                                    @foreach($clients as $client)
                                        <option value="{{$client->id}}" <?php if($client->id == $purchase->client_id) echo "selected"; ?>>{{$client->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            @if(Auth::user()->role == "Admin")
                                <div class="col-md-6">
                                    <label for="example-number-input" class="col-form-label">العميل</label>
                                    <select class="form-control select2 select2-hidden-accessible" name="client_id" style="width: 100%;" tabindex="-1" aria-hidden="true" required>
                                        @foreach($suppliers as $supplier)
                                            <option value="{{$supplier->id}}" <?php if($supplier->id == $purchase->supplier_id) echo "selected"; ?>>{{$supplier->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            @endif
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="example-paid-amount-input" class="col-form-label">الملبغ المدفوع</label>
                                    <input class="form-control" data-parsley-type="digits"	 value="{{ (int) $purchase->paid_amount}}" name="paid_amount" type="text" id="example-paid-amount-input">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="example-remaining-amount-input" class="col-form-label">الملبغ المتبقي</label>
                                    <input class="form-control" data-parsley-type="digits"	 value="{{ (int) $purchase->remaining_amount}}" name="remaining_amount" type="text" id="example-remaining-amount-input">
                                </div>
                            </div>
                            <input type="hidden" name="id" value="{{$purchase->id}}">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="example-number-input" class="col-form-label"></label>
                                    <div class="demo-checkbox">
                                        <input type="submit" class="btn btn-blue" value="حفظ">
                                    </div>
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>                        <!-- /.row -->
                    </div>
                </form>
                <!-- /.box-body -->
            </div>
        </section>
    </div>
@endsection
@section('scripts')
@endsection

