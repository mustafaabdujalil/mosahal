@extends('layouts.master')
@section('title')
المنتجات المسترجعه
@endsection
@section('styles')
@endsection
@section('modals')
@endsection
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">

                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">جدول المنتجات المسترجعه</h3>
                        </div>

                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="recovery-products" class="table-bordered">
                                <thead>
                                <tr>
                                    <th>اسم المنتج</th>
                                    <th>الكمية المسترجعه</th>
                                    <th>السعر للقطعة الواحده</th>
                                    <th>السعر الاجمالي</th>
                                </tr>
                                </thead>
                            </table>
                            <div class="form-group">
                                <label for="sub_total" class="col-md-2 below-table">السعر الاجمالي : </label>
                                <div class="col-md-3">
                                    <div class="input-group">
                                        <input id="total" type="text" class="form-control" name="total" readonly>
                                    </div>
                                </div>
                            </div>
                            <br><br>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
@section('scripts')
    <script type="text/javascript">
        var totalAmount = 0.00;
        $('#recovery-products').DataTable({
            destroy: true,
            processing: true,
            serverSide: true,
            order: [[0, "asc"]],
            dom: 'bfrtlip',
            buttons: [
                {
                    extend: 'copyHtml5',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3]
                    }
                },
                {
                    extend: 'excelHtml5',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3]
                    }
                },
                {
                    extend: 'pdfHtml5',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3]
                    }
                },
                {
                    extend: 'print',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3]
                    }
                },

            ],
            select: true,
            ajax: {
                "url": '/purchases/fetchRecoveryProducts/'+"{{$id}}",
                "type": 'GET',

            },
            columns: [
                {data: 'commodity_name', name: 'commodity_name'},
                {data: 'quantity', name: 'quantity'},
                {data: 'price', name: 'price'},
                {data: 'total_price', name: 'total_price'},
            ],
            aLengthMenu: [
                [10, 50, 100, 200, -1],
                [10, 50, 100, 200, "All"]
            ],
            "oLanguage": {
                "sSearch": "بحث : "
            },
            'fnCreatedRow': function (nRow, aData, iDataIndex) {

                if(iDataIndex == 0){
                    totalAmount = 0.00;
                    totalCurrentAmount = 0.00;
                }
                totalAmount = parseFloat(aData['total_price']) + totalAmount;
                $('#total').val(totalAmount.toFixed(2));
            }

        });

    </script>
@endsection

