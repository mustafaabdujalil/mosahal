@extends('layouts.master')
@section('title')
    المنتجات
@endsection
@section('styles')
@endsection
@section('modals')
@endsection
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">

                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">جدول المنتجات</h3>
{{--                            <a class="btn btn-cyan" style="float: left;" href="{{route('purchases.addProduct')}}">اضافة منتج</a>--}}
                        </div>

                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="products" class="table-bordered">
                                <thead>
                                <tr>
                                    <th>اسم المنتج</th>
                                    <th>الكمية</th>
                                    <th>السعر للقطعة الواحده</th>
                                    <th>السعر الاجمالي</th>
                                    <th>العمليات</th>
                                </tr>
                                </thead>
                            </table>
                            <div class="form-group">
                                <label for="sub_total" class="col-md-2 below-table">اجمالي السعر : </label>
                                <div class="col-md-3">
                                    <div class="input-group">
                                        <input id="total" type="text" class="form-control" name="total" readonly>
                                    </div>
                                </div>
                            </div>
                            <br><br>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
@section('scripts')
    <script type="text/javascript">
        var totalAmount = 0.00;
        $('#products').DataTable({
            destroy: true,
            processing: true,
            serverSide: true,
            order: [[0, "asc"]],
            dom: 'bfrtlip',
            buttons: [
                {
                    extend: 'copyHtml5',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3]
                    }
                },
                {
                    extend: 'excelHtml5',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3]
                    }
                },
                {
                    extend: 'pdfHtml5',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3]
                    }
                },
                {
                    extend: 'print',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3]
                    }
                },

            ],
            select: true,
            ajax: {
                "url": '/purchases/fetchProducts/'+"{{$id}}",
                "type": 'GET',
            },
            columns: [
                {data: 'commodity_name', name: 'commodity_name'},
                {data: 'quantity', name: 'quantity'},
                {data: 'price', name: 'price'},
                {data: 'total_price', name: 'total_price'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ],
            aLengthMenu: [
                [10, 50, 100, 200, -1],
                [10, 50, 100, 200, "All"]
            ],
            "oLanguage": {
                "sSearch": "بحث : "
            },
            'fnCreatedRow': function (nRow, aData, iDataIndex) {
            console.log(aData);
                if(iDataIndex == 0){
                    totalAmount = 0.00;
                }
                totalAmount = parseFloat(aData['total_price']) + totalAmount;
                $('#total').val(totalAmount.toFixed(2));
            }
        });

        async function deleteProduct(id,quantity) {
            const ipAPI = 'https://api.ipify.org?format=json'

            const inputValue = fetch(ipAPI)
                .then(response => response.json())
                .then(data => data.ip)

            const {value: count} = await Swal.fire({
                title: 'برجاء ادخال العدد المراد استرجاعه  ؟',
                input: 'number',
                inputValue: quantity,
                showCancelButton: true,
                inputValidator: (value) => {
                    if (!value || value <= 0) {
                        return 'برجاء ادخال العدد بشكل صحيح'
                    }
                }
            });

            if (count) {
                Swal.fire({
                    title: 'هل انت متاكد من الاسترجاع ؟',
                    text: "ﻻ تستطيع استعاده العدد بعد الاسترجاع",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'نعم',
                    cancelButtonText: 'ﻻ',
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            'url':'/purchases/product/delete',
                            'type':'POST',
                            'data':{
                                'id' : id,
                                'count' : count,
                                "_token": "{{ csrf_token() }}",
                            },
                            success:function(data){
                                Swal.fire({
                                    position: 'center',
                                    type: data.status,
                                    title:data.msg,
                                    showConfirmButton: true,
                                    timer: 3000
                                });
                                setTimeout(function(){
                                    location.reload(true);
                                }, 3000);

                            },
                            error : function(){
                            }
                        });
                    }
                })
            }

        }
    </script>
@endsection

