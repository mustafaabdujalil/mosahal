@extends('layouts.master')
@section('title')
    المبيعات
@endsection
@section('styles')
@endsection
@section('modals')
@endsection
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">

                    <div class="box">
                        <?php
                            if(Auth::user()->role == "SupplierEmployee"){
                                $roles = \App\SupplierRole::where('supplier_id',Auth::user()->id)->pluck('role')->toArray();
                            }else{
                                $roles = [];
                            }
                        ?>
                        @if( in_array(Auth::user()->role,['Admin']))
                            <div class="box-header with-border">
                                <h3 class="box-title">جدول المبيعات</h3>
                                <a class="btn btn-cyan" style="float: left;" href="{{route('purchases.addForm')}}">اضافة قسيمة بيع</a>
                            </div>
                        @endif

                        @if(Auth::user()->role == 'Admin')
                             <div class="filter">
                                 <h6 style="font-style: italic;margin-right: 15px;">بحث بالمورد</h6>
                                 <select name="supplier_id" id="supplier_id" class="form-control">
                                     <option disabled selected>Select a Supplier</option>
                                     <option value="0" <?php if($id == 0) echo "selected";?>>الكل</option>
                                    @foreach($suppliers as $supplier)
                                         <option value="{{$supplier->id}}" <?php if($supplier->id == $id) echo "selected";?>>{{$supplier->name}}</option>
                                     @endforeach
                                 </select>
                             </div>
                        @endif

                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="purchases" class="table-bordered">
                                <thead>
                                <tr>
                                    <th>اسم العميل</th>
                                    <th>اسم المورد</th>
                                    <th>السعر الاجمالي</th>
                                    <th>المبلغ المدفوع</th>
                                    <th>المبلغ المتبقي</th>
                                    <th>شهور الاقساط</th>
                                    <th>القسط الشهري</th>
                                    <th>الحد الائتماني</th>
                                    <th>هوية العميل</th>
                                    <th>تاريخ العملية</th>
                                    <th>العمليات</th>
                                </tr>
                                </thead>
                            </table>
                            <div class="form-group">
                                <label for="sub_total" class="col-md-2 below-table">اجمالي السعر : </label>
                                <div class="col-md-3">
                                    <div class="input-group">
                                        <input id="total" type="text" class="form-control" name="total" readonly>
                                    </div>
                                </div>
                                <label for="sub_total" class="col-md-2 below-table">اجمالي المبلغ المدفوع : </label>
                                <div class="col-md-3">
                                    <div class="input-group">
                                        <input id="total_current_balance" type="text" class="form-control" name="total_current_balance" readonly>
                                    </div>
                                </div>
                                <label for="sub_total" class="col-md-2 below-table">اجمالي المبلغ المتبقي : </label>
                                <div class="col-md-3">
                                    <div class="input-group">
                                        <input id="total_remaining_balance" type="text" class="form-control" name="total_remaining_balance" readonly>
                                    </div>
                                </div>
                                <div class="col-md-2"></div>
                            </div>
                            <br><br>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
@section('scripts')
    <script type="text/javascript">
        var totalAmount = 0.00;
        var totalCurrentAmount = 0.00;
        var totalRemainingAmount = 0.00;
        var purchases = $('#purchases').DataTable({
            destroy: true,
            processing: true,
            serverSide: true,
            order: [[0, "asc"]],
            dom: 'bfrtlip',
            buttons: [
                {
                    extend: 'copyHtml5',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3,4,5,6,7,8,9]
                    }
                },
                {
                    extend: 'excelHtml5',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3,4,5,6,7,8,9]
                    }
                },
                {
                    extend: 'pdfHtml5',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3,4,5,6,7,8,9]
                    }
                },
                {
                    extend: 'print',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3,4,5,6,7,8,9]
                    }
                },

            ],
            select: true,
            ajax: {
                "url": '/purchases/fetchPurchases/'+"{{$id}}",
                "type": 'GET',
            },
            columns: [
                {data: 'client_name', name: 'client_name'},
                {data: 'supplier_name', name: 'supplier_name'},
                {data: 'total_price', name: 'total_price'},
                {data: 'paid_amount', name: 'paid_amount'},
                {data: 'remaining_amount', name: 'remaining_amount'},
                {data: 'premiums_month', name: 'premiums_month'},
                {data: 'monthly_value', name: 'monthly_value'},
                {data: 'max_value', name: 'max_value'},
                {data: 'gender', name: 'gender'},
                {data: 'created_at', name: 'created_at'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ],
            aLengthMenu: [
                [10, 50, 100, -1],
                [10, 50, 100, "All"]
            ],
            "oLanguage": {
                "sSearch": "بحث : "
            },
            'fnCreatedRow': function (nRow, aData, iDataIndex) {

                if(iDataIndex == 0){
                    totalAmount = 0.00;
                    totalCurrentAmount = 0.00;
                    totalRemainingAmount = 0.00;
                }
                totalAmount = parseFloat(aData['total_price']) + totalAmount;
                $('#total').val(totalAmount.toFixed(2));
                totalCurrentAmount = parseFloat(aData['paid_amount']) + totalCurrentAmount;
                $('#total_current_balance').val(totalCurrentAmount.toFixed(2));
                totalRemainingAmount = parseFloat(aData['remaining_amount']) + totalRemainingAmount;
                $('#total_remaining_balance').val(totalRemainingAmount.toFixed(2));
            }
        });

        function deletePurchase(id) {
            Swal.fire({
                title: 'هل انت متاكد من الحذف ؟',
                text: "ﻻ تستطيع استعاده المبيعة بعد الحذف",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'نعم',
                cancelButtonText: 'ﻻ',
            }).then((result) => {
                if (result.value) {
                    console.log(id);
                    $.ajax({
                        'url':'/purchases/delete',
                        'type':'GET',
                        'data':{
                            'id' : id,
                        },
                        success:function(data){
                            location.reload(true);
                        },
                        error : function(){
                        }
                    });
                }
            })
        }


        $('#supplier_id').change(function() {
            var id = $(this).val();
            location.href = '/purchases/supplierPurchases/'+id;
        });
    </script>
@endsection

