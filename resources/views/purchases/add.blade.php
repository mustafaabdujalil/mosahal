@extends('layouts.master')
@section('title')
    اضافة قسيمة بيع جديدة
@endsection
@section('styles')
@endsection
@section('modals')
@endsection
@section('content')
    <div class="content-wrapper" style="min-height: 1295.8px;">
        <section class="content">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title" style="display: inline">اضافة قسيمة بيع جديدة</h3>
                    <h3 style="display: inline"><span id="max-value"></span></h3>
                    <h3 style="display: inline"><span id="card-code"></span></h3>
                </div>
                <!-- /.box-header -->
                <form id="purchase-id" action="{{route('purchases.storeAdding')}}" method="post" data-parsley-validate="" novalidate class="error" enctype="multipart/form-data">
                    @csrf
                    <div class="box-body" id="box-body">
                        <div class="row">
                            @if($flag_one == 1)
                                <div class="col-md-12">
                                    @foreach($clients as $client)
                                        <ul>
                                            <li>  الاسم : {{$client->name}} </li>
                                            <li>  الحد الادني : {{$client->max_value}} </li>
                                            <li>  المبلغ المستحق  : {{$total_remaining_amount}} </li>
                                        </ul>
                                    @endforeach
                                </div>
                            @endif
                            <div class="col-md-12">
                                <label for="example-number-input" class="col-form-label">العميل</label>
                                <select class="form-control select2" id="client" name="client_id" style="width: 100%;" tabindex="-1" aria-hidden="true" required>
                                    <option value="" data-submitted-value="0" selected disabled>Select Client</option>
                                    @foreach($clients as $client)
                                        <option data-submitted-value="{{$client->submitted_value}}" data-card-code="{{$client->user->card_code}}" data-max-value="{{$client->max_value}}" value="{{$client->id}}">{{$client->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <!-- /.col -->
                        </div>
                        @if(Auth::user()->role == "Admin")
                            <div class="row">
                                <div class="col-md-12">
                                    <label for="example-number-input" class="col-form-label">المورد</label>
                                    <select class="form-control select2" id="supplier" name="supplier_id" style="width: 100%;" tabindex="-1" aria-hidden="true" required>
                                        @foreach($suppliers as $supplier)
                                            <option value="{{$supplier->id}}">{{$supplier->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <!-- /.col -->
                            </div>
                        @endif
                        <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label  class=" col-form-label">اسم المنتج</label>
                                        <input class="form-control" value="{{old('commodity_name')}}" name="commodity_name[]" type="text" required>
                                    </div>
                                </div>
                                <div class="col-md-2 quantity-div">
                                    <div class="form-group">
                                        <label for="example-total-price-input" class="col-form-label">الكمية</label>
                                        <input class="form-control sum quantity" value="{{old('quantity')}}" name="quantity[]" data-parsley-type="digits" min="1" type="number" id="example-total-price-input" required>
                                    </div>
                                </div>
                                <div class="col-md-2 price-div">
                                    <div class="form-group">
                                        <label class="col-form-label">السعر للقطعة الواحده</label>
                                        <input class="form-control sum price_per_one" value="{{old('price')}}" name="price[]" min="0" step="0.01" type="number" required>
                                    </div>
                                </div>
                            <!-- /.col -->
                        </div>
                    </div>
                    <!-- /.row -->
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6 offset-md-6">
                                <div class="paid-amount-div">
                                    <div class="form-group">
                                        <label for="example-paid-amount-input" class="col-form-label">  الملبغ المدفوع : <span class="submitted_value"></span></label>
                                        <input class="form-control paid_amount" data-parsley-type="number" value="{{old('paid_amount')}}"
                                               name="paid_amount[]" type="text"
                                               id="example-paid-amount-input" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="example-number-input" class="col-form-label"></label>
                                    <h4>الاجمالي المستحق : <span class="total_remaining">0</span></h4>
                                    <h4>  الاجمالي المدفوع :  <span class="total_paid">0</span></h4>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="example-number-input" class="col-form-label"></label>
                                    <div class="demo-checkbox">
                                        <input type="submit" class="btn btn-sm btn-success" id="submit-btn" value="حفظ">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-8 col-md-offset-4">
                                <div class="form-group">
                                    <label for="example-number-input" class="col-form-label"></label>
                                    <div class="demo-checkbox">
                                        <input type="button" id="addrow" class="btn btn-sm btn-info" style="float: left;" value="اضافة منتج">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </form>
                <!-- /.box-body -->
            </div>
        </section>
    </div>
@endsection
@section('scripts')
    <script>
        $('#client').on("change",function(){
            var submittedValue = $("#client option:selected").attr('data-submitted-value');
            $(".submitted_value").text(submittedValue+' %');
            var maxValue = $("#client option:selected").attr('data-max-value');
            $("#max-value").text(" ( الحد الائتماني المتبقي ج.م"+maxValue+") ");
            var cardCode = $("#client option:selected").attr('data-card-code');
            $("#card-code").text(" ( الكود "+cardCode+") ");
            // $('.paid_amount').attr('data-parsley-min', submittedValue);
        });

        // clone new input for new product
        jQuery(document).ready(function() {
            // set submitted value
            var submittedValue = $("#client option:selected").attr('data-submitted-value');
            $(".submitted_value").text(submittedValue+' %');

            var id = 0;
            jQuery("#addrow").click(function() {
                id++;
                var row = $(".row-parent").find('div:first').clone(true);
                row.find("input:text").val("");
                row.attr('id',id);
                row.appendTo('#box-body');
                row.removeClass('newrow');
                return false;
            });

            $('.remove').on("click", function() {
                $(this).parents(".row").remove();
            });
        });

        // handle changing of paid of amount
        $("input[name^='paid_amount']").keyup(function() {
            // set all to 0
            $(".total_paid").text(0);
            $(".total_remaining").text(0);

            var total = 0;
            var total_paid = 0;
            var sum = 0;
            var submittedValue = $("#client option:selected").attr('data-submitted-value');

            $("input[name^='quantity']").each(function (index, element) {
                sum = parseFloat($(element).val()) * parseFloat($(this).parent().parent().next('div').find('.price_per_one').val());
                total = total + sum;
            });

            var submitted_value = total * submittedValue / 100;
            $(".paid_amount").prop('min',submitted_value);

            $("input[name^='paid_amount']").each(function (index, element) {
                if (!$(this).val()) {
                    total_paid = total_paid + 0;
                }else{
                    total_paid = parseFloat (total_paid) + parseFloat($(element).val())  ;
                }
            });

            $(".total_paid").text(total_paid);
            $(".total_remaining").text(Math.abs(total-total_paid));
            // $(".paid_amount").val(total_paid);

        });


        // handle changing of quantity
        $("input[name^='quantity']").keyup(function() {
            // set all to 0
            $(".total_paid").text(0);
            $(".total_remaining").text(0);
            $(".paid_amount").val(0);

            var total = 0;
            var total_paid = 0;
            var sum = 0;
            var submittedValue = $("#client option:selected").attr('data-submitted-value');

            $("input[name^='quantity']").each(function (index, element) {
                if (!$(this).val()) {
                    total = total + 0;
                }else{
                    sum = parseFloat($(element).val()) * parseFloat($(this).parent().parent().next('div').find('.price_per_one').val());
                    total = total + sum;
                }
            });

            var submitted_value = total * submittedValue / 100;

            $("input[name^='paid_amount']").each(function (index, element) {
                if (!$(this).val()) {
                    total_paid = total_paid + 0;
                }else{
                    total_paid = total_paid + parseFloat($(element).val());
                }
            });

            total_paid = total_paid+submitted_value;

            $(".total_paid").text(total_paid);
            $(".total_remaining").text(Math.abs(total-total_paid));
            $(".paid_amount").val(total_paid);
        });

        // hande changing of price of one piece
        $("input[name^='price']").keyup(function() {

            // set all to 0
            $(".total_paid").text(0);
            $(".total_remaining").text(0);
            $(".paid_amount").val(0);

            var total = 0;
            var total_paid = 0;
            var sum = 0;
            var submittedValue = $("#client option:selected").attr('data-submitted-value');


            $("input[name^='quantity']").each(function (index, element) {
                if (!$(this).val()) {
                    total = total + 0;
                }else{
                    sum = parseFloat($(element).val()) * parseFloat($(this).parent().parent().next('div').find('.price_per_one').val());
                    total = total + sum;
                }
            });

            var submitted_value = total * submittedValue / 100;

            $("input[name^='paid_amount']").each(function (index, element) {
                if (!$(this).val()) {
                    total_paid = total_paid + 0;
                }else{
                    total_paid = total_paid + (parseFloat($(element).val()) * submittedValue / 100);
                }
            });

            total_paid = total_paid+submitted_value;

            $(".total_paid").text(total_paid);
            $(".total_remaining").text(Math.abs(total-total_paid));
            $(".paid_amount").val(total_paid);
        });


        // check confirmation code
        $("#submit-btn").on('click',function (e) {
            e.preventDefault();

            var id = $("#client").find(":selected").val();

            $.ajax({
                'url':'/purchases/send-confirmation-code',
                'type':'POST',
                'data':{
                    'id' : id,
                    "_token": "{{ csrf_token() }}",
                },
                success:function(data){
                    if(data.code == 1){
                        $("#purchase-id").submit();
                        $('#purchase-id').trigger("reset");
                        return false;
                    }else{
                        if(data.code == 0){
                            Swal.fire({
                                position: 'center',
                                type: 'error',
                                title:'حدث خطا في كود التاكيد برجاء محاولة مره اخري',
                                showConfirmButton: true,
                            });
                        }else{
                            confirmCode(data.code);
                        }
                    }

                },
                error : function(){
                    Swal.fire({
                        position: 'center',
                        type: 'error',
                        title:'لقد قمت بالشراءباقصي مبلغ محدد لك من قبل',
                        showConfirmButton: true,
                        timer: 2000
                    });
                    setTimeout(function(){
                        location.reload(true);
                    }, 3000);
                }
            });

        });


    async function confirmCode(code) {
        const ipAPI = 'https://api.ipify.org?format=json'

        const inputValue = fetch(ipAPI)
            .then(response => response.json())
            .then(data => data.ip)

        const {value: userCode} = await Swal.fire({
            title: 'برجاء ادخال كود التاكيد المرسل للعميل لاتمام العملية ؟',
            input: 'text',
            inputValue: '',
            showCancelButton: true,
            inputValidator: (value) => {
                if (value.length != 6) {
                    return 'برجاء ادخال الكود بشكل صحيح مكون 6 حروف / ارقام'
                }
            }
        });

        if (userCode === code) {
            Swal.fire({
                position: 'center',
                type: 'success',
                title:'كود التاكيد صحيح سيتم اتمام العملية بنجاح أنتظر...',
                showConfirmButton: true,
                timer: 2000
            });

            $("#purchase-id").submit();
            window.location = "/purchases";
            setTimeout(function(){
                location.reload(true);
            }, 3000);

            return true
        }
        Swal.fire({
            position: 'center',
            type: 'error',
            title:'كود التاكيد غير صحيح',
            showConfirmButton: true,
            timer: 2000
        });

    }

    $('input').on("keypress", function(e) {
        /* ENTER PRESSED*/
        if (e.keyCode == 13) {
            /* FOCUS ELEMENT */
            var inputs = $(this).parents("form").eq(0).find(":input");
            var idx = inputs.index(this);

            if (idx == inputs.length - 1) {
                inputs[0].select()
            } else {
                inputs[idx + 1].focus(); //  handles submit buttons
                inputs[idx + 1].select();
            }
            return false;
        }
    });

    </script>

@endsection

<div class="row-parent" style="display: none">
    <div class="row" >
        <div class="col-md-3">
            <div class="form-group">
                <label for="example-commodity-name-input" class=" col-form-label">اسم المنتج</label>
                <input class="form-control" value="{{old('commodity_name')}}" name="commodity_name[]" type="text" id="example-commodity-name-input" required>
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group quantity-div">
                <label class="col-form-label">الكمية</label>
                <input class="form-control sum quantity" value="0" name="quantity[]" data-parsley-type="digits" min="1" type="number" required>
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group price-div">
                <label  class="col-form-label">السعر للقطعة الواحده</label>
                <input class="form-control sum price_per_one" value="0" name="price[]" min="0" step="0.01" type="number" required>
            </div>
        </div>
        <div class="col-md-1">
            <div class="form-group">
                <button class="btn btn-xs btn-danger remove" style="margin: 35px 10px;">x</button>
            </div>
        </div>

        <!-- /.col -->
    </div>
</div>
