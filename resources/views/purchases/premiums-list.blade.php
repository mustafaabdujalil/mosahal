@extends('layouts.master')
@section('title')
    جدول الاستحقاقات
@endsection
@section('styles')
    <style>
        #admins tbody tr {
            word-break: break-all;
        }
    </style>
@endsection
@section('modals')
    <div class="modal" tabindex="-1" role="dialog" id="collect_date_modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">تغير تاريخ التحصيل</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="change-collect-date-form" method="post" action="/purchases/premiums/change-collect-date">
                    <div class="modal-body">
                            <div class="col-md-12 form-group">
                                @csrf
                                <label for="collect_date">تاريخ التحصيل الجديد</label>
                                <input type="date" id="collect_date" class="form-control" name="collect_date" required>
                            </div>
                            <input type="hidden" id="premium_id" name="premium_id">
                    </div>
                    <div class="modal-footer">
                        <input type="submit" class="btn btn-success" value="حفظ">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">اغلاق</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
@endsection
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">

                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">جدول الاستحقاقات</h3>
                        </div>

                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="premiums" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th>المبلغ المدفوع</th>
                                    <th>المبلغ المستحق</th>
                                    <th>اجمالي المبلغ المستحق</th>
                                    <th>رقم الشهر</th>
                                    <th> الحالة</th>
                                    <th>كود العملية</th>
                                    <th>تاريخ الاستحقاق</th>
                                    <th>تاريخ التحصيل</th>
                                    <th>المحصل</th>
                                    <th>العمليات</th>
                                </tr>
                                </thead>
                            </table>

                            <div class="form-group">
                                <label for="sub_total" class="col-md-2 below-table">اجمالي المبلغ المدفوع : </label>
                                <div class="col-md-3">
                                    <div class="input-group">
                                        <input id="total" type="text" class="form-control" name="total" readonly>
                                    </div>
                                </div>
                                <label for="sub_total" class="col-md-2 below-table">اجمالي المبلغ المستحق : </label>
                                <div class="col-md-3">
                                    <div class="input-group">
                                        <input id="total_current_balance" type="text" class="form-control" name="total_current_balance" readonly>
                                    </div>
                                </div>
                            </div>
                            <br><br>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
@section('scripts')
    <script type="text/javascript">
        var totalAmount = 0.00;
        var totalCurrentAmount = 0.00;
        var PremiumsTable = $('#premiums').DataTable({
            destroy: true,
            processing: true,
            serverSide: true,
            order: [[0, "asc"]],
            dom: 'bfrtlip',
            buttons: [
                {
                    extend: 'copyHtml5',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3,4,5,6,7,8 ]
                    }
                },
                {
                    extend: 'excelHtml5',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3,4,5,6,7,8 ]
                    }
                },
                {
                    extend: 'pdfHtml5',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3,4,5,6,7,8 ]
                    }
                },
                {
                    extend: 'print',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3,4,5,6,7,8 ]
                    }
                },

            ],
            select: true,
            ajax: {
                "url": '/purchases/fetchPremiums/'+"{{$id}}",
                "type": 'GET',
            },
            columns: [
                {data: 'paid_amount', name: 'paid_amount'},
                {data: 'remaining_amount', name: 'remaining_amount'},
                {data: 'total_remaining_amount', name: 'total_remaining_amount'},
                {data: 'premium_month_number', name: 'premium_month_number'},
                {data: 'status', name: 'status'},
                {data: 'code', name: 'code'},
                {data: 'collect_date', name: 'collect_date'},
                {data: 'updated_at', name: 'updated_at'},
                {data: 'collector', name: 'collector'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ],
            aLengthMenu: [
                [10, 50, 100, 200, -1],
                [10, 50, 100, 200, "All"]
            ],
            "oLanguage": {
                "sSearch": "بحث : "
            },
            'fnCreatedRow': function (nRow, aData, iDataIndex) {

                if(iDataIndex == 0){
                    totalAmount = 0.00;
                    totalCurrentAmount = 0.00;
                }
                totalAmount = parseFloat(aData['paid_amount']) + totalAmount;
                $('#total').val(totalAmount.toFixed(2));
                totalCurrentAmount = parseFloat(aData['remaining_amount']) + totalCurrentAmount;
                $('#total_current_balance').val(totalCurrentAmount.toFixed(2));
            }
        });

        async function pay(id) {

            const ipAPI = 'https://api.ipify.org?format=json'

            const inputValue = fetch(ipAPI)
                .then(response => response.json())
                .then(data => data.ip)

            const {value: paidAmount} = await Swal.fire({
                title: 'برجاء ادخال القيمة المدفوعة ؟',
                input: 'number',
                inputValue: paidAmount,
                showCancelButton: true,
                inputValidator: (value) => {
                    if (!value || value <= 0) {
                        return 'برجاء ادخال القيمة المدفوعة بشكل صحيح'
                    }

                }
            })

            if (paidAmount) {
                Swal.fire({
                    title: 'هل انت متاكد من الدفع ؟',
                    text: "ﻻ تستطيع استعاده القيمة بعد الدفع",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'نعم',
                    cancelButtonText: 'ﻻ',
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            'url':'/purchases/premiums/pay',
                            'type':'POST',
                            'data':{
                                'id' : id,
                                'paidAmount' : paidAmount,
                                "_token": "{{ csrf_token() }}",
                            },
                            success:function(data){
                                PremiumsTable.ajax.reload();
                                Swal.fire({
                                    type: data.status,
                                    title: data.title,
                                    text: data.msg,
                                    footer: '<a href="/purchases/premiums/download-bill-pay/'+data.id+'">Download Bill </a>'
                                });
                                // $('#premiums').DataTable().ajax.reload();
                            },
                            error : function(){
                            }
                        });
                    }
                })
            }
        }

        // relay remaining amount
        function relayRemainingAmount(id,purchase_id) {

            Swal.fire({
                title: 'هل انت متاكد من ترحيل المبلغ المتبقي للشهر القادم ؟',
                text: "ﻻ تستطيع اعاده القيمة بعد الترحيل",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'نعم',
                cancelButtonText: 'ﻻ',
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        'url':'/purchases/premiums/relay-remaining-amount',
                        'type':'POST',
                        'data':{
                            'id' : id,
                            'purchase_id' : purchase_id,
                            "_token": "{{ csrf_token() }}",
                        },
                        success:function(data){
                            Swal.fire({
                                type: data.status,
                                title: data.title,
                                text: data.msg,
                            });
                            $('#premiums').DataTable().ajax.reload();
                        },
                        error : function(){
                        }
                    });
                }
            })
        }

        // relay remaining amount to new month
        function relayRemainingAmountToNewMonth(id) {

            Swal.fire({
                title: 'هل انت متاكد من ترحيل المبلغ المتبقي لشهر اضافي ؟',
                text: "ﻻ تستطيع اعاده القيمة بعد الترحيل",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'نعم',
                cancelButtonText: 'ﻻ',
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        'url':'/purchases/premiums/relay-remaining-amount-to-new-month',
                        'type':'POST',
                        'data':{
                            'id' : id,
                            "_token": "{{ csrf_token() }}",
                        },
                        success:function(data){
                            Swal.fire({
                                type: data.status,
                                title: data.title,
                                text: data.msg,
                            });
                            $('#premiums').DataTable().ajax.reload();
                        },
                        error : function(){
                        }
                    });
                }
            })
        }

        function changeCollectDate(id) {

             $("#premium_id").val(id);
             $("#collect_date_modal").modal('toggle');

            {{--$.ajax({--}}
            {{--    'url':'/purchases/premiums/change-collect-date',--}}
            {{--    'type':'POST',--}}
            {{--    'data':{--}}
            {{--        'id' : id,--}}
            {{--        "_token": "{{ csrf_token() }}",--}}
            {{--    },--}}
            {{--    success:function(data){--}}
            {{--        Swal.fire({--}}
            {{--            type: data.status,--}}
            {{--            title: data.title,--}}
            {{--            text: data.msg,--}}
            {{--        });--}}
            {{--        $('#premiums').DataTable().ajax.reload();--}}
            {{--    },--}}
            {{--    error : function(){--}}
            {{--    }--}}
            {{--});--}}
        }
    </script>
@endsection

