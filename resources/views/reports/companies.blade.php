@extends('layouts.master')
@section('title')
    اجمالي مبيعات الشركات
@endsection
@section('styles')
    <style>
        #companies tbody tr {
            word-break: break-all;
        }
    </style>
@endsection
@section('modals')
@endsection
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">

                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">اجمالي مبيعات الشركات</h3>
                        </div>

                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="companies" class="table-bordered">
                                <thead>
                                <tr>
                                    <th>اسم الشركة</th>
                                    <th>اجمالي المبيعات</th>
                                    <th>العمليات</th>
                                </tr>
                                </thead>
                            </table>
                            <div class="form-group">
                                <label for="sub_total" class="col-md-2 below-table">اجمالي المبيعات: </label>
                                <div class="col-md-3">
                                    <div class="input-group">
                                        <input id="total" type="text" class="form-control" name="total" readonly>
                                    </div>
                                </div>
                            </div>
                            <br><br>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
@section('scripts')
    <script type="text/javascript">
        var totalAmount = 0.00;
        $('#companies').DataTable({
            destroy: true,
            processing: true,
            serverSide: true,
            order: [[0, "asc"]],
            dom: 'bfrtlip',
            buttons: [
                {
                    extend: 'copyHtml5',
                    exportOptions: {
                        columns: [ 0, 1 ]
                    }
                },
                {
                    extend: 'excelHtml5',
                    exportOptions: {
                        columns: [ 0, 1 ]
                    }
                },
                {
                    extend: 'pdfHtml5',
                    exportOptions: {
                        columns: [ 0, 1 ]
                    }
                },
                {
                    extend: 'print',
                    exportOptions: {
                        columns: [ 0, 1 ]
                    }
                },

            ],
            select: true,
            ajax: {
                "url": '{{route("reports.companies.fetch-companies")}}',
                "type": 'GET'
            },
            columns: [
                {data: 'name', name: 'name'},
                {data: 'total_price', name: 'total_price'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ],
            aLengthMenu: [
                [10, 50, 100, 200, -1],
                [10, 50, 100, 200, "All"]
            ],
            "oLanguage": {
                "sSearch": "بحث : "
            },
            'fnCreatedRow': function (nRow, aData, iDataIndex) {

                if(iDataIndex == 0){
                    totalAmount = 0.00;
                }
                totalAmount = parseFloat(aData['total_price']) + totalAmount;
                $('#total').val(totalAmount.toFixed(2));
            }
        });

    </script>
@endsection

