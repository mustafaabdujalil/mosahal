@extends('layouts.master')
@section('title')
    اجمالي مبيعات عملاء شركة {{$company->name}}
@endsection
@section('styles')
    <style>
        #companies tbody tr {
            word-break: break-all;
        }
    </style>
@endsection
@section('modals')
@endsection
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">

                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">اجمالي مبيعات عملاء شركة {{$company->name}}</h3>
                        </div>

                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="clients" class="table-bordered">
                                <thead>
                                <tr>
                                    <th>اسم العميل</th>
                                    <th>اجمالي المبيعات</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
@section('scripts')
    <script type="text/javascript">

        $('#clients').DataTable({
            destroy: true,
            processing: true,
            serverSide: true,
            order: [[0, "asc"]],
            dom: 'bfrtlip',
            buttons: [
                {
                    extend: 'copyHtml5',
                    exportOptions: {
                        columns: [ 0, 1 ]
                    }
                },
                {
                    extend: 'excelHtml5',
                    exportOptions: {
                        columns: [ 0, 1 ]
                    }
                },
                {
                    extend: 'pdfHtml5',
                    exportOptions: {
                        columns: [ 0, 1 ]
                    }
                },
                {
                    extend: 'print',
                    exportOptions: {
                        columns: [ 0, 1 ]
                    }
                },

            ],
            select: true,
            ajax: {
                "url": '/reports/fetch-companies-clients/'+"{{$company->id}}",
                "type": 'GET'
            },
            columns: [
                {data: 'name', name: 'name'},
                {data: 'total_price', name: 'total_price'},
            ],
            aLengthMenu: [
                [10, 50, 100, 200, -1],
                [10, 50, 100, 200, "All"]
            ],
            "oLanguage": {
                "sSearch": "بحث : "
            }
        });

    </script>
@endsection

