@extends('layouts.master')
@section('title')
    اجمالي مبيعات منافذ البيع
@endsection
@section('styles')
    <style>
        #companies tbody tr {
            word-break: break-all;
        }
    </style>
@endsection
@section('modals')
@endsection
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">

                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">اجمالي مبيعات منافذ البيع</h3>
                        </div>

                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="suppliers" class="table-bordered">
                                <thead>
                                <tr>
                                    <th>اسم منفذالبيع</th>
                                    <th>اجمالي المبيعات</th>
                                    <th>المبلغ المخصوم</th>
                                    <th>المبلغ بعد الخصم</th>
                                    <th>المبلغ المسدد</th>
                                    <th>المبلغ المتبقي</th>
                                    <th>العملاء</th>
                                </tr>
                                </thead>
                            </table>
                            <div class="form-group">
                                <label for="sub_total" class="col-md-2 below-table">اجمالي المبيعات : </label>
                                <div class="col-md-3">
                                    <div class="input-group">
                                        <input id="total" type="text" class="form-control" name="total" readonly>
                                    </div>
                                </div>
                                <label for="sub_total" class="col-md-2 below-table">اجمالي المبلغ المخصوم : </label>
                                <div class="col-md-3">
                                    <div class="input-group">
                                        <input id="total_discount" type="text" class="form-control" name="total_discount" readonly>
                                    </div>
                                </div>
                                <label for="sub_total" class="col-md-2 below-table">اجمالي المبلغ بعد الخصم:</label>
                                <div class="col-md-3">
                                    <div class="input-group">
                                        <input id="total_after_discount" type="text" class="form-control" name="total_after_discount" readonly>
                                    </div>
                                </div>
                                <label for="sub_total" class="col-md-2 below-table">اجمالي المبلغ المسدد : </label>
                                <div class="col-md-3">
                                    <div class="input-group">
                                        <input id="total_paid" type="text" class="form-control" name="total_paid" readonly>
                                    </div>
                                </div>
                                <label for="sub_total" class="col-md-2 below-table">اجمالي المبلغ المتبقي : </label>
                                <div class="col-md-3">
                                    <div class="input-group">
                                        <input id="total_remaining" type="text" class="form-control" name="total_remaining" readonly>
                                    </div>
                                </div>
                            </div>
                            <br><br>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
@section('scripts')
    <script type="text/javascript">
        var totalAmount = 0.00;
        var totalDiscount = 0.00;
        var totalAfterDiscount = 0.00;
        var totalPaid = 0.00;
        var totalRemaining= 0.00;

        $('#suppliers').DataTable({
            destroy: true,
            processing: true,
            serverSide: true,
            order: [[0, "asc"]],
            dom: 'bfrtlip',
            buttons: [
                {
                    extend: 'copyHtml5',
                    exportOptions: {
                        columns: [ 0, 1,2,3 ]
                    }
                },
                {
                    extend: 'excelHtml5',
                    exportOptions: {
                        columns: [ 0, 1,2,3 ]
                    }
                },
                {
                    extend: 'pdfHtml5',
                    exportOptions: {
                        columns: [ 0, 1,2,3 ]
                    }
                },
                {
                    extend: 'print',
                    exportOptions: {
                        columns: [ 0, 1,2,3 ]
                    }
                },

            ],
            select: true,
            ajax: {
                "url": '{{route("reports.suppliers.fetch-suppliers")}}',
                "type": 'GET'
            },
            columns: [
                {data: 'name', name: 'name'},
                {data: 'total_price', name: 'total_price'},
                {data: 'discount_percentage_amount', name: 'discount_percentage_amount'},
                {data: 'total_price_after_discount', name: 'total_price_after_discount'},
                {data: 'debt_value', name: 'debt_value'},
                {data: 'remaining_amount', name: 'remaining_amount'},
                {data: 'action', name: 'action'},
            ],
            aLengthMenu: [
                [10, 50, 100, 200, -1],
                [10, 50, 100, 200, "All"]
            ],
            "oLanguage": {
                "sSearch": "بحث : "
            },
            'fnCreatedRow': function (nRow, aData, iDataIndex) {

                if(iDataIndex == 0){
                     totalAmount = 0.00;
                     totalDiscount = 0.00;
                     totalAfterDiscount = 0.00;
                     totalPaid = 0.00;
                     totalRemaining= 0.00;
                }
                totalAmount = parseFloat(aData['total_price']) + totalAmount;
                $('#total').val(totalAmount.toFixed(2));
                totalDiscount = parseFloat(aData['discount_percentage_amount']) + totalDiscount;
                $('#total_discount').val(totalDiscount.toFixed(2));
                totalAfterDiscount = parseFloat(aData['total_price_after_discount']) + totalAfterDiscount;
                $('#total_after_discount').val(totalAfterDiscount.toFixed(2));
                totalPaid = parseFloat(aData['debt_value']) + totalPaid;
                $('#total_paid').val(totalPaid.toFixed(2));
                totalRemaining = parseFloat(aData['remaining_amount']) + totalRemaining;
                $('#total_remaining').val(totalRemaining.toFixed(2));
            }
        });

    </script>
@endsection

