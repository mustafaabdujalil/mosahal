<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientCollector extends Model
{
    //
    protected $table = "client_collectors";
    protected $fillable = ['client_id','collector_id'];
}
