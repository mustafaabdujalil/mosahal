<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CollectivePremiums extends Model
{
    //
    protected $fillable = ['client_id','paid_amount','remaining_amount','total_remaining_amount','premium_month_number',
                            'status','code','collect_date','collector_id'];
}
