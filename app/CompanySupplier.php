<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanySupplier extends Model
{
    //
    protected $fillable = ['company_id','supplier_id'];
    protected $hidden = ['created_at','updated_at'];

    // return company
    public function company(){
        return $this->belongsTo('App\Company','company_id','id');
    }

    // return supplier
    public function supplier(){
        return $this->belongsTo('App\User','supplier_id','id');
    }
}
