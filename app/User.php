<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens,Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','mobile','phone','whatsapp_number','city_id','address','role',
        'image','total_collector_point','activity','status','card_code','discount_value','national_id','fcmToken',
        'debt_value'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token','created_at','updated_at','email_verified_at'
    ];
    /**
     * Return Image with Full path
     *
     *
     */
    public function getImageAttribute($value){
        if(is_null($this->attributes['image'])){
            return "";
        }elseif ($this->attributes['image'] == "user.jpg"){
            return url($this->attributes['image']);
        }elseif(strpos($this->attributes['image'], 'http') > -1) {
            return $this->attributes['image'];
        }

        return url('/storage/'.$this->attributes['image']);
    }

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    // return client
    public function client(){
        return $this->hasOne('App\Client');
    }

    /**
     * The function that return city of user
     *
     *
     */
    public function city(){
        return $this->belongsTo('App\City','city_id','id');
    }

    // retrun collector premiums
    public function collectorPremiums(){
        return $this->hasMany('App\Premiums','collector_id','id');
    }

    // return branch of suppliers
    public function branch(){
        return $this->belongsTo('App\SupplierBranch','supplier_employee_branch_id','id');
    }

    // return supplier employee role
    public function employeeRoles(){
        return $this->hasMany('App\SupplierRole','supplier_id','id');
    }

    // return purchases
    public function purchases(){
        return $this->hasMany('App\Purchase','supplier_id','id');
    }
}
