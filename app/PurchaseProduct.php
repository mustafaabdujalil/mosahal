<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchaseProduct extends Model
{
    //
    protected $hidden = [
        'created_at','updated_at'
    ];

    public function purchase(){
        return $this->belongsTo('App\Purchase','purchase_id','id');
    }
}
