<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SupplierRole extends Model
{
    //
    protected $fillable = ['supplier_id','role'];

    public function supplier(){
        return $this->belongsTo('App\User','supplier_id','id');
    }
}
