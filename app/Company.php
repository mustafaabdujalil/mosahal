<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    // fillable adn hidden
    protected $fillable = ['name','description','manager_name','address','phone','mobile','mobile','logo',
                            'premiums_months','max_value','whatsapp_number','city_id','activity','status'];
    protected $hidden = [
        'password', 'remember_token','created_at','updated_at','email_verified_at'
    ];
    public function getLogoAttribute($value){
        if(is_null($this->attributes['logo'])){
            return "";
        }elseif ($this->attributes['logo'] == "user.jpg"){
            return url($this->attributes['logo']);
        }elseif(strpos($this->attributes['logo'], 'http') > -1) {
            return $this->attributes['logo'];
        }
        return url('/storage/'.$this->attributes['logo']);
    }
    // return clients
    public function clients(){
        return $this->hasMany('App\Client','company_id','id');
    }

    // return city
    public function city(){
        return $this->belongsTo('App\City','city_id','id');
    }

}
