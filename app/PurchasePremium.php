<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchasePremium extends Model
{
    //
    protected $fillable = ['purchase_id','premium_id'];

    public function premium(){
        return $this->belongsTo('App\Premiums','premium_id','id');
    }

    public function purchase(){
        return $this->belongsTo('App\Purchase','purchase_id','id');
    }
}
