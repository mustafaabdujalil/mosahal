<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SupplierBranch extends Model
{
    protected $fillable = [
        'supplier_id', 'branch_name'
    ];
    protected $hidden = [
        'created_at','updated_at'
    ];

    // return supplier data
    public function supplier(){
        return $this->belongsTo('App\User','supplier_id','id');
    }
}
