<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientRelatedPerson extends Model
{
    //
    protected $table = 'client_related_people';
    protected $fillable = ['client_id','related_id'];
}
