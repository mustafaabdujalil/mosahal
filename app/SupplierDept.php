<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SupplierDept extends Model
{
    //
    protected $fillable = ['supplier_id','debt_value'];
}
