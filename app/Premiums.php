<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Premiums extends Model
{
    //
    protected $hidden = [
        'created_at'
    ];
    // return collector
    public function collector(){
        return $this->belongsTo('App\User','collector_id','id');
    }
    // return purchase
    public function purchase(){
        return $this->belongsToMany('App\Purchase','purchase_premia','id','premium_id');
    }
}
