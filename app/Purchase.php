<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
    protected $hidden = [
        'created_at','updated_at'
    ];
    // return client
    public function client(){
        return $this->belongsTo('App\Client','client_id','id');
    }

    // return supplier
    public function supplier(){
        return $this->belongsTo('App\User','supplier_id','id');
    }

    // return premiums
    public function premiums(){
        return $this->belongsToMany('App\Premiums', 'purchase_premia', 'purchase_id', 'premium_id');
    }


    // return products
    public function products(){
        return $this->hasMany('App\PurchaseProduct','purchase_id','id');
    }
}
