<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    //
    protected $fillable = ['name','national_id','gender','social_status','job','company_id','customer_number','department',
        'work_area','salary','address','floor_number','flat_number','area','fork_name','special_mark',
        'city_id','mother_name','mobile','alter_mobile','phone','available_calling_time','whatsapp_number','code',
        'image','dealing_date','rate','submitted_value','repayment_date','follower_name','follower_phone','follower_address',
        'follower_floor_number','follower_flat_number','follower_national_id','follower_area','follower_fork_name','follower_special_mark','relation'
        ,'partner_name','partner_company_name','partner_work_address','partner_job','partner_work_area','partner_department',
        'partner_mobile','status','user_id','fixed_max_value','auto_buy_option'];

    protected $hidden = [
        'created_at','updated_at'
    ];

    // return user
    public function user(){
        return $this->belongsTo('App\User','user_id','id');
    }
    public function getImageAttribute($value){
        if(is_null($this->attributes['image'])){
            return "";
        }elseif ($this->attributes['image'] == "user.jpg"){
            return url($this->attributes['image']);
        }elseif(strpos($this->attributes['image'], 'http') > -1) {
            return $this->attributes['image'];
        }
        return url('/storage/'.$this->attributes['image']);
    }
    // return company
    public function company(){
        return $this->belongsTo('App\Company','company_id','id');
    }
    // return city
    public function city(){
        return $this->belongsTo('App\City','city_id','id');
    }
    // return purchases
    public function purchases(){
        return $this->hasMany('App\Purchase','client_id','id');
    }

    // return collectors
    public function collectors(){
        return $this->hasMany('App\ClientCollector','client_id','id');
    }
    // return relatedPersons
    public function relatedPersons(){
        return $this->hasMany('App\ClientRelatedPerson','client_id','id');
    }
}


