<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class PurchasesAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();
        if(!in_array($user->role,['Admin','Supplier','Collector','SupplierEmployee'])){
            abort(404);
        }
        return $next($request);
    }
}
