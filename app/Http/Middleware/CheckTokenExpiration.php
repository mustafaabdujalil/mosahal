<?php

namespace App\Http\Middleware;

use Closure;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class CheckTokenExpiration
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $userToken = $request->user()->token();
        $expiresDate = $userToken->expires_at;
        $currentDate = Carbon::now();
        if ($currentDate > $expiresDate){
            return response()->json([
                'status' => false,
                'msg' => array ("messages" => ['Login has been expired']),
                'data' => new \stdClass()
            ],401);
        }else{
            return $next($request);
        }
    }
}
