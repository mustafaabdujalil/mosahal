<?php

namespace App\Http\Controllers;

use App\City;
use App\Client;
use App\ClientCollector;
use App\ClientRelatedPerson;
use App\Company;
use App\Premiums;
use App\Purchase;
use App\PurchasePremium;
use App\PurchaseProduct;
use App\SupplierRole;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Image;
use Auth;
use DB;
use Session;
use PDF;

class ClientController extends Controller
{
    // return clients view
    public function index(){
        if(Auth::user()->role == "Admin" || Auth::user()->role == "Collector"){
            return view('clients.index');
        }
        return redirect()->back();
    }

    // fetch clients for datatable
    public function fetchClients()
    {
        if(Auth::user()->role == "Collector"){
            $clients_ids = ClientCollector::where('collector_id',Auth::user()->id)->pluck('client_id');
            $clients = Client::whereIn('id',$clients_ids)->get();
        }else{
            $clients = Client::get();
        }

        return DataTables::of($clients)
            ->addColumn('email', function ($client) {
                $user = User::find($client->user_id);
                return $user->email;
            })
            ->addColumn('name', function ($client) {
                return $client->name;
            })
            ->addColumn('mobile', function ($client) {
                return $client->mobile;
            })
            ->addColumn('phone', function ($client) {
                return $client->phone;
            })
            ->addColumn('whatsapp', function ($client) {
                return $client->whatsapp_number;
            })
            ->addColumn('card_code', function ($client) {
                return $client->user->card_code;
            })
            ->addColumn('address', function ($client) {
                return $client->address;
            })
            ->addColumn('company', function ($client) {
                return $client->company->name;
            })
            ->addColumn('city', function ($client) {
                return $client->city->name;
            })
            ->addColumn('auto_buy_option', function ($client) {
                if($client->auto_buy_option == 0){
                    return "ﻻ";
                }
                return "نعم";
            })
            ->addColumn('remaining_amount', function ($client) {
                $purchases = Purchase::where('client_id',$client->id)->pluck('id');
                $purchases_premiums = PurchasePremium::whereIn('purchase_id',$purchases)->pluck('premium_id');
                $total_remaining_amount = Premiums::whereIn('id',$purchases_premiums)->where('submitted_value_status',0)->sum('remaining_amount');
                return $total_remaining_amount;
            })
            ->addColumn('status', function ($client) {
                if($client->status == 0){
                    return 'Not Active';
                }else{
                    return 'Active';
                }
            })
            ->addColumn('image', function ($client) {
                if($client->image){
                    return '<img src="'.$client->image.'" style="width:30px;">';
                }else{
                    return '<img src="https://placeimg.com/30/30/nature" style="width:30px;">';
                }
            })
            ->addColumn('action', function ($client) {

                    if(Auth::user()->role == "SupplierEmployee"){
                        $supplier_role = SupplierRole::where('supplier_id',Auth::user()->id)->pluck('role')->toArray();
                        if(in_array(0,$supplier_role) || in_array(2,$supplier_role)){
                            $url = '
                                    <a class="btn action-btn" title="دفع" href=\'/client/premiums/'.$client->id.'\'><span class="fa fa-money"></span></a>
                                    <a class="btn action-btn" title="المشتريات" href=\'/client/products/'.$client->id.'\'><span class="fa fa-database"></span></a>
                                    <a class="btn action-btn" title="المرتجع"  href=\'/client/recovery-products/'.$client->id.'\'><span class="fa fa-refresh"></span></a>';
                        }else{
                            $url = '';
                        }
                    }else{
                        $url = '<a class="btn action-btn" title="دفع" href=\'/client/premiums/'.$client->id.'\'><span class="fa fa-money"></span></a>
                               <a class="btn action-btn" title="المشتريات" href=\'/client/products/'.$client->id.'\'><span class="fa fa-database"></span></a>
                                <a class="btn action-btn" title="المرتجع" href=\'/client/recovery-products/'.$client->id.'\'><span class="fa fa-refresh"></span></a>';
                    }

                    if(Auth::user()->role == "Admin"){
                        $url = '
                        <a class="btn action-btn" title="دفع" href=\'/client/premiums/'.$client->id.'\'><span class="fa fa-money"></span></a>
                        <a class="btn action-btn" title="تعديل" href=\'/client/edit/'.$client->id.'\'><span class="fa fa-pencil"></span></a>
                        <a class="btn action-btn" title="حذف" onclick=\'deleteClient('.$client->id.')\'><span class="fa fa-trash-o"></span></a>
                        <a class="btn action-btn" title="المشتريات" href=\'/client/products/'.$client->id.'\'><span class="fa fa-database"></span></a>
                        <a class="btn action-btn" title="المرتجع" href=\'/client/recovery-products/'.$client->id.'\'><span class="fa fa-refresh"></span></a>
                        <a class="btn action-btn" title="عملاء ذات صلة" href=\'/client/related-clients/'.$client->id.'\'><span class="fa fa-users"></span></a>
                        <a class="btn action-btn" href="/client/report/'.$client->id.'" title="طباعة الفاتورة"><span class="fa fa-print"></span></a>
                        ';
                    }

                if(Auth::user()->role == "Collector"){
                    $url = '
                        <a class="btn action-btn" title="دفع" href=\'/client/premiums/'.$client->id.'\'><span class="fa fa-money"></span></a>
                        <a class="btn action-btn" title="الاقساط مستحقة الدفع" href=\'/client/next-premiums/'.$client->id.'\'><span class="fa fa-database"></span></a>

                       ';
                }

                    return $url;

            })
            ->rawColumns(['action', 'image'])
            ->make(true);
    }

    // return add view
    public function addForm(){
        $cities = City::get();
        $companies = Company::get();
        $collectors = User::where('role','Collector')->get();
        $clients = Client::get();
        return view('clients.add',compact('cities','companies','collectors','clients'));
    }

    // store new client
    public function storeAdding(Request $request){

        $rules = [
            'name' => 'required|string',
            'password' => 'required|string|min:6',
            'national_id' => 'required|string',
            'card_code' => 'required|string|unique:users',
            'gender' => 'required|string',
            'social_status' => 'required|string',
            'address' => 'required|string',
            'city_id' => 'required|numeric',
            'company_id' => 'required|numeric',
            'job' => 'required|string',
            'mobile' => 'required|string',
            'submitted_value' => 'required|string',
            'repayment_date' => 'required|numeric',
            'status' => 'required|numeric',
            'auto_buy_option' => 'required|numeric',
            'floor_number' => 'required',
            'flat_number' => 'required',
            'area' => 'required',
            'fork_name' => 'required',
            'special_mark' => 'required',
            'collectors' => 'required',
        ];

        $this->validate($request,$rules);

        $company = Company::find($request->company_id);

        if($request->hasFile('image')){
            $image      = $request->file('image');
            $fileName   = time() . '.' . $image->getClientOriginalExtension();
            $path = 'images/clients/'.$fileName;

            $img = Image::make($image->getRealPath());
            $img->resize(100, 100, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->stream(); // <-- Key point
            \Storage::disk('public')->put('/images/clients'.'/'.$fileName, $img);
        }else{
            $path = 'user.jpg';
        }

        // store user
        $new_user = new User();
        $new_user->name = $request->name;
        $new_user->email = "$request->mobile"."@".$request->name."com";
        $new_user->password = bcrypt($request->password);
        $new_user->mobile = $request->mobile;
        $new_user->address = $request->address;
        $new_user->city_id = $request->city_id;
        $new_user->card_code = $request->card_code;
        $new_user->image = $path;
        $new_user->activity = "Client";
        $new_user->role = "Client";
        $new_user->save();

        // store client
        $new_client= new Client();
        $new_client->user_id = $new_user->id;
        $new_client->name = $request->name;
        $new_client->national_id = $request->national_id;
        $new_client->gender = $request->gender;
        $new_client->social_status = $request->social_status;
        $new_client->job = $request->job;
        $new_client->company_id = $request->company_id;
        $new_client->customer_number = $request->customer_number;
        $new_client->department = $request->department;
        $new_client->work_area = $company->address;
        $new_client->salary = $request->salary;
        $new_client->premiums_months = $company->premiums_months;
        $new_client->max_value = $company->max_value;
        $new_client->fixed_max_value = $company->max_value;
        $new_client->address = $request->address;
        $new_client->floor_number = $request->floor_number;
        $new_client->flat_number = $request->flat_number;
        $new_client->area = $request->area;
        $new_client->fork_name = $request->fork_name;
        $new_client->special_mark = $request->special_mark;
        $new_client->city_id = $request->city_id;
        $new_client->mother_name = $request->mother_name;
        $new_client->mobile = $request->mobile;
        $new_client->alter_mobile = $request->alter_mobile;
        $new_client->phone = $request->phone;
        $new_client->available_calling_time = $request->available_calling_time;
        $new_client->whatsapp_number = $request->whatsapp_number	;
        $new_client->image = $path;
        $new_client->dealing_date = Carbon::now();
        $new_client->submitted_value = $request->submitted_value;
        $new_client->repayment_date = $request->repayment_date;
        $new_client->facebook_link = $request->facebook_link;
        $new_client->follower_name = $request->follower_name;
        $new_client->follower_national_id = $request->follower_national_id;
        $new_client->follower_phone = $request->follower_phone;
        $new_client->follower_address = $request->follower_address;
        $new_client->follower_floor_number = $request->follower_floor_number;
        $new_client->follower_flat_number = $request->follower_flat_number;
        $new_client->follower_area = $request->follower_area;
        $new_client->follower_fork_name = $request->follower_fork_name;
        $new_client->follower_special_mark = $request->follower_special_mark;
        $new_client->relation = $request->relation;
        $new_client->partner_name = $request->partner_name;
        $new_client->partner_company_name = $request->partner_company_name;
        $new_client->partner_work_address = $request->partner_work_address;
        $new_client->partner_job = $request->partner_job;
        $new_client->partner_work_area = $request->partner_work_area;
        $new_client->partner_department = $request->partner_department;
        $new_client->partner_mobile = $request->partner_mobile;
        $new_client->status = $request->status;
        $new_client->auto_buy_option = $request->auto_buy_option;
        $new_client->save();

        // add collectors to client
        if(isset($request->collectors) && count($request->collectors) > 0) {
            foreach ($request->collectors as $collector) {
                ClientCollector::create([
                   'client_id' => $new_client->id,
                   'collector_id' => $collector
                ]);
            }
        }

        // add clients to client
        if(isset($request->clients) && count($request->clients) > 0){
            foreach ($request->clients as $client){
                ClientRelatedPerson::create([
                   'client_id' => $new_client->id,
                   'related_id' => $client
                ]);
            }
        }

        \Session::flash('success', 'Client is created successfully',array('timeout' => 1500));
        return redirect()->route('client.index');
    }

    // return edit view
    public function editForm($id){
        $client = Client::find($id);
        $cities = City::get();
        $companies = Company::get();
        $collectors = User::where('role','Collector')->get();
        $selected_collectors = ClientCollector::where('client_id',$id)->pluck('collector_id')->toArray();
        $clients = Client::where('id','!=',$id)->get();
        $related_clients = ClientRelatedPerson::where('client_id',$id)->pluck('related_id')->toArray();

        return view('clients.edit',compact('client','clients','related_clients','cities','companies',
            'collectors','selected_collectors'));
    }

    // store edit
    public function storeEdit(Request $request){

        $rules = [
            'name' => 'required|string',
            'national_id' => 'required|string',
            'card_code' => 'required|string',
            'gender' => 'required|string',
            'social_status' => 'required|string',
            'address' => 'required|string',
            'city_id' => 'required|numeric',
            'company_id' => 'required|numeric',
            'job' => 'required|string',
            'mobile' => 'required|string',
            'submitted_value' => 'required|string',
            'repayment_date' => 'required|numeric',
            'premiums_months' => 'required|numeric',
            'max_value' => 'required|numeric',
            'status' => 'required|numeric',
            'auto_buy_option' => 'required|numeric',
            'floor_number' => 'required',
            'flat_number' => 'required',
            'area' => 'required',
            'fork_name' => 'required',
            'special_mark' => 'required',
        ];

        $this->validate($request,$rules);

        $company = Company::find($request->company_id);
        $client = Client::find($request->id);

        if($request->hasFile('image')){
            $image      = $request->file('image');
            $fileName   = time() . '.' . $image->getClientOriginalExtension();
            $path = 'images/clients/'.$fileName;

            $img = Image::make($image->getRealPath());
            $img->resize(100, 100, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->stream(); // <-- Key point
            \Storage::disk('public')->put('/images/clients'.'/'.$fileName, $img);
        }else{
            $path = $client->image;
        }

        $client->name = $request->name;
        $client->national_id = $request->national_id;
        $client->gender = $request->gender;
        $client->social_status = $request->social_status;
        $client->job = $request->job;
        $client->company_id = $request->company_id;
        $client->customer_number = $request->customer_number;
        $client->department = $request->department;
        $client->work_area = $company->address;
        $client->salary = $request->salary;
        $client->premiums_months = $request->premiums_months;

        if($request->max_value > $client->fixed_max_value){
            $client->max_value = $client->max_value + ($request->max_value - $client->fixed_max_value);
        }else{
            $client->max_value = $client->max_value - ($client->fixed_max_value - $request->max_value);
        }

        $client->fixed_max_value = $request->max_value;
        $client->address = $request->address;
        $client->facebook_link = $request->facebook_link;
        $client->floor_number = $request->floor_number;
        $client->flat_number = $request->flat_number;
        $client->area = $request->area;
        $client->fork_name = $request->fork_name;
        $client->special_mark = $request->special_mark;
        $client->city_id = $request->city_id;
        $client->mother_name = $request->mother_name;
        $client->mobile = $request->mobile;
        $client->alter_mobile = $request->alter_mobile;
        $client->phone = $request->phone;
        $client->available_calling_time = $request->available_calling_time;
        $client->whatsapp_number = $request->whatsapp_number	;
        $client->image = $path;
        $client->dealing_date = Carbon::now();
        $client->submitted_value = $request->submitted_value;
        $client->repayment_date = $request->repayment_date;
        $client->follower_name = $request->follower_name;
        $client->follower_national_id = $request->follower_national_id;
        $client->follower_phone = $request->follower_phone;
        $client->follower_address = $request->follower_address;
        $client->follower_floor_number = $request->follower_floor_number;
        $client->follower_flat_number = $request->follower_flat_number;
        $client->follower_area = $request->follower_area;
        $client->follower_fork_name = $request->follower_fork_name;
        $client->follower_special_mark = $request->follower_special_mark;
        $client->relation = $request->relation;
        $client->partner_name = $request->partner_name;
        $client->partner_company_name = $request->partner_company_name;
        $client->partner_work_address = $request->partner_work_address;
        $client->partner_job = $request->partner_job;
        $client->partner_work_area = $request->partner_work_area;
        $client->partner_department = $request->partner_department;
        $client->partner_mobile = $request->partner_mobile;
        $client->status = $request->status;
        $client->auto_buy_option = $request->auto_buy_option;
        $client->update();

        // store user
        $update_user = User::find($client->user_id);
        $update_user->name = $request->name;
        $update_user->mobile = $request->mobile;
        $update_user->address = $request->address;
        $update_user->city_id = $request->city_id;
        $update_user->card_code = $request->card_code;
        $update_user->image = $path;
        $update_user->update();


        $client = Client::with('collectors','relatedPersons')->where('id',$request->id)->first();
        $client->collectors()->forcedelete();
        // add collectors to client
        if (isset($request->collectors) && count($request->collectors) > 0){
            foreach ($request->collectors as $collector){
                ClientCollector::create([
                    'client_id' => $request->id,
                    'collector_id' => $collector
                ]);
            }
        }

        $client->relatedPersons()->forcedelete();
        ClientRelatedPerson::where('client_id',$request->id)->forcedelete();
        if(isset($request->clients) && count($request->clients) > 0){
            // add clients to client
            foreach ($request->clients as $related_client){
                ClientRelatedPerson::create([
                    'client_id' => $request->id,
                    'related_id' => $related_client
                ]);
            }
        }


        \Session::flash('success', 'Client data is updated successfully');
        return redirect()->route('client.index');

    }

    // delete client
    public function delete(Request $request){
        $check_client_purchases = Purchase::where('client_id',$request->id)->orderBy('created_at','desc')->get();
        if($check_client_purchases){
            foreach ($check_client_purchases as $purchase){
                if($purchase->remaining_amount > 0){
                    return response()->json(['status' => 'warning']);
                }
            }
        }

        Client::where('id',$request->id)->delete();
        return response()->json(['status' => 'success']);

    }


    // return Premiums
    public function premiums($id){
        return view('clients.premiums-list',compact('id'));
    }

    // fetch premiums
    public function fetchPremiums($id){
        if(Auth::user()->role == "Collector"){
            $purchases_ids = Purchase::where('client_id',$id)->pluck('id');
            $premiums_ids = PurchasePremium::whereIn('purchase_id',$purchases_ids)->pluck('premium_id')->toArray();
            $premiums_ids = array_unique($premiums_ids);
            $premiums = Premiums::whereIn('id',$premiums_ids)->where('submitted_value_status',0)
//                ->where('collect_date','<=',Carbon::now())
                ->get();
        }else{
            $purchases_ids = Purchase::where('client_id',$id)->pluck('id');
            $premiums_ids = PurchasePremium::whereIn('purchase_id',$purchases_ids)->pluck('premium_id')->toArray();
            $premiums = Premiums::whereIn('id',$premiums_ids)->where('submitted_value_status',0)->get();
        }

        return DataTables::of($premiums)
            ->addColumn('paid_amount', function ($premium) {
                return $premium->paid_amount;
            })
            ->addColumn('remaining_amount', function ($premium) {
                return $premium->remaining_amount;
            })
            ->addColumn('total_remaining_amount', function ($premium) {
                return $premium->total_remaining_amount;
            })
            ->addColumn('premium_month_number', function ($premium) {
                return $premium->premium_month_number;
            })
            ->addColumn('status', function ($premium) {
                if($premium->status == 0){
                    return 'لم يتم الدفع';
                }elseif ($premium->status == 1){
                    return 'تم الدفع';
                }else{
                    return 'متاخر';
                }
            })
            ->addColumn('code', function ($premium) {
                return $premium->code;
            })
            ->addColumn('collect_date', function ($premium) {
                return $premium->collect_date;
            })
            ->addColumn('updated_at', function ($premium) {
                if($premium->paid_amount > 0){
                    return $premium->updated_at;
                }
                return "-";
            })
            ->addColumn('collector', function ($premium) {
                if($premium->collector_id == 0){
                    return 'لم يتم التحصيل';
                }else{
                    return $premium->collector->name;
                }
            })
            ->addColumn('action', function ($premium) {
                $purchase_premium = PurchasePremium::where('premium_id',$premium->id)->first();

//                if(($premium->collect_date <= date('Y-m-d'))){
                    if($premium->status != 1){
                        return '<a class="btn action-btn" onclick=\'pay('.$premium->id.','.$premium->remaining_amount.')\' title="ادفع"><span class="fa fa-money"></span></a>';
                    }else{
                        if(Auth::user()->role == 'Admin' && $premium->remaining_amount > 0 && $premium->paid_amount > 0){
                            return '
                                    <a class="btn action-btn" onclick=\'relayRemainingAmount('.$premium->id.','.$purchase_premium->purchase_id.')\' title="ترحيل للشهر القادم"><span class="fa fa-hand-grab-o"></span></a>
                                    <a class="btn action-btn" onclick=\'relayRemainingAmountToNewMonth('.$premium->id.','.$purchase_premium->purchase_id.')\' title="ترحيل لشهر اضافي"><span class="fa fa-plus"></span></a>
                                    <a class="btn action-btn" onclick=\'pay('.$premium->id.','.$premium->remaining_amount.')\' title="ادفع"><span class="fa fa-money"></span></a>
                                    <a class="btn action-btn" href="/purchases/premiums/download-bill-pay/'.$premium->id.'" title="طباعة الفاتورة"><span class="fa fa-print"></span></a>';

                        }else{
                            return '<a class="btn action-btn" href="/purchases/premiums/download-bill-pay/'.$premium->id.'" title="طباعة الفاتورة"><span class="fa fa-print"></span></a>';
                        }
                    }
//                }


            })
            ->rawColumns(['action'])
            ->make(true);
    }

    // purchases products
    public function products($id){
        return view('clients.products',compact('id'));
    }

    // fetch purchases products
    public function fetchProducts($id)
    {
        $purchases_ids = Purchase::where('client_id',$id)->pluck('id');
        $products = PurchaseProduct::whereIn('purchase_id',$purchases_ids)->where('recovery_status',0)->get();

        return DataTables::of($products)
            ->addColumn('commodity_name', function ($purchase) {
                return $purchase->commodity_name;
            })
            ->addColumn('quantity', function ($products) {
                return $products->quantity;
            })
            ->addColumn('price', function ($products) {
                return $products->price;
            })
            ->addColumn('total_price', function ($products) {
                return $products->total_price;
            })
            ->make(true);
    }


    // purchases recovery products
    public function recoveryProducts($id){
        return view('clients.recovery-products',compact('id'));
    }

    // fetch purchases recovery products
    public function fetchRecoveryProducts($id)
    {
        $purchases_ids = Purchase::where('client_id',$id)->pluck('id')->toArray();


        $total_recovery_products_ids = PurchaseProduct::whereIn('purchase_id',$purchases_ids)
                                                        ->where('recovery_status',1)
                                                        ->pluck('id');

        $some_recovery_products_ids = PurchaseProduct::whereIn('purchase_id',$purchases_ids)
                                                    ->where('recovery_status',0)
                                                    ->where('recovery_count','>',0)->pluck('id');


        $products = PurchaseProduct::whereIn('id',$total_recovery_products_ids)
                                    ->orWhereIn('id',$some_recovery_products_ids)->get();

        return DataTables::of($products)
            ->addColumn('commodity_name', function ($purchase) {
                return $purchase->commodity_name;
            })
            ->addColumn('quantity', function ($products) {
                return $products->recovery_count;
            })
            ->addColumn('price', function ($products) {
                return $products->price;
            })
            ->addColumn('total_price', function ($products) {
                return $products->price * $products->recovery_count;
            })
            ->make(true);
    }

    // return Premiums
    public function nextPremiums($id){
        Session::forget('from');
        Session::forget('to');
        return view('clients.next-premiums-list',compact('id'));
    }

    // filter next premiums
    public function filterNextPremiums(Request $request){
        Session::forget('from');
        Session::forget('to');
        if($request->has('from')&&$request->has('to')){
            Session::put('from',$request->from);
            Session::put('to',$request->to);
        }
        $id = $request->id;
        return view('clients.next-premiums-list',compact('id'));
    }


    // fetch premiums
    public function fetchNextPremiums($id){

        $purchases_ids = Purchase::where('client_id',$id)->pluck('id');
        $premiums_ids = PurchasePremium::whereIn('purchase_id',$purchases_ids)->pluck('premium_id')->toArray();
        $premiums_ids = array_unique($premiums_ids);
        $premiums = Premiums::whereIn('id',$premiums_ids)->where('remaining_amount','>',0)->where('submitted_value_status',0)->get();
        if(Session::has('from') && Session::has('to')){
            $premiums = Premiums::whereIn('id',$premiums_ids)->where('remaining_amount','>',0.00)
                                ->where('status',0)
                                ->where('submitted_value_status',0)
                                ->whereBetween(DB::raw('DATE(collect_date)'), array(Session::get('from'), Session::get('to')))
                                ->get();
        }

        return DataTables::of($premiums)
            ->addColumn('paid_amount', function ($premium) {
                return $premium->paid_amount;
            })
            ->addColumn('remaining_amount', function ($premium) {
                return $premium->remaining_amount;
            })
            ->addColumn('total_remaining_amount', function ($premium) {
                return $premium->total_remaining_amount;
            })
            ->addColumn('premium_month_number', function ($premium) {
                return $premium->premium_month_number;
            })
            ->addColumn('status', function ($premium) {
                if($premium->status == 0){
                    return 'لم يتم الدفع';
                }elseif ($premium->status == 1){
                    return 'تم الدفع';
                }else{
                    return 'متاخر';
                }
            })
            ->addColumn('code', function ($premium) {
                return $premium->code;
            })
            ->addColumn('collect_date', function ($premium) {
                return $premium->collect_date;
            })
            ->addColumn('collector', function ($premium) {
                if($premium->collector_id == 0){
                    return 'لم يتم التحصيل';
                }else{
                    return $premium->collector->name;
                }
            })
            ->addColumn('action', function ($premium) {
                $purchase_premium = PurchasePremium::where('premium_id',$premium->id)->first();

//                if(($premium->collect_date <= date('Y-m-d'))){
                if($premium->status != 1){
                    return '<a class="btn action-btn" onclick=\'pay('.$premium->id.','.$premium->remaining_amount.')\' title="ادفع"><span class="fa fa-money"></span></a>';
                }else{
                    if(Auth::user()->role == 'Admin' && $premium->remaining_amount > 0 && $premium->paid_amount > 0){
                        return '
                                    <a class="btn action-btn" onclick=\'relayRemainingAmount('.$premium->id.','.$purchase_premium->purchase_id.')\' title="ترحيل للشهر القادم"><span class="fa fa-hand-grab-o"></span></a>
                                    <a class="btn action-btn" onclick=\'relayRemainingAmountToNewMonth('.$premium->id.','.$purchase_premium->purchase_id.')\' title="ترحيل لشهر اضافي"><span class="fa fa-plus"></span></a>
                                    <a class="btn action-btn" onclick=\'pay('.$premium->id.','.$premium->remaining_amount.')\' title="ادفع"><span class="fa fa-money"></span></a>
                                    <a class="btn action-btn" href="/purchases/premiums/download-bill-pay/'.$premium->id.'" title="طباعة الفاتورة"><span class="fa fa-print"></span></a>';

                    }else{
                        return '<a class="btn action-btn" href="/purchases/premiums/download-bill-pay/'.$premium->id.'" title="طباعة الفاتورة"><span class="fa fa-print"></span></a>';
                    }
                }
//                }
            })
            ->rawColumns(['action'])
            ->make(true);
    }


    // return client view
    public function getClient(Request $request){

        Session::forget('card_code');
        Session::forget('national_id');
        if(Auth::user()->role == "SupplierEmployee" || Auth::user()->role == "Supplier"){
            if(Auth::user()->role == "SupplierEmployee" ){
                $supplier_role = SupplierRole::where('supplier_id',Auth::user()->id)->pluck('role')->toArray();
                if(in_array(0,$supplier_role) || in_array(2,$supplier_role)
                    || in_array(1,$supplier_role)){
                    Session::put('card_code',$request->card_code);
                    Session::put('national_id',$request->national_id);
                    return view('suppliers.clients.index');
                }else{
                    return redirect()->back();
                }
            }
            $supplier_role = SupplierRole::where('supplier_id',Auth::user()->id)->pluck('role')->toArray();
            Session::put('card_code',$request->card_code);
            Session::put('national_id',$request->national_id);
            return view('suppliers.clients.index');
        }
        return redirect()->back();

    }

    // fetch client for datatable for supplier
    public function fetchClient(Request $request)
    {
        $clients = [];
        if(Session::has('card_code') && Session::has('national_id')){
            $user = User::where('card_code',Session::get('card_code'))->first();
            if(!is_null($user)){
                $client = Client::where('user_id',$user->id)->first();
                $last_national_id_four_char = substr($client->national_id, -4);
                if(Session::get('national_id') == $last_national_id_four_char){
                    $clients = Client::where('user_id',$user->id)->get();
                }
            }
        }

        return DataTables::of($clients)
            ->addColumn('email', function ($client) {
                $user = User::find($client->user_id);
                return $user->email;
            })
            ->addColumn('name', function ($client) {
                return $client->name;
            })
            ->addColumn('mobile', function ($client) {
                return $client->mobile;
            })
            ->addColumn('phone', function ($client) {
                return $client->phone;
            })
            ->addColumn('whatsapp', function ($client) {
                return $client->whatsapp_number;
            })
            ->addColumn('card_code', function ($client) {
                return $client->user->card_code;
            })
            ->addColumn('address', function ($client) {
                return $client->address;
            })
            ->addColumn('company', function ($client) {
                return $client->company->name;
            })
            ->addColumn('city', function ($client) {
                return $client->city->name;
            })
            ->addColumn('remaining_amount', function ($client) {
                $purchases = Purchase::where('client_id',$client->id)->pluck('id');
                $purchases_premiums = PurchasePremium::whereIn('purchase_id',$purchases)->pluck('premium_id');
                $total_remaining_amount = Premiums::whereIn('id',$purchases_premiums)->where('submitted_value_status',0)->sum('remaining_amount');
                return $total_remaining_amount;
            })
            ->addColumn('status', function ($client) {
                if($client->status == 0){
                    return 'Not Active';
                }else{
                    return 'Active';
                }
            })
            ->addColumn('image', function ($client) {
                if($client->image){
                    return '<img src="'.$client->image.'" style="width:30px;">';
                }else{
                    return '<img src="https://placeimg.com/30/30/nature" style="width:30px;">';
                }
            })
            ->addColumn('action', function ($client) {
                $url = '';

                // check last premiums
                $purchases_ids = Purchase::where('client_id',$client->id)->pluck('id')->toArray();
                $premiums_ids = PurchasePremium::whereIn('purchase_id',$purchases_ids)
                                        ->select('premium_id')
                                        ->groupBy('premium_id')
                                        ->pluck('premium_id')->toArray();
                $last_premium = Premiums::whereIn('id',$premiums_ids)->where('status',0)
                    ->where('submitted_value_status',0)->orderBy('collect_date','asc')
                                    ->first();

                if(Auth::user()->role == "SupplierEmployee") {
                    $supplier_role = SupplierRole::where('supplier_id', Auth::user()->id)->pluck('role')->toArray();

                    // all permission
                    if (in_array(0, $supplier_role)) {

                        $url .= '<a class="btn action-btn" title="دفع" href=\'/client/premiums/' . $client->id . '\'><span class="fa fa-money"></span></a>
                        <a class="btn action-btn" title="المشتريات" href=\'/client/products/' . $client->id . '\'><span class="fa fa-database"></span></a>';

                        if(!is_null($last_premium)){
                            if($last_premium->collect_date > date('Y-m-d')){
                                $url .='<a class="btn action-btn" title="اضافة قسيمة بيع" href=\'/purchases/add_new_purchases/'.$client->id.'\'><span class="fa fa-plus"></span></a>';
                            }
                        }else{
                            $url .='<a class="btn action-btn" title="اضافة قسيمة بيع" href=\'/purchases/add_new_purchases/'.$client->id.'\'><span class="fa fa-plus"></span></a>';
                        }


                    }

                    // buy permission
                    if (in_array(1, $supplier_role)) {

                        if(!is_null($last_premium)){
                            if($last_premium->collect_date > date('Y-m-d')){
                                $url .='<a class="btn action-btn" title="اضافة قسيمة بيع" href=\'/purchases/add_new_purchases/'.$client->id.'\'><span class="fa fa-plus"></span></a>';
                            }
                        }else{
                            $url .='<a class="btn action-btn" title="اضافة قسيمة بيع" href=\'/purchases/add_new_purchases/'.$client->id.'\'><span class="fa fa-plus"></span></a>';
                        }
                    }

                    // collecting permission
                    if (in_array(2, $supplier_role)) {
                        $url .= '<a class="btn action-btn" title="دفع" href=\'/client/premiums/' . $client->id . '\'><span class="fa fa-money"></span></a>';
                    }

                    // collecting permission
                    if (in_array(3, $supplier_role)) {
                        $url .= '<a class="btn action-btn" title="المشتريات" href=\'/client/products/' . $client->id . '\'><span class="fa fa-database"></span></a>';
                    }

                }else{
                    $url = '<a class="btn action-btn" title="دفع" href=\'/client/premiums/'.$client->id.'\'><span class="fa fa-money"></span></a>
                        <a class="btn action-btn" title="المشتريات" href=\'/client/products/'.$client->id.'\'><span class="fa fa-database"></span></a>';                }

                $url .= '<a class="btn action-btn" href="/client/report/'.$client->id.'" title="طباعة تقرير"><span class="fa fa-print"></span></a>';

                if(!is_null($last_premium)){
                    if($last_premium->collect_date > date('Y-m-d')){
                        $url .='<a class="btn action-btn" title="اضافة قسيمة بيع" href=\'/purchases/add_new_purchases/'.$client->id.'\'><span class="fa fa-plus"></span></a>';
                    }
                }else{
                    $url .='<a class="btn action-btn" title="اضافة قسيمة بيع" href=\'/purchases/add_new_purchases/'.$client->id.'\'><span class="fa fa-plus"></span></a>';
                }

                return $url;

            })
            ->rawColumns(['action', 'image'])
            ->make(true);
    }

    // return client view
    public function getClientByName(Request $request){

        Session::forget('name');
        if(Auth::user()->role == "SupplierEmployee" || Auth::user()->role == "Supplier"){
            if(Auth::user()->role == "SupplierEmployee" ){
                $supplier_role = SupplierRole::where('supplier_id',Auth::user()->id)->pluck('role')->toArray();
                if(in_array(0,$supplier_role) || in_array(2,$supplier_role)
                    || in_array(1,$supplier_role)){
                    Session::put('name',$request->name);
                    return view('suppliers.clients.search-by-name');
                }else{
                    return redirect()->back();
                }
            }
            $supplier_role = SupplierRole::where('supplier_id',Auth::user()->id)->pluck('role')->toArray();
            Session::put('name',$request->name);
            return view('suppliers.clients.search-by-name');
        }
        return redirect()->back();

    }

    // fetch client for datatable for supplier by name
    public function fetchClientByName(Request $request)
    {
        $clients = [];
        if(Session::has('name')){
            $userIds = User::where('name','like','%'.Session::get('name').'%')->pluck('id')->toArray();
            if(count($userIds) > 0){
                $clients = Client::whereIn('user_id',$userIds)->get();
            }else{
                $clients = [];
            }
        }else{
            $clients = [];
        }



        return DataTables::of($clients)
            ->addColumn('email', function ($client) {
                $user = User::find($client->user_id);
                return $user->email;
            })
            ->addColumn('name', function ($client) {
                return $client->name;
            })
            ->addColumn('mobile', function ($client) {
                return $client->mobile;
            })
            ->addColumn('phone', function ($client) {
                return $client->phone;
            })
            ->addColumn('whatsapp', function ($client) {
                return $client->whatsapp_number;
            })
            ->addColumn('card_code', function ($client) {
                return $client->user->card_code;
            })
            ->addColumn('address', function ($client) {
                return $client->address;
            })
            ->addColumn('company', function ($client) {
                return $client->company->name;
            })
            ->addColumn('city', function ($client) {
                return $client->city->name;
            })
            ->addColumn('remaining_amount', function ($client) {
                $purchases = Purchase::where('client_id',$client->id)->pluck('id');
                $purchases_premiums = PurchasePremium::whereIn('purchase_id',$purchases)->pluck('premium_id');
                $total_remaining_amount = Premiums::whereIn('id',$purchases_premiums)
                    ->where('submitted_value_status',0)->sum('remaining_amount');
                return $total_remaining_amount;
            })
            ->addColumn('status', function ($client) {
                if($client->status == 0){
                    return 'Not Active';
                }else{
                    return 'Active';
                }
            })
            ->addColumn('image', function ($client) {
                if($client->image){
                    return '<img src="'.$client->image.'" style="width:30px;">';
                }else{
                    return '<img src="https://placeimg.com/30/30/nature" style="width:30px;">';
                }
            })
            ->addColumn('action', function ($client) {
                $url = '';

                if(Auth::user()->role == "SupplierEmployee") {
                    $supplier_role = SupplierRole::where('supplier_id', Auth::user()->id)->pluck('role')->toArray();
                    // all permission
                    if (in_array(0, $supplier_role) || in_array(2, $supplier_role)) {
                        $url .= '<a class="btn action-btn" title="دفع" href=\'/client/premiums/' . $client->id . '\'><span class="fa fa-money"></span></a>';

                    }

                }else if(Auth::user()->role == "Supplier") {
                    $url = '<a class="btn action-btn" title="دفع" href=\'/client/premiums/' . $client->id . '\'><span class="fa fa-money"></span></a>';
                    return $url;
                }else{
                    return $url;
                }
            })
            ->rawColumns(['action', 'image'])
            ->make(true);
    }



    // download pay bill
    public function report($client_id){
        $client = Client::find($client_id);
        $company = Company::where('id',$client->company_id)->first();
        $purchases_ids = Purchase::where('client_id',$client_id)->pluck('id');
        $premiums_ids = PurchasePremium::whereIn('purchase_id',$purchases_ids)->pluck('premium_id');
        $premiums = Premiums::whereIn('id',$premiums_ids)->where('submitted_value_status',0)->get();
        $bought_products = PurchaseProduct::whereIn('purchase_id',$purchases_ids)->get();
        $refund_products = PurchaseProduct::whereIn('purchase_id',$purchases_ids)->where('recovery_status',1)->get();
        $refund_products_total_price = PurchaseProduct::whereIn('purchase_id',$purchases_ids)
            ->where('recovery_status',1)->sum('total_price');
        $total_paid = Premiums::whereIn('id',$premiums_ids)->where('submitted_value_status',0)->sum('paid_amount');
        $total_remaining = Premiums::whereIn('id',$premiums_ids)->where('submitted_value_status',0)->sum('remaining_amount');

//        $data = ['premiums' => $premiums,'company' => $company,
//            'client'=>$client,'bought_products'=>$bought_products,'refund_products'=>$refund_products,
//            'total_paid'=>$total_paid,'total_remaining_amount'=>$total_remaining,'refund_products_total_price'=>$refund_products_total_price];
////        $pdf = \PDF::loadView('bills.report', $data);


        $html = view('bills.report',['premiums' => $premiums,'company' => $company,
            'client'=>$client,'bought_products'=>$bought_products,'refund_products'=>$refund_products,
            'total_paid'=>$total_paid,'total_remaining_amount'=>$total_remaining,'refund_products_total_price'=>$refund_products_total_price])->render(); // file render
// or pure html
        $pdfarr = [
            'title'=>'تقرير شامل',
            'data'=>$html, // render file blade with content html
            'header'=>['show'=>false], // header content
            'footer'=>['show'=>false], // Footer content
            'font'=>'aealarabiya', //  dejavusans, aefurat ,aealarabiya ,times
            'font-size'=>12, // font-size
            'text'=>'', //Write
            'rtl'=>true, //true or false
            'creator'=>'Mosahal', // creator file - you can remove this key
            'keywords'=>'report', // keywords file - you can remove this key
            'subject'=>'report', // subject file - you can remove this key
            'filename'=>'report.pdf', // filename example - invoice.pdf
            'display'=>'print', // stream , download , print
        ];

        return \PDF::HTML($pdfarr);


//        return $pdf->download('report.pdf');
    }





    // purchases recovery products
    public function relatedClients($id){
        return view('clients.related_clients',compact('id'));
    }

    // fetch purchases recovery products
    public function fetchRelatedClients($id)
    {
        $related_clients_ids = ClientRelatedPerson::where('client_id',$id)->pluck('related_id')->toArray();
        $clients = Client::whereIn('id',$related_clients_ids)->get();

        return DataTables::of($clients)
            ->addColumn('name', function ($client) {
                return $client->name;
            })
            ->addColumn('phone', function ($client) {
                return $client->phone;
            })
            ->make(true);
    }







}
