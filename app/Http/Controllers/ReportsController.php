<?php

namespace App\Http\Controllers;

use App\Client;
use App\Company;
use App\Premiums;
use App\Purchase;
use App\PurchasePremium;
use App\SupplierDept;
use App\User;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class ReportsController extends Controller
{
    // return companies view
    public function companies(){
        return view('reports.companies');
    }

    // fetch companies for datatable
    public function fetchCompanies()
    {
        $companies = Company::whereHas('clients.purchases')->get();

        return DataTables::of($companies)
            ->addColumn('name', function ($company) {
                return $company->name;
            })
            ->addColumn('total_price', function ($company) {

                $clients_ids = Client::where('company_id',$company->id)->pluck('id');
                $total_price = Purchase::whereIn('client_id',$clients_ids)->sum('total_price');
                return $total_price;
            })
            ->addColumn('action', function ($company) {
                return '<a class="btn action-btn" title="العملاء" href=\'/reports/companies/clients/'.$company->id.'\'>
                            <span class="fa fa-users"></span></a>';
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    // return companies clients
    public function companiesClients($id){
        $company = Company::find($id);
        return view('reports.company-clients',compact('company'));
    }

    // fetch companies clients for datatable
    public function fetchCompaniesClients($id)
    {
        $clients = Client::whereHas('purchases')->where('company_id',$id)->get();

        return DataTables::of($clients)
            ->addColumn('name', function ($client) {
                return $client->name;
            })
            ->addColumn('total_price', function ($client) {

                $total_price = Purchase::where('client_id',$client->id)->sum('total_price');
                return $total_price;
            })->make(true);
    }

    // return suppliers view
    public function suppliers(){
        return view('reports.suppliers');
    }

    // fetch companies for datatable
    public function fetchSuppliers()
    {
        if(Auth::user()->role == "Admin"){
            $suppliers = User::whereHas('purchases')->where('role','Supplier')->get();
        }else{
            $suppliers = User::whereHas('purchases')->where('id',Auth::user()->id)->where('role','Supplier')->get();
        }

        return DataTables::of($suppliers)
            ->addColumn('name', function ($supplier) {
                return $supplier->name;
            })
            ->addColumn('total_price', function ($supplier) {

                $total_price = Purchase::where('supplier_id',$supplier->id)->sum('total_price');
                return $total_price;
            })
            ->addColumn('discount_percentage_amount', function ($supplier) {
                $total_price = Purchase::where('supplier_id',$supplier->id)->sum('total_price');
                $discount_percentage_amount = $total_price * $supplier->discount_value / 100;
                return $discount_percentage_amount;
            })
            ->addColumn('total_price_after_discount', function ($supplier) {

                $discount_percentage_amount = 0;
                $total_price = Purchase::where('supplier_id',$supplier->id)->sum('total_price');
                $discount_percentage_amount = $total_price * $supplier->discount_value / 100;

                return $total_price - $discount_percentage_amount;
            })
            ->addColumn('debt_value', function ($supplier) {

                $debt_value = SupplierDept::where('supplier_id',$supplier->id)->sum('debt_value');

                return $debt_value;

            })
            ->addColumn('remaining_amount', function ($supplier) {

                $discount_percentage_amount = 0;
                $total_price = Purchase::where('supplier_id',$supplier->id)->sum('total_price');
                $discount_percentage_amount = $total_price * $supplier->discount_value / 100;

                $debt_value = SupplierDept::where('supplier_id',$supplier->id)->sum('debt_value');

                return $total_price - $discount_percentage_amount - $debt_value ;

            })

            ->addColumn('action', function ($company) {
                return '<a class="btn action-btn" title="العملاء" href=\'/reports/supplier/clients/'.$company->id.'\'>
                            <span class="fa fa-users"></span></a>';
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    // return supplier clients
    public function supplierClients($id){
        $supplier = User::find($id);
        return view('reports.supplier-clients',compact('supplier'));
    }

    // fetch supplier clients for datatable
    public function fetchSupplierClients($id)
    {
        $purchases = Purchase::where('supplier_id',$id)->select('client_id', DB::raw('sum(total_price) as total'))
            ->groupBy('client_id')->get();

        return DataTables::of($purchases)
            ->addColumn('name', function ($purchase) {
                $client = Client::find($purchase->client_id);
                return $client->name;
            })
            ->addColumn('total_price', function ($purchase) {
                // dd($purchase);
                return $purchase->total;
            })->make(true);
    }

}
