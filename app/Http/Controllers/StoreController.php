<?php

namespace App\Http\Controllers;

use App\Premiums;
use App\Purchase;
use App\PurchasePremium;
use App\SupplierDept;
use App\User;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Session;
use Illuminate\Support\Facades\Auth;

class StoreController extends Controller
{
    // return store view
    public function index(Request $request){

        $premiums = Premiums::with('collector')
            ->where('paid_amount','>',0);

        $total_debt_amount = SupplierDept::sum('debt_value');
        $flag = 1;

        if(!is_null($request->from) && !is_null($request->to)){
            Session::forget('store_from');
            Session::forget('store_to');
            Session::put('store_from',$request->from);
            Session::put('store_to',$request->to);
            $premiums = $premiums->whereBetween('collect_date', [$request->from, $request->to]);
        }

        if (isset($request->collector) && count($request->collector) > 0){
            Session::forget('store_collector');
            Session::put('store_collector',$request->collector);

            if(!in_array(0,$request->collector)){
                $premiums = $premiums->whereIn('collector_id',$request->collector);
            }

        }

        if(Auth::user()->role == "Supplier"){
            $premiums = $premiums->where('collector_id',Auth::user()->id);
            $total_debt_amount = SupplierDept::where('supplier_id',Auth::user()->id)->sum('debt_value');
        }

        if (isset($request->collector) && count($request->collector) == 1 && !in_array(0,$request->collector)){
            $check_if_admin = User::whereIn('id',$request->collector)->first();
            if($check_if_admin->role == "Admin"){
                $total_debt_amount = 0;
                $flag = 0;
            }
        }

        if($premiums->count() == 0){
            $total_debt_amount = 0;
            $flag = 0;
        }

        $collectors = User::whereHas('collectorPremiums')->get();
        $total_collecting_amount = $premiums->sum('paid_amount');


        return view('store.index',compact('collectors','flag','total_collecting_amount','total_debt_amount'));
    }

    // fetch store for datatable
    public function fetchCollectors()
    {
        $collectorsIds = User::whereHas('collectorPremiums')->pluck('id')->toArray();

        if(Session::has('store_collector') && !in_array(0,Session::get('store_collector'))){
            $collectorsIds = Session::get('store_collector');
        }

        if(Session::has('store_from') && Session::has('store_to')) {

            $from = Session::get('store_from');
            $to = Session::get('store_to');

            $premiums = Premiums::with('collector')
                ->where('paid_amount','>',0)
                ->whereBetween('collect_date', [$from, $to])
                ->whereIn('collector_id',$collectorsIds)
                ->get();

        }else{
//            $collectors = User::whereIn('id',$collectorsIds)->get();
            $premiums = Premiums::with('collector')
                                ->where('paid_amount','>',0)
                                ->whereIn('collector_id',$collectorsIds)
                                ->get();

        }
        return DataTables::of($premiums)
            ->addColumn('collector_name', function ($premium) {
                return isset($premium->collector) ? $premium->collector->name : "";
            })
            ->addColumn('client_name', function ($premium) {
                $purchase_id = PurchasePremium::where('premium_id',$premium->id)->value('purchase_id');
                $purchase = Purchase::with('client')->where('id',$purchase_id)->first();
                return isset($purchase->client) ? $purchase->client->name : "";
            })
            ->addColumn('collect_date', function ($premium) {
                return $premium->collect_date;
            })
            ->addColumn('paid_amount', function ($premium) {
                return $premium->paid_amount;
            })
            ->addColumn('remaining_amount', function ($premium) {
                return $premium->remaining_amount;
            })
            ->addColumn('submitted_value_status', function ($premium) {
                if($premium->submitted_value_status == 1){
                    return "مقدم";
                }else{
                    return "قسط";
                }
            })
            ->make(true);
    }

    public function resetFilterForm(){
        Session::forget('store_from');
        Session::forget('store_to');
        Session::forget('store_collector');

        return response()->json(['status'=>true,'url'=>url('/store')]);
    }
}
