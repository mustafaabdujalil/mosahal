<?php

namespace App\Http\Controllers;

use App\City;
use App\Premiums;
use App\User;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Image;
use DB;
use Auth;

class CollectorController extends Controller
{
    // return collectors view
    public function index(){
        return view('collectors.index');
    }

    // fetch suppliers for datatable
    public function fetchCollectors()
    {
        $collectors = User::where('role', 'Collector')->get();

        return DataTables::of($collectors)
            ->addColumn('name', function ($collector) {
                return $collector->name;
            })
            ->addColumn('email', function ($collector) {
                return $collector->email;
            })
            ->addColumn('mobile', function ($collector) {
                return $collector->mobile;
            })
            ->addColumn('phone', function ($collector) {
                return $collector->phone;
            })
            ->addColumn('whatsapp', function ($collector) {
                return $collector->whatsapp_number;
            })
            ->addColumn('address', function ($collector) {
                return $collector->address;
            })
            ->addColumn('total_collector_point', function ($collector) {
                return $collector->total_collector_point;
            })
            ->addColumn('city', function ($collector) {
                return $collector->city->name;
            })
            ->addColumn('status', function ($collector) {
                if($collector->status == 0){
                    return 'Not Active';
                }else{
                    return 'Active';
                }
            })
            ->addColumn('image', function ($collector) {
                if($collector->image){
                    return '<img src="'.$collector->image.'" style="width:30px;">';
                }else{
                    return '<img src="https://placeimg.com/30/30/nature" style="width:30px;">';
                }
            })
            ->addColumn('action', function ($collector) {

                return '
                <a class="btn action-btn" href=\'/collector/edit/'.$collector->id.'\'><span class="fa fa-pencil"></span></a>
                <a class="btn action-btn" href=\'/collector/premiums/'.$collector->id.'\'><span class="fa fa-money"></span></a>
                <a class="btn action-btn" onclick=\'deleteCollector('.$collector->id.')\'><span class="fa fa-trash-o"></span></a>';
                ;
            })
            ->rawColumns(['action', 'image'])
            ->make(true);
    }

    // return edit view
    public function editForm($id){
        $collector = User::find($id);
        $cities = City::get();
        return view('collectors.edit',compact('collector','cities'));
    }

    // store edit
    public function storeEdit(Request $request){

        $rules = [
            'name' => 'required|string',
            'email' => 'required|string',
            'mobile' => 'required|string',
            'address' => 'required|string',
            'national_id' => 'required|string',
            'total_collector_point' => 'required|numeric',
            'city_id' => 'required|string',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ];

        $this->validate($request,$rules);

        if($request->has('status')){
            $request->merge(['status' => 1]);
        }else{
            $request->merge(['status' => 0]);
        }
        // upload image if exist
        if($request->hasFile('image')){
            $image      = $request->file('image');
            $fileName   = time() . '.' . $image->getClientOriginalExtension();
            $path = 'images/collectors/'.$fileName;

            $img = Image::make($image->getRealPath());
            $img->resize(100, 100, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->stream(); // <-- Key point
            \Storage::disk('public')->put('/images/collectors'.'/'.$fileName, $img);
        }else{
            $user = User::find($request->id);
            $path = $user->image;
        }
        if(!is_null($request->password)){
            User::where('id',$request->id)->update(['password'=>bcrypt($request->password)]);
        }
        User::where('id',$request->id)->update($request->except(['id', '_token','password']));
        User::where('id',$request->id)->update(['image'=>$path]);


        \Session::flash('success', 'Collector data is updated successfully');
        return redirect()->route('collector.index');

    }

    // return add view
    public function addForm(){
        $cities = City::get();
        return view('collectors.add',compact('cities'));
    }

    // store new collector
    public function storeAdding(Request $request){
        $rules = [
            'name' => 'required|string',
            'email' => 'required|string|email|unique:users',
            'password' => 'required|string|min:6',
            'mobile' => 'required|string',
            'address' => 'required|string',
            'national_id' => 'required|string',
            'city_id' => 'required|numeric',
        ];

        $this->validate($request,$rules);

        $new_collector= new User();
        $new_collector->name = $request->name;
        $new_collector->email = $request->email;
        $new_collector->password = bcrypt($request->password);
        $new_collector->phone = $request->phone;
        $new_collector->mobile = $request->mobile;
        $new_collector->whatsapp_number = $request->whatsapp_number;
        $new_collector->address = $request->address;
        $new_collector->city_id = $request->city_id;
        $new_collector->card_code = $request->card_code;
        $new_collector->national_id = $request->national_id;
        $new_collector->status = 1;
        $new_collector->activity = "Collecting";
        $new_collector->role = 'Collector';
        $new_collector->image = 'user.jpg';
        $new_collector->save();

        \Session::flash('success', 'Collector is created successfully');
        return redirect()->route('collector.index');
    }

    // delete collector
    public function delete(Request $request){
        User::where('id',$request->id)->delete();
        \Session::flash('success', 'Collector data is updated successfully');
        return redirect()->route('collector.index');
    }

    // return Premiums
    public function premiums($id){
        $collector = User::find($id);
        $total= Premiums::where('collector_id',$id)->where('submitted_value_status',0)->where('status',1)->sum('paid_amount');

        return view('collectors.premiums-list',compact('collector','total'));
    }
    // fetch premiums
    public function fetchPremiums(Request $request){
        $premiums = Premiums::where('collector_id',$request->id)->where('submitted_value_status',0)->get();

        return DataTables::of($premiums)
            ->addColumn('paid_amount', function ($premium) {
                return $premium->paid_amount;
            })
            ->addColumn('code', function ($premium) {
                return $premium->code;
            })
            ->addColumn('collect_date', function ($premium) {
                return $premium->collect_date;
            })
            ->make(true);
    }

}
