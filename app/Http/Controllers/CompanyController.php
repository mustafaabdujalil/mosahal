<?php

namespace App\Http\Controllers;

use App\City;
use App\Client;
use App\Company;
use App\CompanySupplier;
use App\Premiums;
use App\Purchase;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\DataTables;
use Image;

class CompanyController extends Controller
{
    // return companies view
    public function index(){
        return view('companies.index');
    }

    // fetch companies for datatable
    public function fetchCompanies()
    {
        $companies = Company::get();

        return DataTables::of($companies)
            ->addColumn('name', function ($company) {
                return $company->name;
            })
            ->addColumn('description', function ($company) {
                return $company->description;
            })
            ->addColumn('manager_name', function ($company) {
                return $company->manager_name;
            })
            ->addColumn('mobile', function ($company) {
                return $company->mobile;
            })
            ->addColumn('phone', function ($company) {
                return $company->phone;
            })
            ->addColumn('whatsapp', function ($company) {
                return $company->whatsapp_number;
            })
            ->addColumn('address', function ($company) {
                return $company->address;
            })
            ->addColumn('city', function ($company) {
                return $company->city->name;
            })
            ->addColumn('status', function ($company) {
                if($company->status == 0){
                    return 'Not Active';
                }else{
                    return 'Active';
                }
            })
            ->addColumn('logo', function ($company) {
                if($company->logo){
                    return '<img src="'.$company->logo.'" style="width:30px;">';
                }else{
                    return '<img src="https://placeimg.com/30/30/nature" style="width:30px;">';
                }
            })
            ->addColumn('premiums_months', function ($company) {
                return $company->premiums_months;
            })
            ->addColumn('max_value', function ($company) {
                return $company->max_value;
            })
            ->addColumn('activity', function ($company) {
                return $company->activity;
            })
            ->addColumn('action', function ($company) {

                return '
                <a class="btn action-btn" href=\'/company/edit/'.$company->id.'\'><span class="fa fa-pencil"></span></a>
                <a class="btn action-btn" href=\'/company/suppliers/'.$company->id.'\'><span class="fa fa-users"></span></a>
                <a class="btn action-btn" onclick=\'deleteCompany('.$company->id.')\'><span class="fa fa-trash-o"></span></a>';
                ;
            })
            ->rawColumns(['action', 'logo'])
            ->make(true);
    }

    // return add view
    public function addForm(){
        $cities = City::get();
        $suppliers = User::where('role','Supplier')->get();
        return view('companies.add',compact('cities','suppliers'));
    }

    // store new company
    public function storeAdding(Request $request){

        $rules = [
            'name' => 'required|string',
            'description' => 'required|string',
            'manager_name' => 'required|string',
            'mobile' => 'required|string',
            'phone' => 'required|string',
            'address' => 'required|string',
            'city_id' => 'required|numeric',
            'activity' => 'required|string',
            'suppliers' => 'required|array',
            'premiums_months' => 'required|numeric',
            'max_value' => 'required|numeric',
            'logo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ];

        $this->validate($request,$rules);


        if($request->hasFile('logo')){
            $image      = $request->file('logo');
            $fileName   = time() . '.' . $image->getClientOriginalExtension();
            $path = 'images/logos/'.$fileName;

            $img = Image::make($image->getRealPath());
            $img->resize(100, 100, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->stream(); // <-- Key point
            \Storage::disk('public')->put('/images/logos'.'/'.$fileName, $img);
        }else{
            $path = "user.jpg";
        }

        // create new company
        $new_company = new Company();
        $new_company->name = $request->name;
        $new_company->description = $request->description;
        $new_company->manager_name = $request->manager_name;
        $new_company->mobile = $request->mobile;
        $new_company->phone = $request->phone;
        $new_company->whatsapp_number = $request->whatsapp_number;
        $new_company->address = $request->address;
        $new_company->city_id = $request->city_id;
        $new_company->status = 1;
        $new_company->activity = $request->activity;
        $new_company->logo = $path;
        $new_company->premiums_months = $request->premiums_months;
        $new_company->max_value = $request->max_value;
        $new_company->save();

        // create suppliers for the company
        foreach ($request->suppliers as $supplier){
            CompanySupplier::create([
               'company_id' => $new_company->id,
               'supplier_id' => $supplier
            ]);
        }

        \Session::flash('success', 'Company is created successfully');
        return redirect()->route('company.index');
    }

    // return edit view
    public function editForm($id){
        $company = Company::find($id);
        $cities = City::get();
        $suppliers = User::where('role','Supplier')->get();
        $company_suppliers = CompanySupplier::where('company_id', $company->id)
                                            ->pluck('supplier_id')->toArray();

                                            return view('companies.edit',compact('company','cities','suppliers','company_suppliers'));
    }

    // store edit
    public function storeEdit(Request $request){

        $rules = [
            'name' => 'required|string',
            'description' => 'required|string',
            'manager_name' => 'required|string',
            'mobile' => 'required|string',
            'phone' => 'required|string',
            'address' => 'required|string',
            'city_id' => 'required|numeric',
            'activity' => 'required|string',
            'premiums_months' => 'required|numeric',
            'suppliers' => 'required|array',
            'max_value' => 'required|numeric',
            'logo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ];

        $this->validate($request,$rules);

        $company = Company::find($request->id);

        if($request->has('status')){
            $request->merge(['status' => 1]);
        }else{
            $request->merge(['status' => 0]);
        }
        // upload image if exist
        if($request->hasFile('logo')){
            $image      = $request->file('logo');
            $fileName   = time() . '.' . $image->getClientOriginalExtension();
            $path = 'images/logos/'.$fileName;

            $img = Image::make($image->getRealPath());
            $img->resize(100, 100, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->stream(); // <-- Key point
            \Storage::disk('public')->put('/images/logos'.'/'.$fileName, $img);

        }else{
            $path = $company->logo;
        }

        // create new company
        $company->name = $request->name;
        $company->description = $request->description;
        $company->manager_name = $request->manager_name;
        $company->mobile = $request->mobile;
        $company->phone = $request->phone;
        $company->whatsapp_number = $request->whatsapp_number;
        $company->address = $request->address;
        $company->city_id = $request->city_id;
        $company->status = 1;
        $company->activity = $request->activity;
        $company->logo = $path;
        $company->premiums_months = $request->premiums_months;
        $company->max_value = $request->max_value;
        $company->status = $request->status;
        $company->update();

        CompanySupplier::where('company_id',$company->id)->delete();
        // create suppliers for the company
        foreach ($request->suppliers as $supplier){
            CompanySupplier::create([
               'company_id' => $company->id,
               'supplier_id' => $supplier
            ]);
        }

        \Session::flash('success', 'Company data is updated successfully');
        return redirect()->route('company.index');

    }

    // delete company
    public function delete(Request $request){

        $company_clients = Client::where('company_id',$request->id)->pluck('id');
        $clients_purchase = Purchase::whereIn('client_id',$company_clients)->pluck('id');
        $check_clients_purchase = Premiums::whereIn('purchase_id',$clients_purchase)
            ->where('submitted_value_status',0)->whereIn('status',[0,2])->get();

        if(count($check_clients_purchase) > 0){
            \Session::flash('success', 'You can not delete company because there is client from this company has premiums');
        }else{
            $company_clients = Client::where('company_id',$request->id)->pluck('id');
            $clients_purchase = Purchase::whereIn('client_id',$company_clients)->pluck('id');
            Premiums::whereIn('purchase_id',$clients_purchase)->where('submitted_value_status',0)->delete();
            Purchase::whereIn('id',$clients_purchase)->delete();
            Client::whereIn('id',$company_clients)->delete();
            Company::where('id',$request->id)->delete();
            \Session::flash('success', 'Admin data is updated successfully');
        }

        return redirect()->route('company.index');
    }

    // return suppliers view
    public function suppliers($id){
        return view('companies.suppliers.index',compact('id'));
    }

    // fetch company suppliers
    public function fetchCompanySuppliers(Request $request)
    {
        $suppliers = CompanySupplier::where('company_id',$request->id)->get();

        return DataTables::of($suppliers)
            ->addColumn('name', function ($supplier) {
                return $supplier->supplier->name;
            })
            ->addColumn('mobile', function ($supplier) {
                return $supplier->supplier->mobile;
            })

            ->addColumn('address', function ($supplier) {
                return $supplier->supplier->address;
            })
            ->addColumn('image', function ($supplier) {
                if($supplier->supplier->image){
                    return '<img src="'.$supplier->supplier->image.'" style="width:30px;">';
                }else{
                    return '<img src="https://placeimg.com/30/30/nature" style="width:30px;">';
                }
            })
            ->addColumn('action', function ($supplier) {

                return '
                <a class="btn action-btn" onclick=\'deleteSupplier('.$supplier->id.')\'><span class="fa fa-trash-o"></span></a>';
                ;
            })
            ->rawColumns(['action', 'image'])
            ->make(true);
    }

    // return add supplier form
    public function addSupplierForm(Request $request){
        $suppliers = User::where('role','Supplier')->get();
        $company = Company::find($request->id);
        if(!$company){
            abort(404);
        }
        $exist_suppliers = CompanySupplier::where('company_id',$request->id)->pluck('supplier_id')->toArray();
        return view('companies.suppliers.add',compact('suppliers','company','exist_suppliers'));
    }

    // store supplier
    public function storeSupplier(Request $request){
        $rules = [
            'company_id' => 'required|numeric',
            'suppliers' => 'required|array',
        ];

        $this->validate($request,$rules);

        // create suppliers for the company
        foreach ($request->suppliers as $supplier){
            $exist = CompanySupplier::where('company_id',$request->company_id)->where('supplier_id',$supplier)->get();
            if(count($exist) == 0){
                CompanySupplier::create([
                    'company_id' => $request->company_id,
                    'supplier_id' => $supplier
                ]);
            }

        }

        \Session::flash('success', 'Supplier is assigned successfully to the company');
        return redirect()->route('company.index');
    }

    // delete supplier
    public function deleteSupplier(Request $request){
        CompanySupplier::where('id',$request->id)->delete();
        return response()->json(['status'=>'success','msg'=>'تم حذف المورد بنجاح']);
    }

}
