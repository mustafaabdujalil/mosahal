<?php

namespace App\Http\Controllers;

use App\City;
use App\Setting;
use App\User;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class SettingController extends Controller
{
    // return collectors view
    public function index(){
        return view('settings.index');
    }

    // fetch suppliers for datatable
    public function fetchSetting()
    {
        $settings = Setting::get();

        return DataTables::of($settings)
            ->addColumn('subscription_value', function ($setting) {
                return $setting->subscription_value;
            })
            ->addColumn('action', function ($setting) {

                return '<a class="btn action-btn" href=\'/settings/edit\'><span class="fa fa-pencil"></span></a>';             ;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    // return edit view
    public function editForm(){
        $setting = Setting::first();
        return view('settings.edit',compact('setting'));
    }

    // store edit
    public function storeEdit(Request $request){

        $rules = [
            'subscription_value' => 'required',
        ];

        $this->validate($request,$rules);

        Setting::where('id',$request->id)->update(['subscription_value'=>$request->subscription_value]);

        \Session::flash('success', 'Setting data is updated successfully');
        return redirect()->route('settings.index');

    }
}
