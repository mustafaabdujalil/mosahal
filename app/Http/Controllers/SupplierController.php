<?php

namespace App\Http\Controllers;

use App\City;
use App\SupplierBranch;
use App\SupplierDept;
use App\SupplierRole;
use App\User;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use DB;
use Auth;
use Image;

class SupplierController extends Controller
{
    // return suppliers view
    public function index(){
        return view('suppliers.index');
    }

    // fetch suppliers for datatable
    public function fetchSuppliers()
    {
        $suppliers = User::where('role', 'Supplier')->get();

        return DataTables::of($suppliers)
            ->addColumn('name', function ($supplier) {
                return $supplier->name;
            })
            ->addColumn('email', function ($supplier) {
                return $supplier->email;
            })
            ->addColumn('card_code', function ($supplier) {
                return $supplier->card_code;
            })
            ->addColumn('mobile', function ($supplier) {
                return $supplier->mobile;
            })
            ->addColumn('phone', function ($supplier) {
                return $supplier->phone;
            })
            ->addColumn('whatsapp', function ($supplier) {
                return $supplier->whatsapp_number;
            })
            ->addColumn('discount_value', function ($supplier) {
                return $supplier->discount_value;
            })
            ->addColumn('address', function ($supplier) {
                return $supplier->address;
            })
            ->addColumn('city', function ($supplier) {
                return $supplier->city->name;
            })
            ->addColumn('collect_time', function ($supplier) {
                return $supplier->collect_time;
            })
            ->addColumn('premiums_months', function ($supplier) {
                return $supplier->premiums_months;
            })
            ->addColumn('debt_value', function ($supplier) {
                $sum = SupplierDept::where('supplier_id',$supplier->id)->sum('debt_value');
                return $sum;
            })
            ->addColumn('status', function ($supplier) {
                if($supplier->status == 0){
                    return 'Not Active';
                }else{
                    return 'Active';
                }
            })
            ->addColumn('image', function ($supplier) {
                if($supplier->image){
                    return '<img src="'.$supplier->image.'" style="width:30px;">';
                }else{
                    return '<img src="https://placeimg.com/30/30/nature" style="width:30px;">';
                }
            })
            ->addColumn('action', function ($supplier) {

                return '
                <a class="btn action-btn" href=\'/supplier/edit/'.$supplier->id.'\'><span class="fa fa-pencil"></span></a>
                <a class="btn action-btn" href=\'/supplier/branches/'.$supplier->id.'\'><span class="fa fa-braille"></span></a>
                <a class="btn action-btn" href=\'/supplier/employees/'.$supplier->id.'\'><span class="fa fa-users"></span></a>
                <a class="btn action-btn" onclick=\'deleteSupplier('.$supplier->id.')\'><span class="fa fa-trash-o"></span></a>
                <a class="btn action-btn" onclick=\'changeDebtValue('.$supplier->id.')\'><span class="fa fa-plus"></span></a>
                ';

            })
            ->rawColumns(['action', 'image'])
            ->make(true);
    }

    // return edit view
    public function editForm($id){
        $supplier = User::find($id);
        $cities = City::get();
        return view('suppliers.edit',compact('supplier','cities'));
    }

    // store edit
    public function storeEdit(Request $request){

        $rules = [
            'name' => 'required|string',
            'email' => 'required|string',
            'mobile' => 'required|string',
            'discount_value' => 'required',
            'address' => 'required|string',
            'city_id' => 'required|string',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ];

        $this->validate($request,$rules);

        if($request->has('status')){
            $request->merge(['status' => 1]);
        }else{
            $request->merge(['status' => 0]);
        }
        // upload image if exist
        if($request->hasFile('image')){
            $image      = $request->file('image');
            $fileName   = time() . '.' . $image->getClientOriginalExtension();
            $path = 'images/suppliers/'.$fileName;

            $img = Image::make($image->getRealPath());
            $img->resize(100, 100, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->stream(); // <-- Key point
            \Storage::disk('public')->put('/images/suppliers'.'/'.$fileName, $img);
        }else{
            $user = User::find($request->id);
            $path = $user->image;
        }
        if(!is_null($request->password)){
            User::where('id',$request->id)->update(['password'=>bcrypt($request->password)]);
        }
        User::where('id',$request->id)->update($request->except(['id', '_token','token','password']));
        User::where('id',$request->id)->update(['image'=>$path]);


        \Session::flash('success', 'Supplier data is updated successfully');
        return redirect()->route('supplier.index');

    }

    // return add view
    public function addForm(){
        $cities = City::get();
        return view('suppliers.add',compact('cities'));
    }

    // store new supplier
    public function storeAdding(Request $request){
        $rules = [
            'name' => 'required|string',
            'email' => 'required|string|email|unique:users',
            'password' => 'required|min:6',
            'mobile' => 'required|string',
            'address' => 'required|string',
            'city_id' => 'required|numeric',
            'activity' => 'required|string',
            'collect_time' => 'required|numeric',
            'premiums_months' => 'required|numeric',
            'discount_value' => 'required',
        ];

        $this->validate($request,$rules);

        $new_supplier = new User();
        $new_supplier->name = $request->name;
        $new_supplier->email = $request->email;
        $new_supplier->password = bcrypt($request->password);
        $new_supplier->phone = $request->phone;
        $new_supplier->mobile = $request->mobile;
        $new_supplier->whatsapp_number = $request->whatsapp_number;
        $new_supplier->discount_value = $request->discount_value;
        $new_supplier->address = $request->address;
        $new_supplier->city_id = $request->city_id;
        $new_supplier->status = 1;
        $new_supplier->activity = $request->activity;
        $new_supplier->collect_time = $request->collect_time;
        $new_supplier->premiums_months = $request->premiums_months;
        $new_supplier->role = 'Supplier';
        $new_supplier->image = 'user.jpg';
        $new_supplier->save();

        \Session::flash('success', 'Supplier is created successfully');
        return redirect()->route('supplier.index');
    }

    // delete supplier
    public function delete(Request $request){

        $branches_ids = SupplierBranch::where('supplier_id',$request->id)->pluck('id')->toArray();
        SupplierBranch::where('supplier_id',$request->id)->delete();
        User::where('id',$request->id)->orWhereIn('supplier_employee_branch_id',$branches_ids)->delete();
        \Session::flash('success', 'Supplier and his branches are deleted successfully');
        return redirect()->route('supplier.index');
    }

    // return branches view
    public function branches($id){
        if(Auth::user()->role != "Admin"){
            if(Auth::user()->id != $id) {
                \Session::flash('error', 'You do not have permission for that');
                return redirect('supplier');
            }
        }
        $supplier = User::find($id);
        return view('suppliers.branches.index',compact('supplier'));
    }

    // store new branch
    public function storeAddingBranch(Request $request){
        $rules = [
            'name' => 'required|string',
        ];

        $this->validate($request,$rules);

        $new_branch = new SupplierBranch();
        $new_branch->branch_name = $request->name;
        $new_branch->supplier_id = $request->supplier_id;
        $new_branch->save();

        \Session::flash('success', 'Branch is created successfully');
        return redirect('supplier/branches/'.$request->supplier_id);
    }
    // fetch suppliers for datatable
    public function fetchBranches(Request $request)
    {
        $branches = SupplierBranch::where('supplier_id', $request->id)->get();

        return DataTables::of($branches)
            ->addColumn('name', function ($branch) {
                return $branch->branch_name;
            })
            ->addColumn('action', function ($branch) {

                return '<a class="btn action-btn" onclick=\'deleteBranch('.$branch->id.')\'><span class="fa fa-trash-o"></span></a>';
                ;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    // add branch form
    public function addBranchForm($id){
        $supplier = User::find($id);
        return view('suppliers.branches.add',compact('supplier'));
    }

    // delete branch
    public function deleteBranch(Request $request){
        $supplier_id = SupplierBranch::where('id',$request->id)->value('supplier_id');
        SupplierBranch::where('id',$request->id)->delete();
        \Session::flash('success', 'Branch is deleted successfully');
        return redirect('supplier/branches/'.$supplier_id);
    }

    // return list of employees
    public function employees($supplier_id){

        if(Auth::user()->role != "Admin"){
            if(Auth::user()->id != $supplier_id){
                \Session::flash('error', 'You do not have permission for that');
                return redirect('supplier');
            }
        }

        return view('suppliers.employees.index',compact('supplier_id'));
    }

    // fetch employees of supplier for datatable
    public function fetchEmployees(Request $request)
    {
        $supplier_branches = SupplierBranch::where('supplier_id',$request->supplier_id)->pluck('id');
        $employees = User::whereIn('supplier_employee_branch_id',$supplier_branches)->get();

        return DataTables::of($employees)
            ->addColumn('name', function ($employee) {
                return $employee->name;
            })
            ->addColumn('email', function ($employee) {
                return $employee->email;
            })
            ->addColumn('branch_name', function ($employee) {
                return $employee->branch->branch_name;
            })
            ->addColumn('roles', function ($employee) {
                if(isset($employee->employeeRoles)){
                    $roles='';
                    foreach ($employee->employeeRoles as $role){
                        if($role->role == 0){
                            $roles .= 'All';
                        }
                        if($role->role == 1){
                            $roles .= ' Seller';
                        }
                        if($role->role == 2){
                            $roles .= ' Collector';
                        }
                        if($role->role == 3){
                            $roles .= ' Relay';
                        }
                    }
                    return $roles;
                }
                return '-';
            })
            ->addColumn('action', function ($employee) {

                return '<a class="btn action-btn" onclick=\'deleteEmployee('.$employee->id.')\'><span class="fa fa-trash-o"></span></a>';
                ;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    // return add employee form
    public function addEmployeeForm($id){
        $supplier = User::find($id);
        $branches = SupplierBranch::where('supplier_id',$supplier->id)->get();
        return view('suppliers.employees.add',compact('supplier','branches'));
    }

    // store new employee
    public function storeAddingEmployee(Request $request){
        $rules = [
            'name' => 'required|string',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:6',
            'branch_id' => 'required',
            'supplier_id' => 'required|numeric|exists:users,id'
        ];

        $this->validate($request,$rules);

        $supplier = User::find($request->supplier_id);
        $supplier_branch = SupplierBranch::where('id',$request->branch_id)->first();

        // Start transaction!
        DB::beginTransaction();

        try {

            // store new supplier
            $new_supplier_employee = new user();
            $new_supplier_employee->name = $request->name;
            $new_supplier_employee->email = $request->email;
            $new_supplier_employee->password = bcrypt($request->password);
            $new_supplier_employee->role = 'SupplierEmployee';
            $new_supplier_employee->mobile = $supplier->mobile;
            $new_supplier_employee->address = $supplier_branch->branch_name;
            $new_supplier_employee->city_id = $supplier->city_id;
            $new_supplier_employee->activity = $supplier->activity;
            $new_supplier_employee->image = 'user.jpg';
            $new_supplier_employee->supplier_employee_branch_id = $request->branch_id;
            $new_supplier_employee->save();

            // store supplier roles
            if(in_array(0,$request->roles)){
                $new_role = new SupplierRole();
                $new_role->supplier_id = $new_supplier_employee->id;
                $new_role->role = 0;// 0:all,1:sell,2:collect,3:relay
                $new_role->save();
            }else{
                foreach ($request->roles as $role){
                    $new_role = new SupplierRole();
                    $new_role->supplier_id = $new_supplier_employee->id;
                    $new_role->role = $role;// 0:all,1:sell,2:collect,3:relay
                    $new_role->save();
                }
            }

        }catch(\Exception $e){
            DB::rollback();
            \Session::flash('error', 'Something is wrong please try again');
            return redirect()->back();
        }

        DB::commit();

        \Session::flash('success', 'Employee is created successfully');
        return redirect('/supplier/employees/'.$request->supplier_id);
    }

    // delete employee
    public function deleteEmployee(Request $request){
        // Start transaction!
        DB::beginTransaction();

        try {

            SupplierRole::where('supplier_id',$request->id)->delete();
            User::where('id',$request->id)->delete();

        }catch(\Exception $e){
            DB::rollback();
            return response()->json(['status'=>'error','msg'=>'حدث خطا برجاء المحالوةمره اخري']);
        }

        DB::commit();

        return response()->json(['status'=>'success','msg'=>'تم حذف الموظف بنجاح']);
    }

}
