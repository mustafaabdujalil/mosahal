<?php

namespace App\Http\Controllers\API;

use App\Client;
use App\ClientCollector;
use App\Company;
use App\Premiums;
use App\Purchase;
use App\PurchasePremium;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CollectorController extends Controller
{

    // return suppliers list
    public function clientPremiums(Request $request){

        if($request->type == 0){
            $users = User::where('national_id','like','%'.$request->value.'%')->orWhere('card_code','like','%'.$request->value.'%')
                ->orWhere('name','like','%'.$request->value.'%')->get(['id','national_id','card_code','name']);
            return response()->json(['status'=> "success",'msg'=>["Client premiums"],'data' => $users],200);
        }

        $users_ids = User::where('national_id',$request->value)->orWhere('card_code',$request->value)
                        ->orWhere('name',$request->value)->pluck('id');
        $clients_ids = Client::whereIn('user_id',$users_ids)->pluck('id');
        $purchases_ids = Purchase::whereIn('client_id',$clients_ids)->pluck('id');
        $premiums_ids = PurchasePremium::whereIn('purchase_id',$purchases_ids)->pluck('premium_id');
        $premiums = Premiums::whereIn('id',$premiums_ids)->where('premium_month_number','>',0)->get();

        foreach ($premiums as $premium){
            $purchase_premium = PurchasePremium::where('premium_id',$premium->id)->first();
            $purchase = Purchase::with('client')->where('id',$purchase_premium->purchase_id)->first();
            $premium->client_name = $purchase->client->name;
            $premium->client_address = $purchase->client->address;
            $premium->client_phone = $purchase->client->mobile;
            $premium->max_value = $purchase->client->max_value;
            $premium->fixed_max_value = $purchase->client->fixed_max_value;
        }
        return response()->json(['status'=> "success",'msg'=>["Client premiums"],'data' => $premiums],200);
    }

    // return suppliers list
    public function dailyPremiums(Request $request){
        $user = $request->user();

        if($request->company_id > 0){
            $clients_ids = ClientCollector::where('collector_id',$user->id)->pluck('client_id');
            $clients_ids = Client::whereIn('id',$clients_ids)->where('company_id',$request->company_id)
                                ->pluck('id');
            $purchases_ids = Purchase::whereIn('client_id',$clients_ids)->pluck('id');
            $premiums_ids = PurchasePremium::whereIn('purchase_id',$purchases_ids)->pluck('premium_id');
        }else{
            $clients_ids = ClientCollector::where('collector_id',$user->id)->pluck('client_id');
            $purchases_ids = Purchase::whereIn('client_id',$clients_ids)->pluck('id');
            $premiums_ids = PurchasePremium::whereIn('purchase_id',$purchases_ids)->pluck('premium_id');
        }

        if($request->has('from') && $request->has('to')){
            $from = $request->from;
            $to = $request->to;
            $premiums = Premiums::whereIn('id',$premiums_ids)

                                ->where(function ($query) use ($from,$to){
                                    $query->where('collect_date','>=',$from)
                                        ->where(function ($query) use ($from,$to){
                                            $query->where('collect_date','<=',$to);
                                        });
                                })

                                ->where('status',0)->where('premium_month_number','>',0)->get();
        }else{
            $premiums = Premiums::whereIn('id',$premiums_ids)->whereDate('collect_date',date('Y-m-d'))
                ->where('status',0)->where('premium_month_number','>',0)->get();
        }

        foreach ($premiums as $premium){
            $purchase_premium = PurchasePremium::where('premium_id',$premium->id)->first();
            $purchase = Purchase::with('client')->where('id',$purchase_premium->purchase_id)->first();
            $premium->client_name = $purchase->client->name;
            $premium->client_address = $purchase->client->address;
            $premium->client_phone = $purchase->client->mobile;
            $premium->max_value = $purchase->client->max_value;
            $premium->fixed_max_value = $purchase->client->fixed_max_value;
        }
        return response()->json(['status'=> "success",'msg'=>["Collector daily premiums"],'data' => $premiums],200);
    }

    // return companies list
    public function companiesList(Request $request){

        $companies = Company::where('status',1)->get();

        return response()->json(['status'=> "success",'msg'=>["Companies list "],'data' => $companies],200);



    }
}
