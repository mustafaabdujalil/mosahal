<?php

namespace App\Http\Controllers\API;

use App\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\User;
use Validator;

class AuthController extends Controller
{
    /**
     * login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request){

        $validator = Validator::make($request->all(), [
            'value' => 'required|string',
            'type'=>'required',
            'password' => 'required|string',
        ]);

        if($validator->fails()){
            $errors = [];
            $error = $validator->messages();
            foreach($error->all() as $value){
                $errors [] = $value;
            }
            return response()->json(['status'=> 'error','msg'=>$errors,'data' => new \stdClass()],402);
        }
        if($request->type == "card_code"){
            if(!Auth::attempt(['card_code' => request('value'), 'password' => request('password')])){
                return response()->json(['status'=>'error','msg'=>['Code card or password is incorrect']], 401);
            }
        }elseif ($request->type == "national_id"){
            if(!Auth::attempt(['national_id' => $request->value, 'password' => request('password')])){
                return response()->json(['status'=>'error','msg'=>['National Id or password is incorrect']], 401);
            }
        }else{
            return response()->json(['status'=>'error','msg'=>['There is something wrong , please try again']], 403);
        }

        $user = User::with('client:id,user_id,fixed_max_value,max_value','city')->where('id',Auth::user()->id)->first();
        User::where('id',$request->user()->id)->update([
            'fcmToken' => $request->fcmToken
        ]);

        $success['token'] =  $user->createToken('MyApp')-> accessToken;
        $data = new \stdClass();
        $data->access_token = $success['token'];
        $data->user = $user;

        return response()->json(['status' => "success",'msg'=>['User logged successfully'],'data'=>$data],200 );

    }

    /**
     * Logout user (Revoke the token)
     *
     * @return [string] status
     * @return [string] msg
     */
    public function logout(Request $request){
        $request->user()->token()->revoke();

        return response()->json(['status'=> "success",'msg'=>["User is logged out Successfully"],'data' => new \stdClass()],200);
    }

    // check expiration token
    public function validateAccessToken(Request $request){
        return response()->json([
            'status' => "error",
            'msg' => array ("messages" => ['Login has been expired']),
            'data' => new \stdClass()
        ],401);
    }

    // return token
    public function token(Request $request){
        $validator = Validator::make($request->all(), [
            'type' => 'required|string',
            'value' => 'required|string',
        ]);

        if($validator->fails()){
            $errors = [];
            $error = $validator->messages();
            foreach($error->all() as $value){
                $errors [] = $value;
            }
            return response()->json(['status'=> 'error','msg'=>$errors,'data' => new \stdClass()],402);
        }

        switch ($request->type) {
            case "name":
                $user = User::where('name',$request->value)->first();
                break;
            case "national_id":
                $user = User::where('national_id',$request->value)->first();
                break;
            default:
                $client = Client::where('code',$request->value)->first();
                $user = User::where('id',$client->user_id)->first();
        }

        $success['token'] =  $user->createToken('MyApp')-> accessToken;
        $data = new \stdClass();
        $data->access_token = $success['token'];
//        $data->user = $user;

        return response()->json(['status' => "success",'msg'=>['User token is returned successfully'],'data'=>$data],200 );
    }


    // add device token for user
    public function setDeviceToken(Request $request){
        User::where('id',$request->user()->id)->update([
            'fcmToken' => $request->fcmToken
        ]);

        return response()->json(['status' => "success",'msg'=>['User device token is updated successfully'],
            'data'=>new \stdClass()],200 );

    }
}
