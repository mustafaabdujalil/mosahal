<?php

namespace App\Http\Controllers\API;

use App\Client;
use App\CollectivePremiums;
use App\Company;
use App\CompanySupplier;
use App\Premiums;
use App\Purchase;
use App\PurchasePremium;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Validator;
use Auth;

class ClientController extends Controller
{
    // return client report
    public function report(Request $request){

        $client = Client::with('company:id,name','city','purchases','purchases.products')
            ->where('user_id',$request->user()->id)->first();

        if($request->user()->role == "Collector"){
            $purchases_ids = Purchase::where('client_id',$client->id)->pluck('id');
            $premiums_ids = PurchasePremium::whereIn('purchase_id',$purchases_ids)->pluck('premium_id')->toArray();
            $premiums_ids = array_unique($premiums_ids);
            $premiums = Premiums::whereIn('id',$premiums_ids)
                ->where('collect_date','<=',Carbon::now())
                ->where('premium_month_number','>',0)
                ->get();
        }else{
            $purchases_ids = Purchase::where('client_id',$client->id)->pluck('id');
            $premiums_ids = PurchasePremium::whereIn('purchase_id',$purchases_ids)->pluck('premium_id')->toArray();
            $premiums = Premiums::whereIn('id',$premiums_ids)->where('premium_month_number','>',0)->get();
        }

        foreach ($premiums as $premium){
            $premium->pdf = url('/purchases/premiums/download-bill-pay/'.$premium->id);
        }

        $total_amount = Purchase::where('client_id',$client->id)->sum('total_price');
        $data = [];
        $data['client'] = $client;
        $data['premiums'] = $premiums;
        $data['total_price'] = $total_amount;

        return response()->json(['status'=> "success",'msg'=>["Client report is returned Successfully"],'data' => $data],200);
    }

    // pay premium
    public function premiumsPay(Request $request){
        $validator = Validator::make($request->all(), [
            'id' => 'required|numeric|exists:premiums,id',
            'paid_amount' => 'required|numeric|min:1',
        ]);

        if($validator->fails()){
            $errors = [];
            $error = $validator->messages();
            foreach($error->all() as $value){
                $errors [] = $value;
            }
            return response()->json(['status'=> 'error','msg'=>$errors,'data' => new \stdClass()],402);
        }

        $premium = Premiums::find($request->id);
        $paid_amount = $request->paid_amount + $premium->paid_amount;//20+50=70
        $remaining_amount = $premium->remaining_amount - $request->paid_amount;//40-200=20
        if($paid_amount < 0 || $remaining_amount < 0){
            return response()->json(['status'=>'error','msg'=>['Invalid paid value'],'data' => new \stdClass()]);
        }

        Premiums::where('id',$request->id)->update([
            'status' => 1,
            'paid_amount' => $paid_amount,
            'remaining_amount' => $remaining_amount,
            'collector_id' => $request->user()->id,
            'collect_date' => date('Y-m-d')
        ]);



        $data = new \stdClass();
        $data->url = url('purchases/premiums/download-bill-pay/'.$request->id);

        return response()->json(['status'=>'success','msg'=>['Your Payment has been processed successfully '],'data'=>$data]);

    }

    public function stores(Request $request){
        $user = $request->user();
        $client = Client::where('user_id',$user->id)->first();
        $stores_ids = CompanySupplier::where('company_id',$client->company_id)->pluck('supplier_id');
        $stores = User::whereIn('id',$stores_ids)->where('role','Supplier')->get();

        $data = [];
        $data['client'] = $client;
        $data['stores'] = $stores;

        return response()->json(['status'=> "success",'msg'=>["Stores list returned Successfully"],'data' => $data],200);
    }
}
