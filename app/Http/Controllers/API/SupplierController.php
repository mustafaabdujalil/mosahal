<?php

namespace App\Http\Controllers\API;

use App\CompanySupplier;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class SupplierController extends Controller
{
    // return suppliers list
    public function list(Request $request){
        $list = User::with('city')->where('role','Supplier')->get();
        return response()->json(['status'=> "success",'msg'=>["Suppliers list is returned Successfully"],'data' => $list],200);
    }

    //return list of suppliers of company
    public function companySuppliers(Request $request){
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|numeric',
        ]);

        if($validator->fails()){
            $errors = [];
            $error = $validator->messages();
            foreach($error->all() as $value){
                $errors [] = $value;
            }
            return response()->json(['status'=> 'error','msg'=>$errors,'data' => new \stdClass()],402);
        }
        $suppliers_ids = CompanySupplier::where('company_id',$request->company_id)->pluck('supplier_id');
        $list = User::with('city')->whereIn('id',$suppliers_ids)
                        ->where('role','Supplier')->get();
        return response()->json(['status'=> "success",'msg'=>["Company Suppliers list is returned Successfully"],'data' => $list],200);
    }
}
