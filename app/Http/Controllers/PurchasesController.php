<?php

namespace App\Http\Controllers;

use App\City;
use App\Client;
use App\ClientCollector;
use App\CollectivePremiums;
use App\Company;
use App\Premiums;
use App\Purchase;
use App\PurchasePremium;
use App\PurchaseProduct;
use App\Setting;
use App\SupplierBranch;
use App\SupplierRole;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Auth;
use DB;
use PDF;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;

class PurchasesController extends Controller
{
    // return Purchases view
    public function index(){
        $suppliers = User::whereIn('role',['Supplier','SupplierEmployee'])->get();
        $id = 0;

        return view('purchases.index',compact('suppliers','id'));
    }

    // return Purchases of specific supplier view
    public function supplierPurchases($id){
        $suppliers = User::whereIn('role',['Supplier','SupplierEmployee'])->get();
        return view('purchases.index',compact('suppliers','id'));
    }

    // fetch companies for datatable
    public function fetchPurchases($id)
    {
        if(Auth::user()->role == "Admin"){
            if((int) $id == 0){
                $purchases = Purchase::select('*')->get();
            }else{
                $purchases = Purchase::where('supplier_id',$id)->select('*')->get();
            }

        }elseif(Auth::user()->role == "Collector"){
            $client_ids = ClientCollector::where('collector_id',Auth::user()->id)->pluck('client_id');
            $purchases = Purchase::whereIn('client_id',$client_ids)->get();
        }else{
            if(Auth::user()->role == "Supplier"){
                $purchases = Purchase::where('supplier_id',Auth::user()->id)->select('*')->get();
            }else{
                $supplier_branch = SupplierBranch::where('id',Auth::user()->supplier_employee_branch_id)->first();
                $supplier_id = $supplier_branch->supplier_id;
                $purchases = Purchase::where('supplier_id',$supplier_id)->select('*')->get();
            }
        }

        return DataTables::of($purchases)

            ->addColumn('client_name', function ($purchase) {
                return $purchase->client->name;
            })
            ->addColumn('supplier_name', function ($purchase) {
                return $purchase->supplier->name;
            })
            ->addColumn('total_price', function ($purchase) {
                return $purchase->total_price;
            })
            ->addColumn('paid_amount', function ($purchase) {
                return $purchase->paid_amount;
            })
            ->addColumn('remaining_amount', function ($purchase) {
                return $purchase->remaining_amount;
            })
            ->addColumn('premiums_month', function ($purchase) {
                return $purchase->premiums_month;
            })
            ->addColumn('monthly_value', function ($purchase) {
                return $purchase->monthly_value;
            })
            ->addColumn('max_value', function ($purchase) {
                return $purchase->client->max_value;
            })
            ->addColumn('gender', function ($purchase) {
                return $purchase->client->gender;
            })
            ->addColumn('created_at', function ($purchase) {
                return $purchase->created_at;
            })
            ->addColumn('action', function ($purchase) {

                if(Auth::user()->role == "Collector"){
                    return '<a class="btn action-btn" href=\'/purchases/premiums/'.$purchase->id.'\' title="الاستحقاقات"><span class="fa fa-money"></span></a>';
                }

                if(Auth::user()->role == "SupplierEmployee"){
                    $roles = SupplierRole::where('supplier_id',Auth::user()->id)->pluck('role')->toArray();
                    $routes = '';
                    if(in_array(0,$roles)){

                        return '<a class="btn action-btn" href=\'/purchases/products/'.$purchase->id.'\' title="المنتجات"><span class="fa fa-database"></span></a>
                                <a class="btn action-btn" href=\'/purchases/recovery-products/'.$purchase->id.'\' title="المنتجات المسترجعه"><span class="fa fa-refresh"></span></a>
                                <a class="btn action-btn" href=\'/purchases/premiums/'.$purchase->id.'\' title="الاستحقاقات"><span class="fa fa-money"></span></a>';

                    }
                    if(in_array(2,$roles)){
                        $routes .='<a class="btn action-btn" href=\'/purchases/premiums/'.$purchase->id.'\' title="الاستحقاقات"><span class="fa fa-money"></span></a>';
                    }
                    if(in_array(3,$roles)){
                        $routes .= '<a class="btn action-btn" href=\'/purchases/products/'.$purchase->id.'\' title="المنتجات"><span class="fa fa-database"></span></a>
                                    <a class="btn action-btn" href=\'/purchases/recovery-products/'.$purchase->id.'\' title="المنتجات المسترجعه"><span class="fa fa-refresh"></span></a>';
                    }

                    return $routes;
                }

                $btn = '';
                if(Auth::user()->role == "Admin"){
                    $btn = '<a class="btn action-btn" onclick=\'deletePurchase('.$purchase->id.')\' title="حذف"><span class="fa fa-remove"></span></a>';
                }
                return '<a class="btn action-btn" href=\'/purchases/products/'.$purchase->id.'\' title="المنتجات"><span class="fa fa-database"></span></a>
                <a class="btn action-btn" href=\'/purchases/recovery-products/'.$purchase->id.'\' title="المنتجات المسترجعه"><span class="fa fa-refresh"></span></a>
                <a class="btn action-btn" href=\'/purchases/premiums/'.$purchase->id.'\' title="الاستحقاقات"><span class="fa fa-money"></span></a>'.$btn;
            })
            ->rawColumns(['action', 'logo'])
            ->make(true);
    }

    // return add view
    public function addForm($client_id = null){

        $active_companies_ids = Company::where('status',1)->pluck('id')->toArray();

        if(is_null($client_id)){
            $clients = Client::whereIn('company_id',$active_companies_ids)->where('status',1)->get();
            $flag_one = 0;
            $total_remaining_amount = 0;
        }else{
            $clients = Client::whereIn('company_id',$active_companies_ids)->where('status',1)
                            ->where('id',$client_id)->get();

            if(count($clients) == 0){
                \Session::flash('warning', 'هذا العميل او شركته غير نشط لذلك لا يستطيع الشراء');
                return redirect('/client/get-client');
            }

            $flag_one = 1;
            $purchases = Purchase::where('client_id',$client_id)->pluck('id');
            $purchases_premiums = PurchasePremium::whereIn('purchase_id',$purchases)->pluck('premium_id');
            $total_remaining_amount = Premiums::whereIn('id',$purchases_premiums)
                ->where('submitted_value_status',0)
                ->sum('remaining_amount');
        }

        $suppliers = User::where('role','Supplier')->where('status',1)->get();
        return view('purchases.add',compact('clients','suppliers','flag_one',
            'total_remaining_amount','confirmation_code'));
    }

    // store new purchase
    public function storeAdding(Request $request){
        $rules = [
            'client_id' => 'required|numeric',
            'commodity_name' => 'required|array',
            'price' => 'required|array',
            'paid_amount' => 'required|array',
        ];

        $this->validate($request,$rules);
        $client = Client::find($request->client_id);
        $client_company = Company::find($client->company_id);

        $purchases_ids = Purchase::where('client_id',$client->id)->pluck('id');
        $purchases_premiums_ids = PurchasePremium::whereIn('purchase_id',$purchases_ids)->pluck('premium_id')->toArray();
        $purchases_premiums_ids = array_unique($purchases_premiums_ids);
        $premiums = Premiums::whereIn('id',$purchases_premiums_ids)->where('submitted_value_status',0)->where('status',2)->count();

        $purchases_premiums = PurchasePremium::whereIn('purchase_id',$purchases_ids)->pluck('premium_id');
        $client_total_remaining_amount = Premiums::whereIn('id',$purchases_premiums)
            ->where('submitted_value_status',0)
            ->sum('remaining_amount');


        if($premiums > 0){
            \Session::flash('warning', 'هذا العميل لا يمكنها الشراء حتي يقوم بدفع ما عليه من اقساط');
            return redirect()->route('purchases.index');
        }

        $total_price = 0;
        for ($i = 0 ; $i < count($request->price) ; $i++){
            $total_price += $request->price[$i] * $request->quantity[$i];
        }
        // check if paid greater than total
        if($total_price < $request->paid_amount[0]){
            \Session::flash('warning', 'لمبلغ المدفوع اكبر من اجمالي سعر المشتريات ');
            return redirect()->route('purchases.index');
        }

        // check if purchases greater than user company depts
        if(($total_price > $client->max_value) || (($client_total_remaining_amount+$total_price) >  $client->fixed_max_value)){
            \Session::flash('warning', 'لقد قمت بالشراءباقصي مبلغ محدد لك من قبل');
            return redirect()->route('purchases.index');
        }

        if($client->max_value < $total_price){
            \Session::flash('warning', 'السعر الاجمالي اكبر من اقصي قيمة للشراء المحدده لهذا العميل');
            return redirect()->route('purchases.index');
        }

        if(Auth::user()->role != "Admin"){
            if(Auth::user()->role == "SupplierEmployee") {
                $supplier_branch = SupplierBranch::where('id',Auth::user()->supplier_employee_branch_id)->first();
                $supplier_id = $supplier_branch->supplier_id;
            }else{
                $supplier_id = Auth::user()->id;
            }
        }else{
            $supplier_id = $request->supplier_id;
        }

        $supplier = User::find($supplier_id);

        // Start transaction!
        DB::beginTransaction();

        try {

            $last_purchases = Purchase::where('client_id',$request->client_id)->pluck('id');
            $last_purchase_premiums = PurchasePremium::whereIn('purchase_id',$last_purchases)->pluck('premium_id')
                ->toArray();
            $last_purchase_premiums = array_unique($last_purchase_premiums);
            $last_premiums = Premiums::whereIn('id',$last_purchase_premiums)->where('remaining_amount','!=',0)
                                    ->where('submitted_value_status',0)->get();

            // create new purchase
            $new_purchase = new Purchase();
            $new_purchase->client_id = $request->client_id;
            $new_purchase->supplier_id	 = $supplier_id;
            $new_purchase->total_price = $total_price;
            $new_purchase->paid_amount = array_sum($request->paid_amount);
            $new_purchase->remaining_amount = $total_price - array_sum($request->paid_amount);
            $new_purchase->premiums_month = $supplier->premiums_months;
            $new_purchase->monthly_value = (( $total_price -  array_sum($request->paid_amount)) /  $supplier->premiums_months);
            $new_purchase->save();

            // store submitted value
            $date = date('Y-m-d');
            $new_premium = new Premiums();
            $new_premium->paid_amount = array_sum($request->paid_amount);;
            $new_premium->remaining_amount = 0;
            $new_premium->total_remaining_amount = $total_price - array_sum($request->paid_amount);
            $new_premium->premium_month_number = 0;
            $new_premium->status = 1;
            $new_premium->code = uniqid(rand(5,10));
            $new_premium->collect_date = $date;
            $new_premium->submitted_value_status = 1;
            $new_premium->collector_id = Auth::user()->id;
            $new_premium->save();

            // create purchase premium for submitted value
            $new_purchase_premium = new PurchasePremium();
            $new_purchase_premium->purchase_id = $new_purchase->id;
            $new_purchase_premium->premium_id = $new_premium->id;
            $new_purchase_premium->save();

            // store each product
            for ($i = 0 ; $i < count($request->price) ; $i++){
                $new_product = new PurchaseProduct();
                $new_product->purchase_id = $new_purchase->id;
                $new_product->commodity_name = $request->commodity_name[$i];
                $new_product->price = $request->price[$i];
                $new_product->quantity = $request->quantity[$i];
                $new_product->total_price = $request->price[$i] * $request->quantity[$i];
                $new_product->save();
            }

            $start = new \DateTime();
            $start = $start->format('Y-m-d');
            $day = $client->repayment_date;
            $expiry = self::collectDate($start,$day);

            if(count($last_premiums) > 0){

                // if number of premiums month == number of old premiums so no change in number of row
                if($supplier->premiums_months == count($last_premiums)){

                    $value_every_month = ( $total_price -  array_sum($request->paid_amount)) / $supplier->premiums_months;
                    foreach ($last_premiums as $premium){
                        Premiums::where('id',$premium->id)->update([
                            'remaining_amount' => $premium->remaining_amount + $value_every_month,
                            'total_remaining_amount' => $premium->total_remaining_amount + $value_every_month,
                        ]);

                        // create purchase premium
                        $new_purchase_premium = new PurchasePremium();
                        $new_purchase_premium->purchase_id = $new_purchase->id;
                        $new_purchase_premium->premium_id = $premium->id;
                        $new_purchase_premium->save();
                    }

                }
                elseif($supplier->premiums_months > count($last_premiums)){
                    // if number of premiums month > number of old premiums so we need to add additional row
                    $value_every_month = ( $total_price -  array_sum($request->paid_amount)) / $supplier->premiums_months;
                    foreach ($last_premiums as $premium){

                        Premiums::where('id',$premium->id)->update([
                            'remaining_amount' => $premium->remaining_amount + $value_every_month,
                            'total_remaining_amount' => $premium->total_remaining_amount + $value_every_month,
                        ]);

                        // create purchase premium
                        $new_purchase_premium = new PurchasePremium();
                        $new_purchase_premium->purchase_id = $new_purchase->id;
                        $new_purchase_premium->premium_id = $premium->id;
                        $new_purchase_premium->save();

                        $expiry = new \DateTime($premium->collect_date);

                    }

                    for ($i = count($last_premiums)+1 ; $i <= $supplier->premiums_months ; $i++){
                        $month = '+1 months';
                        $j = $i-1;
                        $date = $expiry->modify($month);
                        $new_premium = new Premiums();
                        $new_premium->paid_amount = 0;
                        $new_premium->remaining_amount = abs($value_every_month);
                        $new_premium->total_remaining_amount = abs($new_purchase->remaining_amount - ($value_every_month * $j));
                        $new_premium->premium_month_number = $i;
                        $new_premium->status = 0;
                        $new_premium->code = uniqid(rand(5,10));
                        $new_premium->collect_date = $date;
                        $new_premium->save();

                        // create purchase premium
                        $new_purchase_premium = new PurchasePremium();
                        $new_purchase_premium->purchase_id = $new_purchase->id;
                        $new_purchase_premium->premium_id = $new_premium->id;
                        $new_purchase_premium->save();
                    }

                }else{
                    // if number of premiums month < number of old premiums so we don't need to add additional row
                    $value_every_month = ( $total_price -  array_sum($request->paid_amount)) / $supplier->premiums_months;
                    foreach ($last_premiums as $key => $premium){
                        if($key >= $supplier->premiums_months ){
                            break;
                        }
                        Premiums::where('id',$premium->id)->update([
                            'remaining_amount' => $premium->remaining_amount + $value_every_month,
                            'total_remaining_amount' => $premium->total_remaining_amount + $value_every_month,
                        ]);

                        // create purchase premium
                        $new_purchase_premium = new PurchasePremium();
                        $new_purchase_premium->purchase_id = $new_purchase->id;
                        $new_purchase_premium->premium_id = $premium->id;
                        $new_purchase_premium->save();
                    }
                }
            }else{

                for ($i = 1 ; $i <= $supplier->premiums_months ; $i++){

                   $today_day = date('d',strtotime($start));

                   if(($i == 1) && ((int)$today_day <= $client->repayment_date)){
                       $month = '+0 Month';
                   }else{
                       $month = '+1 Month';
                   }

                    $j = abs($i-1);
                    $date = $expiry->modify($month);
                    $new_premium = new Premiums();
                    $new_premium->paid_amount = 0;
                    $new_premium->remaining_amount = abs($new_purchase->monthly_value);
                    $new_premium->total_remaining_amount = abs($new_purchase->remaining_amount - ($new_purchase->monthly_value * $j));
                    $new_premium->premium_month_number = $i;
                    $new_premium->status = 0;
                    $new_premium->code = uniqid(rand(5,10));
                    $new_premium->collect_date = $date;
                    $new_premium->save();

                    // create purchase premium
                    $new_purchase_premium = new PurchasePremium();
                    $new_purchase_premium->purchase_id = $new_purchase->id;
                    $new_purchase_premium->premium_id = $new_premium->id;
                    $new_purchase_premium->save();

                }

            }

            Client::where('id',$client->id)->decrement('max_value', $total_price);

        }catch(\Exception $e){
            DB::rollback();
            dd($e);
            \Session::flash('error', 'Something is wrong please try again');
            return redirect()->back();
        }

        DB::commit();

        $client = Client::find($request->client_id);
        $company = Company::where('id',$client->company_id)->first();
        $purchase_products = PurchaseProduct::where('purchase_id',$new_purchase->id)->get();
        $purchase = Purchase::find($new_purchase->id);
        $purchases_ids = Purchase::where('client_id',$client->id)->pluck('id');
        $premiums_ids = PurchasePremium::whereIn('purchase_id',$purchases_ids)->pluck('premium_id');
        $premiums = Premiums::whereIn('id',$premiums_ids)->where('submitted_value_status',0)->get();
        $total_paid = Premiums::whereIn('id',$premiums_ids)->where('submitted_value_status',0)->sum('paid_amount');
        $total_remaining = Premiums::whereIn('id',$premiums_ids)->where('submitted_value_status',0)->sum('remaining_amount');

        $client->max_value = $client->max_value + $request->paid_amount[0];
        $client->update();

        \Session::flash('success', 'Purchase is created successfully');

        \Session::put(['client'=>$client,'premiums'=>$premiums,'company'=>$company,
            'purchase_products'=>$purchase_products,'purchase'=>$purchase,'total_paid'=>$total_paid,
            'total_remaining'=>$total_remaining]);

        return redirect()->route('purchases.purchaseBill');


    }

    // print purchase bill
    public function purchaseBill(){

        if(!\Session::has('client')){
            return redirect('/purchases');
        }

        $data = [];
        $data['client'] = \Session::get('client');
        $data['premiums'] = \Session::get('premiums');
        $data['company'] = \Session::get('company');
        $data['purchase_products'] = \Session::get('purchase_products');
        $data['purchase'] = \Session::get('purchase');
        $data['total_paid'] = \Session::get('total_paid');
        $data['total_remaining'] = \Session::get('total_remaining');

        \Session::forget('client');
        \Session::forget('premiums');
        \Session::forget('company');
        \Session::forget('purchase_products');
        \Session::forget('purchase');
        \Session::forget('total_paid');
        \Session::forget('total_remaining');


        $html = view('bills.purchase-bill',$data)->render(); // file render
        $pdfarr = [
            'title'=>'الفاتورة',
            'data'=>$html, // render file blade with content html
            'header'=>['show'=>false], // header content
            'footer'=>['show'=>false], // Footer content
            'font'=>'aealarabiya', //  dejavusans, aefurat ,aealarabiya ,times
            'font-size'=>12, // font-size
            'text'=>'', //Write
            'rtl'=>true, //true or false
            'creator'=>'phpanonymous', // creator file - you can remove this key
            'keywords'=>'phpanonymous keywords', // keywords file - you can remove this key
            'subject'=>'فاتورة', // subject file - you can remove this key
            'filename'=>'فاتورة.pdf', // filename example - invoice.pdf
            'display'=>'print', // stream , download , print
        ];

        return \PDF::HTML($pdfarr);
    }

    // return edit view
    public function editForm($id){
        $clients = Client::get();
        $purchase = Purchase::find($id);
        $employee_suppliers_ids = SupplierRole::whereIn('role',[0,2])->pluck('supplier_id');
        $suppliers = User::where('role','Supplier')->orWhereIn('id',$employee_suppliers_ids)->get();
        return view('purchases.edit',compact('clients','purchase','suppliers'));
    }

    // store edit
    public function storeEdit(Request $request){

        $rules = [
            'client_id' => 'required|numeric',
            'commodity_name' => 'required|string',
            'commodity_description' => 'required|string',
            'total_price' => 'required|string',
            'paid_amount' => 'required|string',
            'remaining_amount' => 'required|string',
        ];

        $this->validate($request,$rules);

        $client = Client::find($request->client_id);
        $client_company = Company::find($client->company_id);

        if(Auth::user()->role != "Admin"){
            $supplier_id = Auth::user()->id;
        }else{
            $supplier_id = $request->supplier_id;
        }

        $supplier = User::find($supplier_id);

        // create new purchase
        $new_purchase = Purchase::find($request->id);
        $new_purchase->client_id = $request->client_id;
        $new_purchase->supplier_id	 = $supplier_id;
        $new_purchase->commodity_description = $request->commodity_description;
        $new_purchase->total_price = $request->total_price;
        $new_purchase->paid_amount = $request->paid_amount;
        $new_purchase->remaining_amount = $request->total_price - $request->paid_amount;
        $new_purchase->premiums_month = $supplier->premiums_months;
        $new_purchase->monthly_value = (( $request->total_price - $request->paid_amount) / $supplier->premiums_months);
        $new_purchase->save();

        \Session::flash('success', 'Purchase data is updated successfully');
        return redirect()->route('purchases.index');

    }

    // purchases products
    public function products($id){
        return view('purchases.products',compact('id'));
    }

    // fetch purchases products
    public function fetchProducts($id)
    {
        $products = PurchaseProduct::where('purchase_id',$id)->where('recovery_status',0)->get();

        return DataTables::of($products)
            ->addColumn('commodity_name', function ($purchase) {
                return $purchase->commodity_name;
            })
            ->addColumn('quantity', function ($products) {
                return $products->quantity;
            })
            ->addColumn('price', function ($products) {
                return $products->price;
            })
            ->addColumn('total_price', function ($products) {
                return $products->total_price;
            })
            ->addColumn('action', function ($products) {

                return '<a class="btn action-btn" onclick=\'deleteProduct('.$products->id.",".$products->quantity.')\' title="حذف"><span class="fa fa-remove"></span></a>';
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    // delete product from purchase
    public function deleteProduct(Request $request){
            $product = PurchaseProduct::find($request->id);
            // check refunded quantity and existing quantity
            if((int) $product->quantity < $request->count){
                return response()->json(['status'=>'خطأ','msg'=>'العدد المراد استرجاعه اكبر من العدد الذي تم شراءه']);
            }

            $total_price = $request->count * $product->price;

            $premiums_ids = PurchasePremium::where('purchase_id',$product->purchase_id)->pluck('premium_id')->toArray();
            $check_if_paid_premium = Premiums::whereIn('id',$premiums_ids)->where('submitted_value_status',0)->where('paid_amount','!=',0)->get();

            if(count($check_if_paid_premium) > 0){
                return response()->json(['status'=>'خطأ','msg'=>'ﻻ يمكن الاسترجاع لان العميل قم بدفع احد الاقساط']);
            }

            $premiums = Premiums::whereIn('id',$premiums_ids)->where('submitted_value_status',0)->where('status',0)->get();
            $monthly_value = $total_price / count($premiums);
            Client::where('id',$product->purchase->client_id)->increment('max_value', $total_price);
            Purchase::where('id',$product->purchase_id)->decrement('total_price', $total_price);
            Purchase::where('id',$product->purchase_id)->where('remaining_amount','>',$total_price)
                    ->decrement('remaining_amount', $total_price);
            Purchase::where('id',$product->purchase_id)->decrement('monthly_value', $monthly_value);

            $purchase = Purchase::where('id',$product->purchase_id)->first();

            foreach ($premiums as $premium){
                $j = 1;
                Premiums::where('id',$premium->id)->decrement('remaining_amount',$monthly_value);
                Premiums::where('id',$premium->id)->decrement('total_remaining_amount',$monthly_value);
                $j++;
            }

            if($request->count == $product->quantity){
                $status = 1;
            }else{
                $status = 0;
            }
            $product->recovery_status = $status;
            $product->recovery_count = $product->recovery_count + $request->count;
            $product->quantity = $product->quantity - $request->count;
            $product->total_price = $product->total_price - ($request->count * $product->price);
            $product->update();

        return response()->json(['status'=>'تم','msg'=>'تم الحذف بنجاح']);

    }

    // return Premiums
    public function premiums($id){
        return view('purchases.premiums-list',compact('id'));
    }
    // fetch premiums
    public function fetchPremiums($id){
        if(Auth::user()->role == "Collector"){
            $premiums_ids = PurchasePremium::where('purchase_id',$id)->pluck('premium_id')->toArray();
            $premiums = Premiums::whereIn('id',$premiums_ids)
                        ->where('submitted_value_status',0)
                        ->where('collect_date','<=',Carbon::now())
                        ->get();
        }else{
            $premiums_ids = PurchasePremium::where('purchase_id',$id)->pluck('premium_id')->toArray();
            $premiums = Premiums::whereIn('id',$premiums_ids)->where('submitted_value_status',0)->get();
        }

        return DataTables::of($premiums)
            ->addColumn('paid_amount', function ($premium) {
                return $premium->paid_amount;
            })
            ->addColumn('remaining_amount', function ($premium) {
                return $premium->remaining_amount;
            })
            ->addColumn('total_remaining_amount', function ($premium) {
                return $premium->total_remaining_amount;
            })
            ->addColumn('premium_month_number', function ($premium) {
                return $premium->premium_month_number;
            })
            ->addColumn('status', function ($premium) {
                if($premium->status == 0){
                    return 'لم يتم الدفع';
                }elseif ($premium->status == 1){
                    return 'تم الدفع';
                }else{
                    return 'متاخر';
                }
            })
            ->addColumn('code', function ($premium) {
                return $premium->code;
            })
            ->addColumn('collect_date', function ($premium) {
                return $premium->collect_date;
            })
            ->addColumn('updated_at', function ($premium) {
                if($premium->paid_amount > 0){
                    return $premium->updated_at;
                }
                return "-";
            })
            ->addColumn('collector', function ($premium) {
                if($premium->collector_id == 0){
                    return 'لم يتم التحصيل';
                }else{
                    return $premium->collector->name;
                }
            })
            ->addColumn('action', function ($premium) {
                if($premium->status == 1){
                    return '<a class="btn action-btn" href="/purchases/premiums/download-bill-pay/'.$premium->id.'" title="طباعة الفاتورة"><span class="fa fa-print"></span></a>';
                }else{
                    if(Auth::user()->role == "Admin"){
                        return '<a class="btn action-btn" onclick=\'changeCollectDate('.$premium->id.','.$premium->collect_date.')\' title="تغير ميعاد السداد"><span class="fa fa-hand-grab-o"></span></a>';                    }
                }
            })
            ->rawColumns(['action'])
            ->make(true);
        }
    // pay premium
    public function premiumsPay(Request $request){
        $rules = [
            'id' => 'required|numeric',
            'paidAmount' => 'required',
        ];

        $this->validate($request,$rules);

        $premium = Premiums::find($request->id);
        $paid_amount = $request->paidAmount + $premium->paid_amount;//20+50=70
        $remaining_amount = $premium->remaining_amount - $request->paidAmount;//40-200=20

        if($paid_amount < 0 || $remaining_amount < 0){
            return response()->json(['status'=>'error','title'=>'خطأ','msg'=>'الملبغ المدفوع غير صحيح','id'=>$request->id]);
        }
        $setting = Setting::first();
        if(is_null($setting)){
            $subscription_value = 5;
        }else{
            $subscription_value = $setting->subscription_value;
        }

        $paid_amount = $paid_amount + $subscription_value;

        Premiums::where('id',$request->id)->update([
            'status' => 1,
            'paid_amount' => $paid_amount,
            'remaining_amount' => $remaining_amount,
            'collector_id' => Auth::user()->id,
        ]);

        $purchases_ids = PurchasePremium::where('premium_id',$request->id)->pluck('purchase_id');
        $paidAmount = $request->paidAmount - $subscription_value;
        foreach($purchases_ids as $id){

            $purchase = Purchase::where('id',$id)->first();

            if($purchase->remaining_amount != 0) {

                if($paidAmount >=  $purchase->premiums_month){

                    if($purchase->next_premium_number == $premium->premium_month_number){
                        $purchase->paid_amount = $purchase->paid_amount + $purchase->monthly_value;
                        $purchase->remaining_amount = $purchase->remaining_amount - $purchase->monthly_value;
                        $paidAmount = $paidAmount - $purchase->monthly_value;
                        $purchase->next_premium_number = $purchase->next_premium_number + 1;
                    }

                } else {

                    if($purchase->next_premium_number == $premium->premium_month_number) {
                        $purchase->paid_amount = $purchase->paid_amount + $paidAmount;
                        $purchase->remaining_amount = $purchase->remaining_amount - $paidAmount;
                        $paidAmount = 0;
                        $purchase->next_premium_number = $purchase->next_premium_number + 1;
                    }

                }
            }

            $purchase->update();

            if($paidAmount <= 0){
                break;
            }

        }
        Client::where('id',$purchase->client_id)->increment('max_value' , $request->paidAmount);

        return response()->json(['status'=>'success','title'=>'Done','msg'=>'تم الدفع بنجاح','id'=>$request->id]);

    }

    // relay remaining amount
    public function relayRemainingAmount(Request $request){
        $rules = [
            'id' => 'required|numeric',
            'purchase_id' => 'required|numeric',
        ];

        $this->validate($request,$rules);

        $premium = Premiums::where('id',$request->id)->first();
        $purchase_premiums = PurchasePremium::where('purchase_id',$request->purchase_id)->orderBy('created_at','asc')
                                            ->pluck('premium_id');
        foreach ($purchase_premiums as $key => $value){
            if($value == $premium->id){
                $next_premium = $purchase_premiums[$key+1];
                break;
            }
        }

        Premiums::where('id',$next_premium)
            ->where('premium_month_number',$premium->premium_month_number+1)
            ->where('submitted_value_status',0)
            ->increment('remaining_amount',$premium->remaining_amount);
        Premiums::where('id',$next_premium)
            ->where('premium_month_number',$premium->premium_month_number+1)
            ->where('submitted_value_status',0)
            ->increment('total_remaining_amount',$premium->remaining_amount);

        Premiums::where('id',$request->id)->update([
            'remaining_amount' => 0,
        ]);

        return response()->json(['status'=>'success','title'=>'Done','msg'=>'تم ترحيل المبلغ الي الشهر القادم']);
    }

    // relay remaining amount to new month
    public function relayRemainingAmountToNewMonth(Request $request){
        $rules = [
            'id' => 'required|numeric',
            'purchase_id' => 'required|numeric',
        ];

        $this->validate($request,$rules);

        $premium = Premiums::find($request->id);
        $purchase_premium= PurchasePremium::where('purchase_id',$request->purchase_id)->orderBy('created_at','decs')
            ->first();
        $last_premium = Premiums::where('id',$purchase_premium->premium_id)->where('submitted_value_status',0)->first();


        $new_premium = new Premiums();
        $new_premium->paid_amount = 0;
        $new_premium->remaining_amount = $premium->remaining_amount;
        $new_premium->total_remaining_amount = $premium->remaining_amount;
        $new_premium->premium_month_number = $last_premium->premium_month_number + 1;
        $new_premium->status = 0;
        $new_premium->code = uniqid(rand(5,10));
        $time = strtotime($last_premium->collect_date);
        $collect_date = date("Y-m-d", strtotime("+1 month", $time));
        $new_premium->collect_date = $collect_date;
        $new_premium->save();

        Premiums::where('id',$request->id)->update([
            'remaining_amount' => 0,
        ]);

        // create purchase premium
        $new_purchase_premium = new PurchasePremium();
        $new_purchase_premium->purchase_id = $request->purchase_id;
        $new_purchase_premium->premium_id = $new_premium->id;
        $new_purchase_premium->save();


        return response()->json(['status'=>'success','title'=>'Done','msg'=>'تم ترحيل المبلغ الي شهر اضافي']);
    }

    // download pay bill
    public function downloadBillPay($id){
            $purchase_premium= PurchasePremium::where('premium_id',$id)->orderBy('created_at','decs')
                                                ->first();
            $premium = Premiums::find($id);

            $purchase = Purchase::find($purchase_premium->purchase_id);
            $client = Client::find($purchase->client_id);
            $company = Company::find($client->company_id);
            $client_purchases_ids = Purchase::where('client_id',$client->id)->pluck('id')->toArray();
            $purchase_premiums_ids= PurchasePremium::whereIn('purchase_id',$client_purchases_ids)
                                                    ->pluck('premium_id')->toArray();
            $purchase_premiums_ids = array_unique($purchase_premiums_ids);
            $collective_premiums = Premiums::whereIn('id',$purchase_premiums_ids)->where('submitted_value_status',0)->get();
            $sum_collective_premiums = Premiums::whereIn('id',$purchase_premiums_ids)->where('submitted_value_status',0)->sum('remaining_amount');
            $setting = Setting::first();

            $html = view('bills.pay-premium',['premium' => $premium,
                'client'=>$client,'company'=>$company,'collective_premiums'=>$collective_premiums,
                'sum_collective_premiums'=>$sum_collective_premiums,'setting'=>$setting])->render(); // file render
            $pdfarr = [
                'title'=>'تقرير شامل',
                'data'=>$html, // render file blade with content html
                'header'=>['show'=>false], // header content
                'footer'=>['show'=>false], // Footer content
                'font'=>'aealarabiya', //  dejavusans, aefurat ,aealarabiya ,times
                'font-size'=>12, // font-size
                'text'=>'', //Write
                'rtl'=>true, //true or false
                'creator'=>'Mosahal', // creator file - you can remove this key
                'keywords'=>'bill', // keywords file - you can remove this key
                'subject'=>'bill', // subject file - you can remove this key
                'filename'=>'bill.pdf', // filename example - invoice.pdf
                'display'=>'print', // stream , download , print
            ];

            return \PDF::HTML($pdfarr);
    }

    public function delete(Request $request){
        $purchase = Purchase::find($request->id);
        if(!$purchase->remaining_amount){
            return response()->json(['status'=>'error','title'=>'Done','msg'=>'لم تستطيع مسح المبيعه لانه تم دفع فسط من قبل ']);
        }
        $purchase = Purchase::where('id',$request->id)->first();
        $premiums_ids = PurchasePremium::where('purchase_id',$request->id)->pluck('premium_id')->toArray();

        Premiums::whereIn('id',$premiums_ids)->decrement('remaining_amount',$purchase->monthly_value);
        Premiums::whereIn('id',$premiums_ids)->where('paid_amount',0)->decrement('total_remaining_amount',$purchase->monthly_value);
        Premiums::whereIn('id',$premiums_ids)->where('remaining_amount','<=',0)->delete();

        PurchaseProduct::where('purchase_id',$request->id)->delete();
        PurchasePremium::where('purchase_id',$request->id)->delete();

        Client::where('id',$purchase->client_id)->increment('max_value',$purchase->total_price);
        Purchase::where('id',$request->id)->delete();

        return response()->json(['status'=>'success','title'=>'Done','msg'=>'تم الحذف بنجاح']);
    }

    // purchases products
    public function recoveryProducts($id){
        return view('purchases.recovery-products',compact('id'));
    }

    // fetch purchases products
    public function fetchRecoveryProducts($id)
    {

        $total_recovery_products_ids = PurchaseProduct::where('purchase_id',$id)->where('recovery_status',1)->pluck('id');
        $some_recovery_products_ids = PurchaseProduct::where('purchase_id',$id)->where('recovery_status',0)
                                                    ->where('recovery_count','>',0)->pluck('id');
        $products = PurchaseProduct::whereIn('id',$total_recovery_products_ids)->orWhereIn('id',$some_recovery_products_ids)
                                    ->get();

        return DataTables::of($products)
            ->addColumn('commodity_name', function ($purchase) {
                return $purchase->commodity_name;
            })
            ->addColumn('quantity', function ($products) {
                return $products->recovery_count;
            })
            ->addColumn('price', function ($products) {
                return $products->price;
            })
            ->addColumn('total_price', function ($products) {
                return $products->price * $products->recovery_count;
            })
            ->make(true);
    }

    public function collectDate($date,$day){

        $date = explode('-', $date); // explode to get array of YY-MM-DD
        $date[2] = $day; // this would change the previous value of DD/Day to this one. Or input any value you want to execute when the button is triggered
        $date = implode('-', $date); // that will output '2013-10-10'.
        $date = date_create($date);

        return $date;
    }


    public function changeCollectDate(Request $request){

        $check_if_premiums_is_paid = Premiums::find($request->premium_id);
        if(isset($check_if_premiums_is_paid)){
            if($check_if_premiums_is_paid->status == 1){
                return redirect()->back()->with('warning','The premium is paid before so you can not change collect date');

            }else{
                Premiums::where('id',$request->premium_id)->update([
                    'collect_date' => $request->collect_date,
                ]);
            }
        }else{
            return redirect()->back()->with('error','The premium is not found');
        }


        return redirect()->back()->with('success','the premium collect date changed successfully');
    }

    // send confirmation code to mobile
    public function sendConfirmationCode(Request $request){

        $client = Client::where('id',$request->id)->first();
        if(is_null($client)){
            return response()->json(['code' => 0]);
        }
        $user = User::where('id',$client->user_id)->first();
        $confirmation_code = str_random(6);
        $msg = $confirmation_code.' كود التاكيد الخاص بك هو ';

        if($client->auto_buy_option == 1){
            return response()->json(['status'=>'success','code' => 1]);
        }

        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60*20);

        $notificationBuilder = new PayloadNotificationBuilder('Confirmation Code');
        $notificationBuilder->setBody($msg)
            ->setSound('default');

        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData(['confirmation_code' => 'Confirmation code']);

        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();

        $token = $user->fcmToken;

        $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);

        if($downstreamResponse->numberSuccess()){
            return response()->json(['status'=>'success','code' => $confirmation_code]);
        }else{
            return response()->json(['status'=>'success','code' => 0]);
        }

    }

    public function sendNotification($device_token){

        $confirmation_code = str_random(6);
        $msg = $confirmation_code.' كود التاكيد الخاص بك هو ';

        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60*20);

        $notificationBuilder = new PayloadNotificationBuilder('Confirmation Code');
        $notificationBuilder->setBody($msg)
            ->setSound('default');

        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData(['confirmation_code' => 'Confirmation code']);

        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();

        $token = $device_token;
//        $token = "dD7S4FiLOF4:APA91bHOqfXYoqRVsjCct666X-Wjp-mVCLuU7QoySmbkG2iI26rgBJxzCJS0LpBurBhz-a2DvQPlDEBwC8JF43F4cA3hZCZ9qPDqZgeFWfOr32AMAv3HCrgr9wzHYv7XH0-tL4-lb4XG";

        $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);
        dd($downstreamResponse);
        return response()->json(['status'=>'success','code' => $confirmation_code]);
    }
}
