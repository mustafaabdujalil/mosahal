<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('auth')->group(function () {
    Route::post('login', 'API\AuthController@login');
    Route::get('token', 'API\AuthController@validateAccessToken')->name('token');
});
/*/************************APIs routes with auth**************************/

Route::group(['middleware' => ['auth:api','checkToken']], function() {
    /*/********logout route***************/
    Route::prefix('auth')->group(function () {
        Route::get('logout','API\AuthController@logout');
        Route::post('set-device-token','API\AuthController@setDeviceToken');
    });

    Route::group(['middleware' => ['PrivilegesMiddleware']], function() {
        /*/********clients route***************/
        Route::prefix('client')->group(function () {
            Route::get('report','API\ClientController@report');
            Route::get('stores','API\ClientController@stores');
            Route::post('pay','API\ClientController@premiumsPay');
            Route::post('token','API\AuthController@token');
        });

        /*/********supplier route***************/
        Route::prefix('supplier')->group(function () {
            Route::get('list','API\SupplierController@list');
        });

        /*/********company route***************/
        Route::prefix('company')->group(function () {
            Route::get('suppliers/list','API\SupplierController@companySuppliers');
        });

        /*/********collectors route***************/
        Route::prefix('collector')->group(function () {
            Route::get('client-premiums','API\CollectorController@clientPremiums');
            Route::get('daily-premiums','API\CollectorController@dailyPremiums');
            Route::get('companies-list','API\CollectorController@companiesList');
        });
    });
});

/*/********Handle error if routes is not found***************/
Route::fallback(function(){
    $msg = new \stdClass();
    $msg->messages[] = "Request is not found !";

    return response()->json(['status'=> false,'msg'=>$msg,'data' => new \stdClass()],404);
});
