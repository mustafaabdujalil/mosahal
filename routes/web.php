<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');



Route::group(['middleware' => ['auth']], function () {

    // Admin Routes
    Route::group(['prefix' => 'admin',  'middleware' => 'AdminRole'], function() {
        Route::get('/','AdminController@index')->name('admin.index');
        Route::get('/fetchAdmins','AdminController@fetchAdmins')->name('admin.fetchAdmins');
        Route::get('/edit/{id}','AdminController@editForm')->name('admin.editForm');
        Route::post('/store_edit','AdminController@storeEdit')->name('admin.storeEdit');
        Route::post('/delete','AdminController@delete')->name('admin.delete');
        Route::get('/add_new_admin','AdminController@addForm')->name('admin.addForm');
        Route::post('/store_adding','AdminController@storeAdding')->name('admin.storeAdding');
        Route::post('/store_debt_value','AdminController@storeDebtValue')->name('admin.storeDebtValue');

    });
    // Supplier Routes
    Route::prefix('supplier')->group(function () {
        Route::group(['middleware' => ['AdminRole']], function () {
            Route::get('/', 'SupplierController@index')->name('supplier.index');
            Route::get('/fetchSuppliers', 'SupplierController@fetchSuppliers')->name('supplier.fetchSuppliers');
            Route::get('/edit/{id}', 'SupplierController@editForm')->name('supplier.editForm');
            Route::post('/store_edit', 'SupplierController@storeEdit')->name('supplier.storeEdit');
            Route::post('/delete', 'SupplierController@delete')->name('supplier.delete');
            Route::get('/add_new_supplier', 'SupplierController@addForm')->name('supplier.addForm');
            Route::post('/store_adding', 'SupplierController@storeAdding')->name('supplier.storeAdding');
        });
        Route::get('/branches/{id}','SupplierController@branches')->name('supplier.branches');
        Route::get('/add_new_branch/{id}','SupplierController@addBranchForm')->name('supplier.addBranchForm');
        Route::post('/store_adding_branch','SupplierController@storeAddingBranch')->name('supplier.storeAddingBranch');
        Route::get('/fetchBranches','SupplierController@fetchBranches')->name('supplier.fetchBranches');
        Route::post('/delete_branch','SupplierController@deleteBranch')->name('supplier.deleteBranch');
        Route::get('/employees/{id}','SupplierController@employees')->name('supplier.employees');
        Route::get('/add_new_employee/{id}','SupplierController@addEmployeeForm')->name('supplier.addEmployeeForm');
        Route::post('/store_adding_employee','SupplierController@storeAddingEmployee')->name('supplier.storeAddingEmployee');
        Route::get('/fetchEmployees','SupplierController@fetchEmployees')->name('supplier.fetchEmployees');
        Route::post('/delete_employee','SupplierController@deleteEmployee')->name('supplier.deleteEmployee');

    });
    // Collector Routes
    Route::group(['prefix' => 'collector',  'middleware' => 'AdminRole'], function() {
        Route::get('/','CollectorController@index')->name('collector.index');
        Route::get('/fetchCollectors','CollectorController@fetchCollectors')->name('collector.fetchCollectors');
        Route::get('/edit/{id}','CollectorController@editForm')->name('collector.editForm');
        Route::post('/store_edit','CollectorController@storeEdit')->name('collector.storeEdit');
        Route::post('/delete','CollectorController@delete')->name('collector.delete');
        Route::get('/add_new_collector','CollectorController@addForm')->name('collector.addForm');
        Route::post('/store_adding','CollectorController@storeAdding')->name('collector.storeAdding');
        Route::get('/premiums/{id}','CollectorController@premiums')->name('collector.premiums');
        Route::get('/fetchPremiums','CollectorController@fetchPremiums')->name('collector.fetchPremiums');
    });
    // Clients Routes
    Route::prefix('client')->group(function () {
        Route::get('/','ClientController@index')->name('client.index');
        Route::get('/fetchClients','ClientController@fetchClients')->name('client.fetchClients');
        Route::get('/edit/{id}','ClientController@editForm')->name('client.editForm');
        Route::post('/store_edit','ClientController@storeEdit')->name('client.storeEdit');
        Route::post('/delete','ClientController@delete')->name('client.delete');
        Route::get('/add_new_client','ClientController@addForm')->name('client.addForm');
        Route::post('/store_adding','ClientController@storeAdding')->name('client.storeAdding');
        Route::get('/premiums/{id}','ClientController@premiums')->name('client.premiums');
        Route::get('/fetchPremiums/{id}','ClientController@fetchPremiums')->name('client.fetchPremiums');
        Route::get('/next-premiums/{id}','ClientController@nextPremiums')->name('client.nextPremiums');
        Route::get('/filter-next-premiums','ClientController@filterNextPremiums')->name('client.filterNextPremiums');
        Route::get('/fetchNextPremiums/{id}','ClientController@fetchNextPremiums')->name('client.fetchNextPremiums');
        Route::get('/recovery-products/{id}','ClientController@recoveryProducts')->name('client.recoveryProducts');
        Route::get('/fetchRecoveryProducts/{id}','ClientController@fetchRecoveryProducts')->name('client.fetchRecoveryProducts');
        Route::get('/products/{id}','ClientController@products')->name('client.products');
        Route::get('/fetchProducts/{id}','ClientController@fetchProducts')->name('client.fetchProducts');
        Route::get('/get-client','ClientController@getClient')->name('client.getClient');
        Route::get('/fetchClient','ClientController@fetchClient')->name('client.fetchClient');
        Route::get('/get-client-by-name','ClientController@getClientByName')->name('client.getClientByName');
        Route::get('/fetchClientByName','ClientController@fetchClientByName')->name('client.fetchClientByName');
        Route::get('/report/{client_id}','ClientController@report')->name('client.report');
        Route::get('/related-clients/{id}','ClientController@relatedClients')->name('client.relatedClients');
        Route::get('/fetchRelatedClients/{id}','ClientController@fetchRelatedClients')->name('client.fetchRelatedClients');

    });
    // purchases Routes
    Route::group(['prefix' => 'purchases',  'middleware' => 'PurchasesAccess'], function() {
        Route::get('/','PurchasesController@index')->name('purchases.index');
        Route::get('/supplierPurchases/{id}','PurchasesController@supplierPurchases')->name('purchases.supplierPurchases');
        Route::get('/fetchPurchases/{id}','PurchasesController@fetchPurchases')->name('purchases.fetchPurchases');
        Route::get('/edit/{id}','PurchasesController@editForm')->name('purchases.editForm');
        Route::get('/premiums/{id}','PurchasesController@premiums')->name('purchases.premiums');
        Route::get('/fetchPremiums/{id}','PurchasesController@fetchPremiums')->name('purchases.fetchPremiums');
        Route::get('/products/{id}','PurchasesController@products')->name('purchases.products');
        Route::get('/fetchProducts/{id}','PurchasesController@fetchProducts')->name('purchases.fetchProducts');
        Route::post('/product/delete','PurchasesController@deleteProduct')->name('purchases.product.delete');
        Route::post('/premiums/pay','PurchasesController@premiumsPay')->name('purchases.premiums.pay');
        Route::post('/premiums/relay-remaining-amount','PurchasesController@relayRemainingAmount')->name('purchases.premiums.relay.remaining.amount');
        Route::post('/premiums/relay-remaining-amount-to-new-month','PurchasesController@relayRemainingAmountToNewMonth')
            ->name('purchases.premiums.relay-remaining-amount-to-new-month');
        Route::post('/store_edit','PurchasesController@storeEdit')->name('purchases.storeEdit');
        Route::get('/delete','PurchasesController@delete')->name('purchases.delete');
        Route::get('/add_new_purchases/{client_id?}','PurchasesController@addForm')->name('purchases.addForm');
        Route::post('/store_adding','PurchasesController@storeAdding')->name('purchases.storeAdding');
        Route::get('/bill','PurchasesController@purchaseBill')->name('purchases.purchaseBill');
        Route::get('/recovery-products/{id}','PurchasesController@recoveryProducts')->name('purchases.recoveryProducts');
        Route::get('/fetchRecoveryProducts/{id}','PurchasesController@fetchRecoveryProducts')->name('purchases.fetchRecoveryProducts');
        Route::post('/premiums/change-collect-date','PurchasesController@changeCollectDate')->name('purchases.changeCollectDate');
    });
    // Company Routes
    Route::group(['prefix' => 'company',  'middleware' => 'AdminRole'], function() {
        Route::get('/','CompanyController@index')->name('company.index');
        Route::get('/fetchCompanies','CompanyController@fetchCompanies')->name('company.fetchCompanies');
        Route::get('/edit/{id}','CompanyController@editForm')->name('company.editForm');
        Route::post('/store_edit','CompanyController@storeEdit')->name('company.storeEdit');
        Route::get('/delete','CompanyController@delete')->name('company.delete');
        Route::get('/add_new_company','CompanyController@addForm')->name('company.addForm');
        Route::post('/store_adding','CompanyController@storeAdding')->name('company.storeAdding');
        Route::get('/suppliers/{id}','CompanyController@suppliers')->name('company.suppliers');
        Route::get('/fetchCompanySuppliers','CompanyController@fetchCompanySuppliers')->name('company.fetchCompanySuppliers');
        Route::get('/supplier/add','CompanyController@addSupplierForm')->name('company.supplier.addForm');
        Route::post('/supplier/store','CompanyController@storeSupplier')->name('company.supplier.store');
        Route::delete('/supplier/delete','CompanyController@deleteSupplier')->name('company.deleteSupplier');
    });
    // reports Routes
    Route::group(['prefix' => 'reports',  'middleware' => 'AdminRole'], function() {
        Route::get('/companies','ReportsController@companies')->name('reports.companies.index');
        Route::get('/fetch-companies','ReportsController@fetchCompanies')->name('reports.companies.fetch-companies');
        Route::get('/companies/clients/{id}','ReportsController@companiesClients')->name('reports.companies.clients');
        Route::get('/fetch-companies-clients/{id}','ReportsController@fetchCompaniesClients')->name('reports.companies.fetch-clients');
    });
    // store Routes
    Route::group(['prefix' => 'store'], function() {
        Route::get('/','StoreController@index')->name('store.index');
        Route::get('/reset-filter-form','StoreController@resetFilterForm')->name('store.resetFilterForm');
        Route::get('/fetchCollectors','StoreController@fetchCollectors')->name('store.fetchCollectors');
    });
    // reports Routes
    Route::group(['prefix' => 'reports'], function() {
        Route::get('/companies','ReportsController@companies')->name('reports.companies.index');
        Route::get('/fetch-companies','ReportsController@fetchCompanies')->name('reports.companies.fetch-companies');
        Route::get('/companies/clients/{id}','ReportsController@companiesClients')->name('reports.companies.clients');
        Route::get('/fetch-companies-clients/{id}','ReportsController@fetchCompaniesClients')->name('reports.companies.fetch-clients');

        Route::get('/suppliers','ReportsController@suppliers')->name('reports.suppliers.index');
        Route::get('/fetch-suppliers','ReportsController@fetchSuppliers')->name('reports.suppliers.fetch-suppliers');
        Route::get('/supplier/clients/{id}','ReportsController@supplierClients')->name('reports.supplier.clients');
        Route::get('/fetch-supplier-clients/{id}','ReportsController@fetchSupplierClients')->name('reports.supplier.fetch-clients');
    });
    // Collector Routes
    Route::group(['prefix' => 'settings',  'middleware' => 'AdminRole'], function() {
        Route::get('/','SettingController@index')->name('settings.index');
        Route::get('/fetchSetting','SettingController@fetchSetting')->name('settings.fetchSetting');
        Route::get('/edit','SettingController@editForm')->name('settings.editForm');
        Route::post('/store_edit','SettingController@storeEdit')->name('settings.storeEdit');
    });

    Route::group(['prefix' => 'reports'], function() {
        Route::get('/suppliers','ReportsController@suppliers')->name('reports.suppliers.index');
        Route::get('/fetch-suppliers','ReportsController@fetchSuppliers')->name('reports.suppliers.fetch-suppliers');
        Route::get('/supplier/clients/{id}','ReportsController@supplierClients')->name('reports.supplier.clients');
        Route::get('/fetch-supplier-clients/{id}','ReportsController@fetchSupplierClients')->name('reports.supplier.fetch-clients');

    });

});

Route::get('/purchases/premiums/download-bill-pay/{id}','PurchasesController@downloadBillPay')->name('purchases.premiums.bill.pay');
Route::post('/purchases/send-confirmation-code','PurchasesController@sendConfirmationCode')->name('purchases.sendConfirmationCode');


Route::get('/notification/{device_token}','PurchasesController@sendNotification');
