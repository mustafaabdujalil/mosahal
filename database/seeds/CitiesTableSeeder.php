<?php

use Illuminate\Database\Seeder;
use App\City;

class CitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // cities array
		$cities = array(
			array('name' => "Aswan"),
			array('name' => "Asyut"),
			array('name' => "Bani Suwayf"),
			array('name' => "Bur Sa'id"),
			array('name' => "Cairo"),
			array('name' => "Dumyat"),
			array('name' => "Kafr-ash-Shaykh"),
			array('name' => "Matruh"),
			array('name' => "Muhafazat ad Daqahliyah"),
			array('name' => "Muhafazat al Fayyum"),
			array('name' => "Muhafazat al Gharbiyah"),
			array('name' => "Muhafazat al Iskandariyah"),
			array('name' => "Muhafazat al Qahirah"),
			array('name' => "Qina"),
			array('name' => "Sawhaj"),
			array('name' => "Sina al-Janubiyah"),
			array('name' => "Sina ash-Shamaliyah"),
			array('name' => "ad-Daqahliyah"),
			array('name' => "al-Bahr-al-Ahmar"),
			array('name' => "al-Buhayrah"),
			array('name' => "al-Fayyum"),
			array('name' => "al-Gharbiyah"),
			array('name' => "al-Iskandariyah"),
			array('name' => "al-Ismailiyah"),
			array('name' => "al-Jizah"),
			array('name' => "al-Minufiyah"),
			array('name' => "al-Minya"),
			array('name' => "al-Qahira"),
			array('name' => "al-Qalyubiyah"),
			array('name' => "al-Uqsur"),
			array('name' => "al-Wadi al-Jadid"),
			array('name' => "as-Suways"),
			array('name' => "ash-Sharqiyah"),
		);
		// fill cities table with cities array

        foreach ($cities as $key => $city) {
            # code...
            $new_city = new City();
            $new_city->name = $city['name'];
            $new_city->save();
        }

    }
}
