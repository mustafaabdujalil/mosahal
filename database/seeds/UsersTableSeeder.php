<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $user = User::where('email','admin@mosahal.com')->first();
        if(empty($user)){
            $admin = new User();
            $admin->name = 'Admin';
            $admin->email = 'admin@mosahal.com';
            $admin->password = bcrypt('password');
            $admin->mobile = '123456';
            $admin->address = '10 mohamed ali street';
            $admin->role = 'Admin';
            $admin->image = 'user.jpg';
            $admin->card_code = 'admin';
            $admin->city_id = 23;
            $admin->activity = 'Admin';
            $admin->save();
        }
    }
}
