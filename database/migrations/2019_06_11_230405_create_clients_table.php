<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');//
            $table->string('national_id');//
            $table->string('gender');//
            $table->string('social_status');//
            $table->string('job');//
            $table->integer('company_id');//
            $table->string('customer_number')->nullable();
            $table->string('department')->nullable();
            $table->string('work_area')->nullable();
            $table->integer('salary')->nullable();
            $table->string('address');//
            $table->integer('floor_number')->nullable();
            $table->integer('flat_number')->nullable();
            $table->string('area')->nullable();
            $table->string('fork_name')->nullable();
            $table->string('special_mark')->nullable();
            $table->integer('city_id')->nullable();
            $table->string('mother_name')->nullable();
            $table->string('mobile');//
            $table->string('alter_mobile')->nullable();
            $table->string('phone')->nullable();
            $table->string('available_calling_time')->nullable();
            $table->string('whatsapp_number')->nullable();
            $table->string('code');//
            $table->text('image')->nullable();
            $table->date('dealing_date')->nullable();
            $table->string('rate')->nullable();
            $table->integer('submitted_value')->nullable();
            $table->integer('repayment_date');//
            $table->string('follower_name')->nullable();
            $table->string('follower_phone')->nullable();
            $table->string('follower_address')->nullable();
            $table->integer('follower_floor_number')->nullable();
            $table->integer('follower_flat_number')->nullable();
            $table->string('follower_area')->nullable();
            $table->string('follower_fork_name')->nullable();
            $table->string('follower_special_mark')->nullable();
            $table->string('relation')->nullable();
            $table->string('partner_name')->nullable();
            $table->string('partner_company_name')->nullable();
            $table->string('partner_work_address')->nullable();
            $table->string('partner_job')->nullable();
            $table->string('partner_work_area')->nullable();
            $table->string('partner_department')->nullable();
            $table->string('partner_mobile')->nullable();
            $table->integer('status')->comment('0:not active, 1:active');
            $table->text('facebook_link')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
