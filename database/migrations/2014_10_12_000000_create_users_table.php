<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->text('image')->nullable();
            $table->string('role')->comment('admin','supplier','collector');
            $table->string('phone')->nullable();
            $table->string('mobile');
            $table->string('whatsapp_number')->nullable();
            $table->text('address');
            $table->integer('city_id');
            $table->integer('total_collector_point')->default(0);
            $table->string('activity');
            $table->integer('status')->default(1)->comment('0:not active , 1:active');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
