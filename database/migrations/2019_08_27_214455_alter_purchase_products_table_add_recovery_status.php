<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPurchaseProductsTableAddRecoveryStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('purchase_products', function($table) {
            $table->integer('recovery_status')->default(0)->comment('0:not recovery , 1:recovery');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('purchase_products', function($table) {
            $table->dropColumn('recovery_status');
        });
    }
}
