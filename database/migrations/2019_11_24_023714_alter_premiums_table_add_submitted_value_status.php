<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPremiumsTableAddSubmittedValueStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('premiums', function($table) {
            $table->integer('submitted_value_status')->default(0)->after('collector_id');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('premiums', function($table) {
            $table->dropColumn('submitted_value_status');
        });
    }
}
