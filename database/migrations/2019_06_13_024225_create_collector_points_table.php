<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCollectorPointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('collector_points', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('collector_id')->comment('collector id');// from table users
            $table->unsignedBigInteger('premiums_id')->comment('premiums id');
            $table->integer('points')->default(0);
            $table->foreign('premiums_id')->references('id')->on('premiums')->onDelete('cascade');
            $table->foreign('collector_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('collector_points');
    }
}
