<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePremiumsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('premiums', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('purchase_id');
            $table->decimal('paid_amount',8,2)->default(0.00);
            $table->decimal('remaining_amount',8,2)->default(0.00);// from total must be paid
            $table->decimal('total_remaining_amount',8,2)->default(0.00);
            $table->integer('premium_month_number');
            $table->integer('status')->comment('0:not paid , 1:paid, 2:delay');
            $table->string('code');
            $table->date('collect_date');
            $table->unsignedBigInteger('collector_id')->nullable();// from table users
            $table->foreign('collector_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('purchase_id')->references('id')->on('purchases')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('premiums');
    }
}
