<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUsersTableAddNewCols extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('users', function($table) {
            $table->integer('collect_time')->after('activity')->nullable()->comment('for supplier only');
            $table->integer('premiums_months')->after('collect_time')->default(0)->comment('for supplier only');
            $table->integer('supplier_employee_branch_id')->after('premiums_months')->nullable()->comment('for supplier only');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('users', function($table) {
            $table->dropColumn('collect_time');
            $table->dropColumn('premiums_months');
            $table->dropColumn('supplier_employee_branch_id');
        });
    }
}
