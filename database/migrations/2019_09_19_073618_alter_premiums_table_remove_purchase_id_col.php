<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPremiumsTableRemovePurchaseIdCol extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('premiums', function($table) {
            $table->dropForeign('premiums_purchase_id_foreign');
            $table->dropColumn('purchase_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('premiums', function($table) {
            $table->integer('purchase_id')->after('id');
        });
    }

}
