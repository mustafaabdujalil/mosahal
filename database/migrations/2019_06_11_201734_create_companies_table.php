<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->text('description');
            $table->string('manager_name');
            $table->text('address');
            $table->string('phone')->nullable();
            $table->string('mobile');
            $table->text('logo')->nullable();
            $table->integer('premiums_months');
            $table->integer('max_value');
            $table->string('whatsapp_number')->nullable();
            $table->string('city_id');
            $table->string('activity');
            $table->integer('status')->default(1)->comment('0:not active , 1:active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
